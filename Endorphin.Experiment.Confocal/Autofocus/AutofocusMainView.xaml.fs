// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Confocal.Autofocus.View

open Endorphin.Experiment.Confocal.Autofocus
open FsXaml
open System.Threading
open System.Runtime.CompilerServices
open System.Windows
open System.Windows.Controls
open System.Windows.Data
open System.Windows.Media
open System.Windows.Threading

type AutofocusMainView = XAML<"Autofocus/AutofocusMainView.xaml">