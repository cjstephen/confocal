// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

#r "../../packages/Endorphin.Core/lib/net452/Endorphin.Core.dll"
#r "../../packages/FSharp.Control.Reactive/lib/net40/FSharp.Control.Reactive.dll"
#r "../../packages/FSharp.ViewModule.Core/lib/portable-net45+netcore45+wpa81+wp8+MonoAndroid1+MonoTouch1/FSharp.ViewModule.dll"
#r "../../packages/Extended.Wpf.Toolkit/lib/net40/Xceed.Wpf.Toolkit.dll"
#r "../../packages/FsXaml.Wpf/lib/net45/FsXaml.Wpf.dll"
#r "../../packages/FsXaml.Wpf/lib/net45/FsXaml.Wpf.TypeProvider.dll"
#r "../../packages/OxyPlot.Core/lib/net45/OxyPlot.dll"
#r "../../packages/OxyPlot.Wpf/lib/net45/OxyPlot.Wpf.dll"
#r "../../packages/OxyPlot.Wpf/lib/net45/OxyPlot.Xps.dll"
#r "PresentationCore.dll"
#r "PresentationFramework.dll"
#r "System.Core.dll"
#r "System.dll"
#r "System.Numerics.dll"
#r "../../packages/Rx-Core/lib/net45/System.Reactive.Core.dll"
#r "../../packages/Rx-Interfaces/lib/net45/System.Reactive.Interfaces.dll"
#r "../../packages/Rx-Linq/lib/net45/System.Reactive.Linq.dll"
#r "../../packages/Expression.Blend.Sdk/lib/net45/System.Windows.Interactivity.dll"
#r "System.Xaml.dll"
#r "System.Xml.dll"
#r "WindowsBase.dll"
#r "../../packages/Endorphin.Instrument.SwabianInstruments.PulseStreamer8/lib/net452/Endorphin.Instrument.SwabianInstruments.PulseStreamer8.dll"
#r "../../packages/Endorphin.Instrument.PicoQuant.PicoHarp300/lib/net452/Endorphin.Instrument.PicoQuant.PicoHarp300.dll"
#r "../../packages/Endorphin.Abstract/lib/net452/Endorphin.Abstract.dll"
#r "../../Endorphin.Instrument.FastScanningController/bin/Debug/Endorphin.Instrument.FastScanningController.dll"
#r "../bin/Debug/Endorphin.Experiment.Confocal.dll"

open System
open System.Threading
open FsXaml
open Endorphin.Experiment.Confocal.Autofocus
open Endorphin.Instrument
open Endorphin.Instrument.SwabianInstruments
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Core

let autofocusWindow = Ui.createAutofocusWindow picoHarpStreamingAcquisition pulseGeneratorAgent scanningControllerAgent
let dispatcher = Windows.Threading.Dispatcher.CurrentDispatcher
let autofocusAutomator = Automation.createAgent autofocusWindow dispatcher
autofocusWindow.Show()