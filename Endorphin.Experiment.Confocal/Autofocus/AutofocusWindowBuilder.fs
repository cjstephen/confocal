// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Confocal.Autofocus

open System
open System.Windows
open System.Windows.Controls
open System.Windows.Forms.Integration
open Endorphin.Core
open Endorphin.Experiment.Confocal.Autofocus

module Ui =
    let createAutofocusWindow picoHarpStreamingAcquisition pulseGeneratorAgent scanningControllerAgent =   
        let autofocusWindow = System.Windows.Window()
        autofocusWindow.Title    <- "Autofocus"
        autofocusWindow.Width    <- 660.
        autofocusWindow.Height   <- 380.

        let autofocusView = Endorphin.Experiment.Confocal.Autofocus.View.AutofocusMainView()
        autofocusView.DataContext <- Endorphin.Experiment.Confocal.Autofocus.AutofocusViewModel(picoHarpStreamingAcquisition, pulseGeneratorAgent, scanningControllerAgent)
        autofocusWindow.Content <- autofocusView
        ElementHost.EnableModelessKeyboardInterop(autofocusWindow)
        autofocusWindow

module Automation = 
    type AutofocusMessage =
        | PerformAutofocus

    type AutofocusAutomator = AutofocusAutomator of agent: Agent<AutofocusMessage>

    let internal performAutofocus (autofocusWindow: Windows.Window) (dispatcher : Threading.Dispatcher) = 
        do dispatcher.Invoke ( fun () ->
            let view = autofocusWindow.Content :?> Endorphin.Experiment.Confocal.Autofocus.View.AutofocusMainView
            do view.StartButton.Command.Execute(view.StartButton.CommandParameter))

    let createAgent (autofocusWindow: Windows.Window) dispatcher = 
        Agent.Start(fun mailbox ->
            let rec loop () = async { 
                let! message = mailbox.Receive()
                match message with
                | PerformAutofocus      -> performAutofocus autofocusWindow dispatcher; return! loop ()
                }

            loop ())