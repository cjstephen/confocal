// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Confocal.Autofocus

open System
open System.Windows
open System.IO
open System.Threading
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols

open FsXaml
open OxyPlot
open AutofocusExperiment
open Endorphin.Core
open Endorphin.Instrument.FastScanningController
open Endorphin.Instrument
open Endorphin.Abstract.Position

open FSharp.Control.Reactive
open FSharp.ViewModule

type AutofocusExperimentState = 
    | Waiting
    | Running of experiment : AutofocusExperiment * cts : CancellationTokenSource
    | Finished of parameters : AutofocusExperimentParameters * data : XYData * ZData

type AutofocusViewModel(picoHarpStreamingAcquisition, pulseGeneratorAgent, stageControllerAgent) as self = 
    inherit ViewModelBase()

    let defaultParameters = 
        { XYScanDistance  = 0.5m<um>
          ZScanDistance   = 0.7m<um>
          NumberOfPoints  = 30
          IntegrationTime = 15<ms>}

    let xySeries = new Series.HeatMapSeries() 
    do xySeries.Interpolate <- false
    do xySeries.Data <- Array2D.init 10 10 (fun _ _ -> Double.NaN) 
    let xyDefaultPlotModel = 
        let model = new PlotModel()
        model.Series.Add xySeries
        let axes = new OxyPlot.Axes.LinearColorAxis()
        axes.Palette <- OxyPlot.OxyPalettes.Jet(700)
        axes.HighColor <- OxyColors.DarkRed
        axes.LowColor <- OxyColors.Blue
        axes.IsAxisVisible <- false
        model.Axes.Add(axes)
        model

    let zSeries = new Series.LineSeries()
    let zDefaultPlotModel =
        let model = new PlotModel()
        model.Series.Add zSeries
        let axes = new OxyPlot.Axes.LinearAxis()
        axes.IsAxisVisible <- false
        model.Axes.Add(axes)
        model

    let experimentParameters = self.Factory.Backing(<@ self.ExperimentParameters @>, defaultParameters, Parameters.validate)
    let experimentState = self.Factory.Backing(<@ self.ExperimentState @>, Waiting)
    let xyPlotModel = self.Factory.Backing(<@ self.XYPlotModel @>, xyDefaultPlotModel)
    let zPlotModel = self.Factory.Backing(<@ self.ZPlotModel @>, zDefaultPlotModel)
    let statusMessage = self.Factory.Backing(<@ self.StatusMessage @>, "")

    let performExperiment ui (parameters : AutofocusExperimentParameters) = async {
        self.ZData.Clear()
        self.ZPlotModel.InvalidatePlot true
        do! Async.SwitchToContext ui

        let cts = new CancellationTokenSource()

        let experiment = AutofocusExperiment.create parameters picoHarpStreamingAcquisition pulseGeneratorAgent stageControllerAgent

        use __ = 
            AutofocusExperiment.status experiment
            |> Observable.observeOnContext ui
            |> Observable.subscribeWithError
                (fun status -> self.StatusMessage <- sprintf "%s" <| status.ToString())
                (fun exn    -> self.StatusMessage <- sprintf "Experiment failed :%s" exn.Message)

        use __ = 
            xyData experiment
            |> Observable.observeOnContext ui
            |> Observable.map (fun data -> Seq.head <| (Map.toSeq data))
            |> Observable.subscribe (fun (index, counts) ->
                self.XYData.[fst index, snd index] <- float counts )

        use __ = 
            Observable.interval (TimeSpan.FromMilliseconds 200.)
            |> Observable.subscribe (fun _ -> self.XYPlotModel.InvalidatePlot true)

        use __ = 
            zData experiment
            |> Observable.map (fun data -> Seq.head <| (Map.toSeq data))
            |> Observable.observeOnContext ui
            |> Observable.subscribe (fun data ->
                let point = new DataPoint(float (fst data), float (snd data))
                self.ZData.Add point
                self.ZPlotModel.InvalidatePlot true)

        let! origin = async {
            use! stageControllerAccess = QueuedAccessAgent.requestControl stageControllerAgent
            return! Instrument.Position.getPosition stageControllerAccess.Contents 
        }

        xySeries.Data <- Array2D.zeroCreate self.ExperimentParameters.NumberOfPoints self.ExperimentParameters.NumberOfPoints
        xySeries.X0 <- float <| (((Point.tfst origin) - (self.ExperimentParameters.XYScanDistance)) / 1m<um>)
        xySeries.X1 <- float <| (((Point.tfst origin) + (self.ExperimentParameters.XYScanDistance)) / 1m<um>)
        xySeries.Y0 <- float <| (((Point.tsnd origin) - (self.ExperimentParameters.XYScanDistance)) / 1m<um>)
        xySeries.Y1 <- float <| (((Point.tsnd origin) + (self.ExperimentParameters.XYScanDistance)) / 1m<um>)

        self.ExperimentState <- Running(experiment, cts)

        do! Async.SwitchToThreadPool()

        let! waitForData = 
            AutofocusExperiment.zData experiment
            |> Observable.last
            |> Async.AwaitObservable
            |> Async.StartChild

        let! experimentResult = AutofocusExperiment.run experiment cts.Token
        let! data = waitForData

        do! Async.SwitchToContext ui
        self.ExperimentState <- Finished (parameters, Unchecked.defaultof<_>, Unchecked.defaultof<_>)
    }

    let triggerAutofocus ui parameters = 
        performExperiment ui parameters

    let startExperimentCommand =
        self.Factory.CommandAsyncParamChecked(
            performExperiment,
            (fun _ -> self.IsReadyToPerformExperiment),
            [ <@ self.IsReadyToPerformExperiment @> ])

    let stopExperimentCommand =
        self.Factory.CommandSyncParamChecked(
            (function Running (_, cts) -> cts.Cancel() | _ -> ()), 
            (fun _ -> self.IsPerformingExperiment), 
            [ <@ self.IsPerformingExperiment @> ])
            
    do
        self.DependencyTracker.AddPropertyDependency(<@ self.NumberOfPoints @>,       <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.XYScanDistance @>,       <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.ZScanDistance @>,        <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IntegrationTime @>,      <@ self.ExperimentParameters @>)

        self.DependencyTracker.AddPropertyDependency(<@ self.IsPerformingExperiment @>, <@ self.ExperimentState @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IsReadyToPerformExperiment @>, <@ self.IsPerformingExperiment @>)

    member x.XYPlotModel : PlotModel = xyPlotModel.Value
    member x.XYData : Double[,] = xySeries.Data

    member x.ZPlotModel : PlotModel = zPlotModel.Value
    member x.ZData : ResizeArray<DataPoint> = zSeries.Points

    member x.ExperimentParameters
        with get()     = experimentParameters.Value 
        and  set value = experimentParameters.Value <- value
    
    member x.ExperimentState
        with get()     = experimentState.Value
        and  set value = experimentState.Value <- value

    member x.StatusMessage
        with get()     = statusMessage.Value
        and  set value = statusMessage.Value <- value

    member x.IsPerformingExperiment = x.ExperimentState |> (function Running _ -> true | _ -> false)
    member x.IsReadyToPerformExperiment = not self.IsPerformingExperiment

    member x.XYScanDistance 
        with get()     = Parameters.xyScanDistance x.ExperimentParameters
        and  set value = x.ExperimentParameters <- Parameters.withXyScanDistance value x.ExperimentParameters

    member x.ZScanDistance 
        with get()     = Parameters.zScanDistance x.ExperimentParameters
        and  set value = x.ExperimentParameters <- Parameters.withZScanDistance value x.ExperimentParameters

    member x.IntegrationTime
        with get()     = Parameters.integrationTime x.ExperimentParameters
        and  set value = x.ExperimentParameters <- Parameters.withIntegrationTime value x.ExperimentParameters

    member x.NumberOfPoints 
        with get()     = Parameters.numberOfPoints x.ExperimentParameters
        and  set value = x.ExperimentParameters <- Parameters.withNumberOfPoints value x.ExperimentParameters

    member x.StartExperiment = startExperimentCommand
    member x.StopExperiment = stopExperimentCommand