// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Confocal.Autofocus

open System
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.ViewModule.Validation
open FSharp.Control.Reactive
open Endorphin.Core
open Endorphin.Instrument
open Endorphin.Instrument.FastScanningController
open Endorphin.Instrument.PicoQuant
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open Endorphin.Abstract.Position
open Endorphin.Abstract.Position

module AutofocusExperiment =
    [<AutoOpen>]
    module Model =
        type AutofocusExperimentParameters =
            internal { XYScanDistance  : decimal<um>
                       ZScanDistance   : decimal<um>
                       NumberOfPoints  : int
                       IntegrationTime : int<ms> }

        type Count = int

        type XYIndex = int * int
        type XYData = Map<XYIndex, Count>

        type ZIndex = int
        type ZData = Map<ZIndex, Count>

        type AutofocusExperimentStatus =
            | StartingExperiment
            | PerformingXYScan
            | PerformingZScan
            | FinishedExperiment
            | CancelledExperiment

            override status.ToString() =
                match status with
                | StartingExperiment  -> "Writing to hardware"
                | PerformingXYScan    -> "Performing XY scan"
                | PerformingZScan     -> "Performing Z scan"
                | FinishedExperiment  -> "Experiment finished"
                | CancelledExperiment -> "Experiment cancelled"

        type AutofocusExperimentResult =
            | ExperimentCompleted
            | ExperimentError of exn
            | ExperimentCancelled

        type AutofocusExperiment =
            internal { Parameters                   : AutofocusExperimentParameters
                       PicoHarpStreamingAcquisition : PicoHarp300.Streaming.StreamingAcquisition
                       StageControllerAgent         : QueuedAccessAgent<ScanningController>
                       PulseGeneratorAgent          : QueuedAccessAgent<PulseStreamer8>
                       StatusChanged                : NotificationEvent<AutofocusExperimentStatus>
                       XYDataAvailable              : Event<XYData>
                       ZDataAvailable               : Event<ZData> }

        module Parameters =
            // query functions
            let xyScanDistance parameters = parameters.XYScanDistance

            let zScanDistance parameters = parameters.ZScanDistance

            let integrationTime parameters = parameters.IntegrationTime

            let numberOfPoints parameters = parameters.NumberOfPoints

            // modification functions
            let withXyScanDistance xyScanDistance parameters =
                { parameters with XYScanDistance = xyScanDistance }

            let withZScanDistance zScanDistance parameters =
                { parameters with ZScanDistance = zScanDistance }

            let withIntegrationTime integrationTime parameters =
                { parameters with IntegrationTime = integrationTime }

            let withNumberOfPoints numberOfPoints parameters =
                { parameters with NumberOfPoints = numberOfPoints }

            let create xyScanDistance zScanDistance numberOfPoints integrationTime =
                { XYScanDistance  = xyScanDistance
                  ZScanDistance   = zScanDistance
                  NumberOfPoints  = numberOfPoints
                  IntegrationTime = integrationTime }

            // validation functions
            let private scanSizeGreaterThanZero =
                (fun parameters ->
                    if ((xyScanDistance parameters) <= 0m<um> || (zScanDistance parameters) <= 0m<um>)
                    then Some "Scan distances must be greater than zero."
                    else None)
                |> Validators.custom

            let validate = scanSizeGreaterThanZero

    let create parameters picoHarpStreamingAcquisition pulseGeneratorAgent stageControllerAgent =
        { Parameters                    = parameters
          PicoHarpStreamingAcquisition  = picoHarpStreamingAcquisition
          PulseGeneratorAgent           = pulseGeneratorAgent
          StageControllerAgent          = stageControllerAgent
          StatusChanged                 = NotificationEvent<_>()
          XYDataAvailable               = Event<_>()
          ZDataAvailable                = Event<_>() }

    let status experiment =
        experiment.StatusChanged.Publish
        |> Observable.fromNotificationEvent

    /// run the XY part of the scan
    let private scanXy experiment (stageControllerAccess : DisposableBox<FastScanningController.ScanningController>) path cancellationToken = async {
        experiment.StatusChanged.Trigger <| Next PerformingXYScan
        do! Instrument.Timing.setDwellTime stageControllerAccess.Contents experiment.Parameters.IntegrationTime

        let histogramParameters = PicoHarp300.Streaming.Acquisition.Histogram.Parameters.create (PicoHarp300.Model.Duration_ms (1.0<ms> * float (experiment.Parameters.IntegrationTime/1<ms>))) (PicoHarp300.Model.Duration.Duration_ms (1.0<ms> * float (experiment.Parameters.IntegrationTime/1<ms>))) PicoHarp300.Model.TTTRConfiguration.Marker2
        let histogramAcquisition = PicoHarp300.Streaming.Acquisition.Histogram.create experiment.PicoHarpStreamingAcquisition histogramParameters

        let histogramObservable =
            PicoHarp300.Streaming.Acquisition.Histogram.HistogramsAvailable histogramAcquisition
            |> Observable.mapi (fun i histogram -> (i, histogram))

        use __ =
            histogramObservable
            |> Observable.subscribe (fun (i, histogram) ->
                let index = Path.pointAtIndex i path
                match histogram.Histogram with
                | [(binNumber, intensity)]     -> experiment.XYDataAvailable.Trigger <| Map.empty.Add(index, intensity)
                | []                           -> experiment.XYDataAvailable.Trigger <| Map.empty.Add(index, 0)
                | _                            -> raise (new Exception(sprintf "PicoHarp streaming histogram in a format other than (bin, intensity) - %A" histogram.Histogram) ) )

        let! waitToFinish =
            histogramObservable
            |> Observable.filter (fun (i, _) -> i = experiment.Parameters.NumberOfPoints * experiment.Parameters.NumberOfPoints - 1)
            |> Async.AwaitObservable
            |> Async.Ignore
            |> Async.StartChild

        do! Instrument.Position.Path.runPath stageControllerAccess.Contents

        do! waitToFinish
    }

    /// run the Z part of the scan
    let private scanZ experiment (stageControllerAccess : DisposableBox<FastScanningController.ScanningController>) path cancellationToken = async {
        experiment.StatusChanged.Trigger <| Next PerformingZScan
        do! Instrument.Timing.setDwellTime stageControllerAccess.Contents experiment.Parameters.IntegrationTime
        let histogramParameters = PicoHarp300.Streaming.Acquisition.Histogram.Parameters.create (PicoHarp300.Model.Duration_ms (1.0<ms> * float (experiment.Parameters.IntegrationTime/1<ms>))) (PicoHarp300.Model.Duration.Duration_ms (1.0<ms> * float (experiment.Parameters.IntegrationTime/1<ms>))) PicoHarp300.Model.TTTRConfiguration.Marker2
        let histogramAcquisition = PicoHarp300.Streaming.Acquisition.Histogram.create experiment.PicoHarpStreamingAcquisition histogramParameters

        let histogramObservable =
            PicoHarp300.Streaming.Acquisition.Histogram.HistogramsAvailable histogramAcquisition
            |> Observable.mapi (fun i histogram -> (i, histogram))

        use __ =
            histogramObservable
            |> Observable.subscribe (fun (i, histogram) ->
                let index = Path.pointAtIndex i path
                match histogram.Histogram with
                | [(binNumber, intensity)]     -> experiment.ZDataAvailable.Trigger <| Map.empty.Add(snd index, intensity)
                | []                           -> experiment.ZDataAvailable.Trigger <| Map.empty.Add(snd index, 0)
                | _                            -> raise (new Exception(sprintf "PicoHarp streaming histogram in a format other than (bin, intensity) - %A" histogram.Histogram) ) )

        let! waitToFinish =
            histogramObservable
            |> Observable.filter (fun (i, _) -> i = experiment.Parameters.NumberOfPoints - 1)
            |> Async.AwaitObservable
            |> Async.Ignore
            |> Async.StartChild

        do! Instrument.Position.Path.runPath stageControllerAccess.Contents

        do! waitToFinish
    }

    let run experiment cancellationToken = async {
        let resultChannel = new ResultChannel<_>()

        use stageControllerAccess = async { return! QueuedAccessAgent.requestControl experiment.StageControllerAgent } |> Async.RunSynchronously
        use pulseStreamerAccess = async { return! QueuedAccessAgent.requestControl experiment.PulseGeneratorAgent } |> Async.RunSynchronously

        // turn laser to CW for autofocus
        PulseStreamer.PulseSequence.setState [Channel.Channel1] pulseStreamerAccess.Contents |> Async.RunSynchronously

        let xyExperimentWorkflow = async {
            experiment.StatusChanged.Trigger <| Next StartingExperiment

            let! origin = Instrument.Position.getPosition stageControllerAccess.Contents
            let transformedOrigin = ((Point.tfst origin - experiment.Parameters.XYScanDistance), (Point.tsnd origin - experiment.Parameters.XYScanDistance), Point.ttrd origin)
            let path = Path.createSquareSnakeByNumberOfPoints transformedOrigin (experiment.Parameters.XYScanDistance * 2m) experiment.Parameters.NumberOfPoints Path.Plane.XY

            do! Instrument.Position.Path.writePathToController stageControllerAccess.Contents path

            do! Instrument.Position.setPosition stageControllerAccess.Contents (Path.coordinatesAtIndex 0 path)
            do! Async.Sleep 500

            use xyData =
                experiment.XYDataAvailable.Publish
                |> Observable.scanInit (Map.empty.Add((0, 0), 0), Map.empty.Add((0, 0), 0)) (fun (maximum, nextMaximum) (newPoint : XYData) ->
                    if (snd <| (Seq.head <| (Map.toSeq newPoint))) > (snd <| (Seq.head <| (Map.toSeq maximum))) then
                        let point = Seq.head <| (Map.toSeq newPoint)
                        (newPoint, maximum)
                    else if (snd <| (Seq.head <| (Map.toSeq newPoint))) > (snd <| (Seq.head <| (Map.toSeq nextMaximum))) then
                        let point = Seq.head <| (Map.toSeq newPoint)
                        (maximum, newPoint)
                    else
                        (maximum, nextMaximum))
                |> Observable.map (fun (x,y) -> y)
                |> Observable.mapi (fun i x -> (i, x))
                |> Observable.filter (fun indexedPoint ->
                    (fst indexedPoint) = ((experiment.Parameters.NumberOfPoints * experiment.Parameters.NumberOfPoints) - 1))
                |> Observable.subscribe (fun indexedPoint ->
                    let point = snd indexedPoint
                    Async.RunSynchronously <| async {
                        let index = fst <| (Seq.head <| (Map.toSeq point))
                        let maxXy = Path.coordinateForPoint index path
                        do! Instrument.Position.setPosition stageControllerAccess.Contents maxXy })
            do! scanXy experiment stageControllerAccess path cancellationToken
        }

        let zExperimentWorkflow = async {
            experiment.StatusChanged.Trigger <| Next PerformingZScan

            let! origin = Instrument.Position.getPosition stageControllerAccess.Contents
            let transformedOrigin = (Point.tfst origin, Point.tsnd origin, (Point.ttrd origin - experiment.Parameters.ZScanDistance))
            let path = Path.createByNumberOfPoints transformedOrigin (0m<um>, (experiment.Parameters.ZScanDistance*2m)) (1, experiment.Parameters.NumberOfPoints) Path.Plane.XZ

            do! Instrument.Position.Path.writePathToController stageControllerAccess.Contents path

            do! Instrument.Position.setPosition stageControllerAccess.Contents (Path.coordinatesAtIndex 0 path)
            do! Async.Sleep 500

            use zData =
                experiment.ZDataAvailable.Publish
                |> Observable.scanInit (Map.empty.Add(0, 0), Map.empty.Add(0,0)) (fun (maximum, nextMaximum) (newPoint : ZData) ->
                    if (snd <| (Seq.head <| (Map.toSeq newPoint))) > (snd <| (Seq.head <| (Map.toSeq maximum))) then
                        (newPoint, maximum)
                    else if (snd <| (Seq.head <| (Map.toSeq newPoint))) > (snd <| (Seq.head <| (Map.toSeq nextMaximum))) then
                        (maximum, newPoint)
                    else
                        (maximum, nextMaximum))
                |> Observable.map (fun (x,y) -> y)
                |> Observable.mapi (fun i x -> (i, x))
                |> Observable.filter (fun indexedPoint ->
                    (fst indexedPoint) = (experiment.Parameters.NumberOfPoints - 1))
                |> Observable.subscribe (fun indexedPoint ->
                    let point = snd indexedPoint
                    Async.RunSynchronously <| async {
                        let index = fst <| (Seq.head <| (Map.toSeq point))
                        let maxZ = Path.coordinateForPoint (0, index) path
                        do! Instrument.Position.setPosition stageControllerAccess.Contents maxZ })
            do! scanZ experiment stageControllerAccess path cancellationToken
        }

        let experimentWorkflow = async {
            do! xyExperimentWorkflow
            do! zExperimentWorkflow
        }

        Async.StartWithContinuations (experimentWorkflow,
            (fun () ->
                experiment.StatusChanged.Trigger <| Next FinishedExperiment
                experiment.StatusChanged.Trigger <| Completed
                resultChannel.RegisterResult ExperimentCompleted),
            (fun exn ->
                experiment.StatusChanged.Trigger <| Error exn
                resultChannel.RegisterResult (ExperimentError exn)),
            (fun exn ->
                experiment.StatusChanged.Trigger <| Next CancelledExperiment
                experiment.StatusChanged.Trigger <| Completed
                resultChannel.RegisterResult ExperimentCancelled),
            cancellationToken)

        let experimentResult = resultChannel.AwaitResult()
        do Instrument.Position.Path.stopPath stageControllerAccess.Contents |> Async.RunSynchronously
        return! experimentResult }

    let xyData experiment =
        experiment.XYDataAvailable.Publish
        |> Observable.takeUntilOther (Observable.last <| status experiment)

    let zData experiment =
        experiment.ZDataAvailable.Publish
        |> Observable.takeUntilOther (Observable.last <| status experiment)