// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Confocal.Autocorrelation

open System
open System.Windows
open System.IO
open System.Threading
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open System.Windows.Forms
open Microsoft.Win32

open FsXaml
open OxyPlot
open AutocorrelationExperiment
open Endorphin.Core
open Endorphin.Instrument

open FSharp.Control.Reactive
open FSharp.ViewModule

module PhotonTimeDiskWriter = 
    type DiskWriterMessage =   
        | OpenFile
        | WritePhotonTriggersToDisk of uint64 list
        | CloseFile

    type PhotonTimeDiskWriter = PhotonTimeDiskWriter of agent: Agent<DiskWriterMessage>

    let internal writePhotonTriggersToDisk (photonTriggers : uint64 list) (fileStream : FileStream) = 
        let photonArrivalTimesAsByteList = List.map (uint64 >> BitConverter.GetBytes) photonTriggers

        let photonArrivalTimesAsByteArray : byte[] = Array.zeroCreate (8 * (List.length photonArrivalTimesAsByteList))

        photonArrivalTimesAsByteList
        |> List.iteri (fun i photonArrivalTime -> photonArrivalTimesAsByteArray.[(i * 8)..(i * 8 + 7)] <- photonArrivalTime) 

        //if photonArrivalTimesAsByteList.[(List.length photonArrivalTimesAsByteList) - 1] < photonArrivalTimesAsByteList.[0] then
        //    printfn "something is wrong"

        do fileStream.Write(photonArrivalTimesAsByteArray,  0,  (Array.length photonArrivalTimesAsByteArray))

    let createDiskWriter (filepath : string) = 
        let fileStream = Unchecked.defaultof<_>

        Agent.Start(fun mailbox ->
            let rec loop fileStream = async { 
                let! message = mailbox.Receive()
                match message with 
                | OpenFile -> 
                    return! loop (new System.IO.FileStream(filepath, FileMode.Create))
                | WritePhotonTriggersToDisk photonTriggers -> 
                    writePhotonTriggersToDisk photonTriggers fileStream
                    return! loop fileStream
                | CloseFile ->
                    fileStream.Close()
                    return! loop (Unchecked.defaultof<_>) }
            loop fileStream )

type AutocorrelationExperimentState = 
    | Waiting
    | Running of experiment : AutocorrelationExperiment * cts : CancellationTokenSource
    | Finished

type AutocorrelationViewModel(picoHarpStreamingAcquisition, pulseGeneratorAgent) as self = 
    inherit ViewModelBase()

    let defaultParameters = { TotalExperimentTime = 180<s> }

    let experimentParameters = self.Factory.Backing(<@ self.ExperimentParameters @>, defaultParameters)
    let experimentState = self.Factory.Backing(<@ self.ExperimentState @>, Waiting)
    let experimentElapsedTime = self.Factory.Backing(<@ self.ExperimentElapsedTime @>, "")
    let statusMessage = self.Factory.Backing(<@ self.StatusMessage @>, "")
    let filepath = self.Factory.Backing(<@ self.Filepath @>, "")

    let chooseFile () = 
        let saveFile = new SaveFileDialog (Filter="Photon stream | *.ttd")
        let result = saveFile.ShowDialog()
        if not (result.HasValue && result.Value) then
            ()
        else 
            self.Filepath <- saveFile.FileName

    let performExperiment ui = async {
        do! Async.SwitchToContext ui

        let cts = new CancellationTokenSource()

        let parameters = self.ExperimentParameters
        let experiment = AutocorrelationExperiment.create parameters picoHarpStreamingAcquisition pulseGeneratorAgent

        use __ = 
            AutocorrelationExperiment.status experiment
            |> Observable.subscribeWithError
                (fun status -> self.StatusMessage <- sprintf "%s" <| status.ToString())
                (fun exn    -> self.StatusMessage <- sprintf "Experiment failed :%s" exn.Message)

        let baseFilename = Path.Combine(Path.GetDirectoryName(self.Filepath), Path.GetFileNameWithoutExtension(self.Filepath))
        let channel0PhotonWriter = PhotonTimeDiskWriter.createDiskWriter (baseFilename + "-1.ttd")
        channel0PhotonWriter.Post (PhotonTimeDiskWriter.OpenFile)
        let channel1PhotonWriter = PhotonTimeDiskWriter.createDiskWriter (baseFilename + "-2.ttd")
        channel1PhotonWriter.Post (PhotonTimeDiskWriter.OpenFile)

        use __ = 
            AutocorrelationExperiment.photonStream experiment
            |> Observable.observeOnContext ui
            |> Observable.subscribe(fun (ch0photons, ch1photons) ->
                channel0PhotonWriter.Post (PhotonTimeDiskWriter.WritePhotonTriggersToDisk(ch0photons))
                channel1PhotonWriter.Post (PhotonTimeDiskWriter.WritePhotonTriggersToDisk(ch1photons)))

        self.ExperimentState <- Running(experiment, cts)

        use __ = 
            Observable.interval (TimeSpan.FromSeconds 1.)
            |> Observable.subscribe (fun i -> self.ExperimentElapsedTime <- "Elapsed time: " + (string i) + " s")

        do! Async.SwitchToThreadPool()

        let! experimentResult = AutocorrelationExperiment.run
                                <| experiment
                                <| cts.Token

        do! Async.SwitchToContext ui

        channel0PhotonWriter.Post (PhotonTimeDiskWriter.CloseFile)
        channel1PhotonWriter.Post (PhotonTimeDiskWriter.CloseFile)

        self.ExperimentState <- Finished
        self.ExperimentElapsedTime <- ""
    }

    // commands
    let startExperimentCommand = 
        self.Factory.CommandAsyncChecked(
            performExperiment,
            (fun _ -> self.IsReadyToStart), 
            [ <@ self.IsReadyToStart @> ])

    let stopExperimentCommand = 
        self.Factory.CommandSyncParamChecked(
            (function 
                | Running (experiment, cts) -> cts.Cancel()
                | _ -> ()), 
            (fun _ -> self.IsPerformingExperiment), 
            [ <@ self.IsPerformingExperiment @> ])

    let chooseFileCommand = 
        self.Factory.CommandSync(chooseFile)

    do
        self.DependencyTracker.AddPropertyDependency(<@ self.IsPerformingExperiment @>,                  <@ self.ExperimentState @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.TotalExperimentTime @>,                     <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependencies(<@@ self.IsReadyToStart @@>,                      [ <@@ self.IsPerformingExperiment @@>; <@@ self.Filepath @@> ])

    member x.IsPerformingExperiment = x.ExperimentState |> (function Running _ -> true | _ -> false)

    member x.IsReadyToStart = not x.IsPerformingExperiment && x.Filepath <> ""

    member x.ExperimentParameters 
        with get()     = experimentParameters.Value
        and  set value = experimentParameters.Value <- value

    member x.ExperimentState
        with get()     = experimentState.Value
        and  set value = experimentState.Value <- value

    member x.StatusMessage
        with get()     = statusMessage.Value
        and  set value = statusMessage.Value <- value

    member x.Filepath
        with get()     = filepath.Value
        and  set value = filepath.Value <- value

    member x.ExperimentElapsedTime
        with get()     = experimentElapsedTime.Value
        and  set value = experimentElapsedTime.Value <- value

    member x.TotalExperimentTime
        with get()     = int (Parameters.totalExperimentTime x.ExperimentParameters)
        and  set value = x.ExperimentParameters <- Parameters.withTotalExperimentTime (value * 1<s>) x.ExperimentParameters

    member x.StartExperiment = startExperimentCommand
    member x.StopExperiment = stopExperimentCommand
    member x.ChooseFilename = chooseFileCommand