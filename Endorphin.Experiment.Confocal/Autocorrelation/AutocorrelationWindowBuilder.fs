// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Confocal.Autocorrelation

open System
open System.Windows
open System.Windows.Controls
open System.Windows.Forms.Integration
open Endorphin.Core
open Endorphin.Experiment.Confocal.Autofocus

module Ui =
    let createAutofocusWindow picoHarpStreamingAcquisition pulseGeneratorAgent =   
        let autocorrelationWindow = System.Windows.Window()
        autocorrelationWindow.Title    <- "Autocorrelation"
        autocorrelationWindow.Width    <- 300.
        autocorrelationWindow.Height   <- 220.

        let autocorrelationView = Endorphin.Experiment.Confocal.Autocorrelation.View.AutocorrelationMainView()
        autocorrelationView.DataContext <- Endorphin.Experiment.Confocal.Autocorrelation.AutocorrelationViewModel(picoHarpStreamingAcquisition, pulseGeneratorAgent)
        autocorrelationWindow.Content <- autocorrelationView
        ElementHost.EnableModelessKeyboardInterop(autocorrelationWindow)
        autocorrelationWindow