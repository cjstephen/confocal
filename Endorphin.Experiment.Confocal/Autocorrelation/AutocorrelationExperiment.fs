// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Confocal.Autocorrelation

open System
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.ViewModule.Validation
open FSharp.Control.Reactive
open Endorphin.Core
open Endorphin.Instrument.PicoQuant
open Endorphin.Instrument.FastScanningController
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open Endorphin.Abstract.Position

module AutocorrelationExperiment = 
    [<AutoOpen>]
    module Model = 
        type AutocorrelationExperimentParameters = 
            { TotalExperimentTime     : int<s> }

        type AutocorrelationExperimentStatus = 
            | StartingExperiment
            | PerformingExperiment
            | CancelledExperiment
            | FinishedExperiment

            override status.ToString() = 
                match status with
                | StartingExperiment    -> "Setting up measurement"
                | PerformingExperiment  -> "Experiment running"
                | CancelledExperiment   -> "Experiment cancelled"
                | FinishedExperiment    -> "Experiment finished"

        type AutocorrelationExperimentResult = 
            | ExperimentCompleted
            | ExperimentError of exn
            | ExperimentCancelled

        type LiveCounts = int

        type PhotonStream = uint64 list * uint64 list

        type AutocorrelationExperiment = 
            internal { Parameters                   : AutocorrelationExperimentParameters
                       PicoHarpStreamingAcquisition : PicoHarp300.Streaming.StreamingAcquisition
                       PulseGeneratorAgent          : QueuedAccessAgent<PulseStreamer8>
                       StatusChanged                : NotificationEvent<AutocorrelationExperimentStatus> 
                       PhotonTriggersAvailable      : Event<PhotonStream> }

        module Parameters =
            let totalExperimentTime (parameters : AutocorrelationExperimentParameters) = parameters.TotalExperimentTime

            let withTotalExperimentTime experimentTime parameters = { parameters with TotalExperimentTime = experimentTime }

            let create totalTime = 
                { TotalExperimentTime = totalTime }

    let create parameters picoHarpStreamingAcquisition pulseGeneratorAgent =
        { Parameters                    = parameters
          PicoHarpStreamingAcquisition  = picoHarpStreamingAcquisition
          PulseGeneratorAgent           = pulseGeneratorAgent
          StatusChanged                 = NotificationEvent<_>()
          PhotonTriggersAvailable       = Event<_>() }

    let parameters experiment = experiment.Parameters

    let status experiment = 
        experiment.StatusChanged.Publish
        |> Observable.fromNotificationEvent

    let run experiment cancellationToken = async {
        let resultChannel = new ResultChannel<_>()

        let experimentWorkflow = async {
            do! async {
                use pulseStreamerAccess = async { return! QueuedAccessAgent.requestControl experiment.PulseGeneratorAgent } |> Async.RunSynchronously

                // turn laser to CW 
                do! PulseStreamer.PulseSequence.setState [Channel.Channel1] pulseStreamerAccess.Contents        
            }

            let photonStreamAcquisition = PicoHarp300.Streaming.Acquisition.PhotonTimeStream.create experiment.PicoHarpStreamingAcquisition

            use __ = 
                PicoHarp300.Streaming.Acquisition.PhotonTimeStream.PhotonTimesAvailable photonStreamAcquisition
                |> Observable.subscribe experiment.PhotonTriggersAvailable.Trigger

            experiment.StatusChanged.Trigger <| Next PerformingExperiment

            let! waitToFinish = 
                Observable.interval (TimeSpan.FromSeconds (float (Parameters.totalExperimentTime (parameters experiment))))
                |> Async.AwaitObservable
                |> Async.Ignore
                |> Async.StartChild

            do! waitToFinish }

        Async.StartWithContinuations (experimentWorkflow, 
            (fun () -> 
                experiment.StatusChanged.Trigger <| Next FinishedExperiment
                experiment.StatusChanged.Trigger <| Completed
                resultChannel.RegisterResult ExperimentCompleted),
            (fun exn ->
                experiment.StatusChanged.Trigger <| Error exn
                resultChannel.RegisterResult (ExperimentError exn)),
            (fun exn ->
                experiment.StatusChanged.Trigger <| Next CancelledExperiment
                experiment.StatusChanged.Trigger <| Completed
                resultChannel.RegisterResult ExperimentCancelled),
            cancellationToken)

        return! resultChannel.AwaitResult() }

    let photonStream experiment = 
        experiment.PhotonTriggersAvailable.Publish
        |> Observable.takeUntilOther (Observable.last <| status experiment)