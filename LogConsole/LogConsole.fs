// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.LogConsole

open System.Windows.Controls
open FsXaml
open ViewModule
open ViewModule.FSharp
open System.Threading
open FSharp.Control.Reactive

type LogConsoleBase = XAML<"LogConsole.xaml">

type ObservableAppender() =
    inherit log4net.Appender.AppenderSkeleton()

    let output = new Event<log4net.Core.LoggingEvent>()

    override __.Append (event:log4net.Core.LoggingEvent) = event |> output.Trigger
    member __.LoggingEvents() = output.Publish
    member this.RenderEvent x = this.RenderLoggingEvent x

    member this.LoggingEventStrings() = this.LoggingEvents() |> Event.map this.RenderEvent

type LogConsole() as view =
    inherit LogConsoleBase()

    let logSubscribe _ =
        let heirarchy = log4net.LogManager.GetRepository()
        let logger = log4net.LogManager.GetLogger "LogConsole"
        let observableAppender = heirarchy.GetAppenders()
                                    |> Array.filter (function | :? ObservableAppender -> true | _ -> false )
                                    |> Array.tryHead
                                    |> Option.map (fun a -> a :?> ObservableAppender)
        match observableAppender with
        | Some a ->
            a.LoggingEventStrings()
            |> Observable.observeOnContext SynchronizationContext.Current
            |> Observable.add view.AppendLogEvent
        | None ->
            logger.Error "LogConsole cannot connect as no ObservableAppenders can be found"
            view.AppendLogEvent "LogConsole not connected as no ObservableAppender found. Check app.config for log4net configuration"

    do
        view.Loaded.Add(logSubscribe)

    member view.AppendLogEvent x =
        x |> view.LogOutput.AppendText 
        view.LogOutput.CaretIndex <- view.LogOutput.Text.Length
        view.LogOutput.HorizontalScrollBarVisibility <- ScrollBarVisibility.Hidden
        view.LogOutput.VerticalScrollBarVisibility <- ScrollBarVisibility.Visible
        view.LogOutput.ScrollToEnd()

type LogConsoleViewModel() =
    inherit ViewModelBase()

