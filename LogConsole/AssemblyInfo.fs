// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace System
open System.Reflection

[<assembly: AssemblyTitleAttribute("Endorphin.LogConsole")>]
[<assembly: AssemblyProductAttribute("Endorphin.LogConsole")>]
[<assembly: AssemblyDescriptionAttribute("Stream & display support for log4net.")>]
[<assembly: AssemblyVersionAttribute("0.1.0")>]
[<assembly: AssemblyFileVersionAttribute("0.1.0")>]
do ()

module internal AssemblyVersionInformation =
    let [<Literal>] Version = "0.1.0"
    let [<Literal>] InformationalVersion = "0.1.0"
