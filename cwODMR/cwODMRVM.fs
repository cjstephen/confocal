﻿namespace cwODMR

open ViewModule
open ViewModule.FSharp
open OxyPlot
open Endorphin.Connection
open Endorphin.Instruments
open Endorphin.Instrument.Keysight
open Endorphin.Instrument.Keysight.N5172B.Model
open System.Threading
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Core
open System
open Acquisition
open MathNet.Numerics.Statistics
open FSharp.Control.Reactive


type Frequency = decimal<Hz>
type ScanParameters = {
    StartFrequency  : Frequency
    EndFrequency    : Frequency
    Power           : decimal<dBm>
    Points          : int
    IntegrationTime : int<ms> }

type TriggerScan = unit -> unit
type ScanOutput = IObservable<Frequency*Intensity>
type ExperimentOutput = ScanParameters*IObservable<ScanOutput>
type Experiment = {
    Parameters    : ScanParameters
    Instruments   : Lease<InstrumentId>
    Scans         : NotificationEvent<ScanOutput>
    StopAfterScan : CancellationTokenSource
    StopNow       : CancellationTokenSource }


type ScanChange =
    | Start of ScanParameters
    | Parameters of AsyncReplyChannel<ScanParameters option>
    | IsRunning of AsyncReplyChannel<bool>
    | Prepared
    | RunScan
    | ScanCompleted
    | CancelImmediately
    | StopAfterScan
    | Stop
    | Stopped
type ScanState =
    | Idle
    | Preparing of Experiment
    | Ready of Experiment
    | Running of Experiment
    | Stopping


module CwOdmr =

    let defaultParameters = {
        StartFrequency = 2700000000m<Hz>
        EndFrequency = 3000000000m<Hz>
        Power = 10m<dBm>
        Points = 1000
        IntegrationTime = 10<ms> }

    let frequencies (p:ScanParameters) =
        let inc = (p.EndFrequency - p.StartFrequency) / (p.Points-1 |> decimal)
        Array.init p.Points (fun i -> p.StartFrequency + (decimal i * inc))

type CwOdmrAgent(connectionManager : ConnectionManager.ConnectionManager<InstrumentId>) =

    let logger = log4net.LogManager.GetLogger "cwODMR"
    let experimentOutput = new NotificationEvent<ExperimentOutput>()
    let statusChange = new NotificationEvent<string>()
    let updateStatus = Next >> statusChange.Trigger

    let detectionInstrument = PhotonCounterId "PhotonCounter"
    let rfSource = RfSourceId "Keysight"

    let setupKeysight (experiment:Experiment) = async {
        // Set up Keysight
        logger.Debug "Setting up keysight"
        let parameters = experiment.Parameters
        let dwellDuration (t:int<ms>) = t |> float |> (*) 1.e-3<s> |> Duration_sec |> Some
        let decToFloat (x:decimal<'a>) = decimal x |> Decimal.ToDouble |> LanguagePrimitives.FloatWithMeasure<'a>
        let frequencyStep = (parameters.EndFrequency - parameters.StartFrequency) / (parameters.Points-1 |> decimal)
        let startFrequency = parameters.StartFrequency - frequencyStep
        let endFrequency = parameters.EndFrequency + frequencyStep
        let startFrequency' = decToFloat startFrequency
        let endFrequency' = decToFloat endFrequency
        
        let keysight = Instruments.getRfSource "Keysight" experiment.Instruments
        let sweep = N5172B.Sweep.Configure.frequencyStepSweepInHz startFrequency' endFrequency'
                    |> N5172B.Sweep.Configure.withPoints (parameters.Points+2)
                    |> N5172B.Sweep.Configure.withFixedPowerInDbm (decToFloat parameters.Power)
                    |> N5172B.Sweep.Configure.withDwellTime (dwellDuration parameters.IntegrationTime)
                    |> N5172B.Sweep.Configure.withStepTrigger (Some Triggering.Bus)
                    |> N5172B.Sweep.Configure.withRetrace Off
                    |> Sweep.StepSweep
        let routing = N5172B.Routing.empty |> N5172B.Routing.withTrigger1 N5172B.Model.Route.RouteSourceSettled
        let modulation = None
        let rfConfig = { Sweep = sweep; Modulation = [] }
        do! N5172B.Rf.applySettings rfConfig keysight
        do! N5172B.Routing.set routing keysight
        do! N5172B.Basic.setOutput On keysight }

    let runScan experiment = async {
//        logger.Debug "Emitting experiment start"
////        let odmrRun = new NotificationEvent<ScanOutput>()
//        let seriesOfScans = experiment.Scans.Publish |> Observable.fromNotificationEvent
//        (parameters,seriesOfScans) |> Next |> odmrExperimentEvent.Trigger
        let frequencies = CwOdmr.frequencies experiment.Parameters
        let frequencies = Array.concat [ Array.create 2 frequencies.[0] ; frequencies ]
        let keysight = Instruments.getRfSource "Keysight" experiment.Instruments
        let dwell = experiment.Parameters.IntegrationTime |> float
        let timeout = if dwell > 200.0 then dwell * 4.0 else 800.0
        if not experiment.StopAfterScan.Token.IsCancellationRequested then
            do! Async.Sleep 100
            do! Async.SwitchToNewThread()
            logger.Debug "Setting up scan"
            // create & emit scan set sequence
            let data = Acquisition.scan detectionInstrument experiment.Instruments frequencies
                        |> Observable.skip 1
                        |> Observable.take (experiment.Parameters.Points)
//                        |> Observable.timeoutSpan (TimeSpan.FromMilliseconds timeout )
                        |> Observable.iterErrorEnd ignore (sprintf "Acquisition error %A" >> logger.Error) (fun _ -> logger.Info "Scan sequence complete")
//                           |> Observable.performFinally (fun _ -> logger.Debug "Scan data finished")
//            sprintf "Points: %d" experiment.Parameters.Points |> logger.Debug
            logger.Debug "Emitting scan"
            data |> Next |> experiment.Scans.Trigger
            logger.Debug "Emitted scan"
            do! Async.Sleep 200
            // trigger Keysight
            logger.Debug "Triggering sweep"
            do! N5172B.Basic.trigger keysight
//            let data = data |> Observable.observeOn System.Reactive.Concurrency.CurrentThreadScheduler.Instance
//            Observable.scanInit 0 (fun t _ -> t+1) data |> Observable.add (sprintf "Point no: %d" >> logger.Debug)
//            Observable.count data |> Observable.add (sprintf "captured %d points" >> logger.Debug)
//            logger.Debug "Waiting"
//            Observable.last data |> Observable.wait |> ignore
//                let waitUntilFinished = c |> Observable.map ignore |> Observable.wait
//            logger.Debug "Scan iteration completed"
//                do! waitUntilFinished
            }


    let agentLoop (inbox:MailboxProcessor<ScanChange>) =
        let rec loop (state:ScanState) = async {
            let! msg = inbox.Receive()

            match msg with
            | Start parameters ->
                match state with
                | Idle ->
                    let! lease = connectionManager.RequestInstrumentsWithTimeout (set [ detectionInstrument; rfSource ]) Active 1000
                    match lease with
                    | None ->
                        logger.Error "Failed to get instruments for CW scan"
                        return! loop Idle
                    | Some lease ->
                        let experiment : Experiment = {
                            Parameters = parameters
                            Instruments = lease
                            Scans = new NotificationEvent<ScanOutput>()
                            StopNow = new CancellationTokenSource()
                            StopAfterScan = new CancellationTokenSource() }
                        updateStatus "Preparing"

                        let a = experiment.Scans.Publish |> Observable.fromNotificationEvent
                        (parameters,a) |> Next |> experimentOutput.Trigger
                        Async.StartWithContinuations( async { do! Async.SwitchToNewThread()
                                                              logger.Debug "Setting up experiment"
                                                              do! setupKeysight experiment },
                                                     (fun _ -> logger.Info "Ready to start scans"
                                                               Prepared |> inbox.Post),
                                                     (fun exn -> exn |> sprintf "Scan setup failed with error %A" |> logger.Error
                                                                 Stop |> inbox.Post ),
                                                     (fun ce  -> logger.Info "Scan setup cancelled"
                                                                 Stop |> inbox.Post ),
                                                     experiment.StopNow.Token)
                        return! loop <| Preparing experiment
                | _ -> logger.Warn "Attempt to start a new experiment when one is in progress"
                       return! loop state
            | Prepared ->
                match state with
                | Preparing experiment -> logger.Info "Experiment set up"
                                          RunScan |> inbox.Post
                                          updateStatus "Ready"
                                          return! loop <| Ready experiment
                | _ -> sprintf "Attempt mark experiment prepared when in state %A" state |> logger.Warn
                       return! loop state

            | RunScan ->
                match state with
                | Ready experiment ->
                    logger.Info "Scan requested"
                    if experiment.StopAfterScan.IsCancellationRequested || experiment.StopNow.IsCancellationRequested then
                        logger.Info "Not starting scan as this run has been cancelled"
                        updateStatus "Not starting a new scan"
                        Stop |> inbox.Post
                        return! loop state
                    else
                        // start scan
                        logger.Info "Running scan"
                        updateStatus "Running scan"
                        do! runScan experiment
                        return! loop (Running experiment)
                | _ ->
                    sprintf "Attempt to run a scan when in state %A" state |> logger.Warn
                    return! loop state

            | ScanCompleted ->
                match state with
                | Running experiment ->
                    RunScan |> inbox.Post
                    updateStatus "Scan completed"
                    return! loop (Ready experiment)
                | _ ->
                    sprintf "Scan completed received when in state %A" state |> logger.Warn
                    return! loop state

            | IsRunning rc ->
                match state with
                | Preparing experiment
                | Ready experiment
                | Running experiment -> rc.Reply true
                | _ -> rc.Reply false
                return! loop state

            | CancelImmediately ->
                match state with
                | Preparing experiment
                | Ready experiment
                | Running experiment -> experiment.StopNow.Cancel(); updateStatus "Cancelling"; return! loop state
                | _ -> return! loop state

            | StopAfterScan ->
                match state with
                | Preparing experiment
                | Ready experiment
                | Running experiment -> experiment.StopAfterScan.Cancel(); updateStatus "Stopping"; return! loop state
                | _ -> return! loop state

            | Stop ->
                let cleanup experiment = async {
                    let keysight = Instruments.getRfSource "Keysight" experiment.Instruments
                    do! N5172B.Basic.setOutput Off keysight
                    do! N5172B.Sweep.Runtime.abort keysight
                    experiment.Instruments.Release() }
                match state with
                | Preparing experiment
                | Ready experiment
                | Running experiment ->
                        Async.StartWithContinuations( async { do! Async.SwitchToNewThread()
                                                              logger.Debug "Stopping experiment"
                                                              do! cleanup experiment },
                                                     (fun _ -> logger.Info "Stopped scans cleanly"
                                                               Stopped |> inbox.Post ),
                                                     (fun exn -> exn |> sprintf "Stop scans errored %A" |> logger.Error
                                                                 Stopped |> inbox.Post ),
                                                     (fun ce  -> logger.Warn "Stop scans cancelled unexpectedly"
                                                                 Stopped |> inbox.Post ))
                        updateStatus "Stopping"
                        return! loop Stopping
                | _ ->  logger.Debug "Stop requested when not running"
                        return! loop state
            | Stopped ->
                match state with
                | Stopping -> updateStatus "Stopped"; return! loop Idle
                | _ -> sprintf "Attempted to mark as stopped when in state %A" state |> logger.Warn
                       return! loop state
            | Parameters rc ->
                match state with
                | Preparing experiment
                | Ready experiment
                | Running experiment -> experiment.Parameters |> Some |> rc.Reply; return! loop state
                | _ -> None |> rc.Reply; return! loop state }

        loop Idle

    let agent = MailboxProcessor.Start agentLoop

    member __.Start parameters = Start parameters |> agent.Post
    member __.StopNow() = CancelImmediately |> agent.Post; Stop |> agent.Post
    member __.StopAfterScan() = StopAfterScan |> agent.Post
    member __.ScanCompleted() = ScanCompleted |> agent.Post
    member __.RunningParameters() = Parameters |> agent.PostAndReply
    member __.Experiments() = experimentOutput.Publish |> Observable.fromNotificationEvent
    member __.Status() = statusChange.Publish |> Observable.fromNotificationEvent
    member __.IsRunning() = IsRunning |> agent.PostAndReply





























type CwOdmrVM(connectionManager : ConnectionManager.ConnectionManager<InstrumentId>) as self =
    inherit ViewModelBase()
    
    let logger = log4net.LogManager.GetLogger "cwODMR"
    let ui = System.Threading.SynchronizationContext.Current
    let parameters = self.Factory.Backing(<@ self.Parameters @>, CwOdmr.defaultParameters)
    let status = self.Factory.Backing(<@ self.Status @>, "")
    let cwOdmrAgent = new CwOdmrAgent(connectionManager)

    let frequencyInGHz (f:Frequency) = f |> decimal |> System.Decimal.ToDouble |> (*) 1e-9

    let zeroArray parameters =
        let f = CwOdmr.frequencies parameters
        f |> Array.map (fun f -> (f,0.0</s>))

    let zeroDataPoints = CwOdmr.frequencies >> Array.map (fun f -> new DataPoint(frequencyInGHz f, 0.0))

    let initPlotModel parameters =
        let plotModel = new OxyPlot.PlotModel()
        let xAxis = new OxyPlot.Axes.LinearAxis()
        xAxis.Title <- "Microwave frequency (GHz)"
        xAxis.Position <- OxyPlot.Axes.AxisPosition.Bottom
        xAxis.Minimum <- CwOdmr.defaultParameters.StartFrequency |> frequencyInGHz
        xAxis.Maximum <- CwOdmr.defaultParameters.EndFrequency |> frequencyInGHz
        let yAxis = new OxyPlot.Axes.LinearAxis()
        yAxis.Title <- "Fluorescence (counts/s)"
        yAxis.Position <- OxyPlot.Axes.AxisPosition.Left
        yAxis.Minimum <- 0.0
        plotModel.Axes.Add(xAxis)
        plotModel.Axes.Add(yAxis)

//        let initialData = zeroDataPoints parameters
//
        let liveSeries = new OxyPlot.Series.LineSeries()
        liveSeries.Color <- OxyColors.Orange
        liveSeries.StrokeThickness <- 1.0
        liveSeries.Title <- "Current scan"
//        liveSeries.Points.AddRange (zeroDataPoints parameters)

        let accumulatedSeries = new OxyPlot.Series.LineSeries()
        accumulatedSeries.Color <- OxyColors.Navy
        accumulatedSeries.Title <- "Accumulated"
        accumulatedSeries.StrokeThickness <- 1.0
        accumulatedSeries.Points.AddRange (zeroDataPoints parameters)
        
        plotModel.Series.Add liveSeries
        plotModel.Series.Add accumulatedSeries

        plotModel

    let mutable plotModel = initPlotModel CwOdmr.defaultParameters

    let liveScanSeries() = plotModel.Series.Item(0) :?> OxyPlot.Series.LineSeries
    let resetPlot parameters = plotModel <- initPlotModel parameters
    let resetLiveScan() =  liveScanSeries().Points.Clear()
                           self.UpdatePlot()

    let setupPlot (parameters:ScanParameters) =
        let xaxis = plotModel.Axes.Item(0) :?> OxyPlot.Axes.LinearAxis
        xaxis.Minimum <- parameters.StartFrequency |> frequencyInGHz
        xaxis.Maximum <- parameters.EndFrequency |> frequencyInGHz

    let addLivePoint (frequency,intensity:Intensity) =
        let count = liveScanSeries().Points.Count
        let f = frequencyInGHz frequency
//        sprintf "Setting %d of %d to %f %f" i count f (float intensity) |> logger.Debug
        liveScanSeries().Points.Add <| new DataPoint(frequencyInGHz frequency,float intensity)
        self.UpdatePlot()

    let updateAccumulated (data:(Frequency*RunningStatistics)[]) =
        let accumulatedSeries = plotModel.Series.Item(1) :?> OxyPlot.Series.LineSeries
        accumulatedSeries.Points.Clear()
        logger.Debug "Updating accumulated"
        data |> Array.map (fun (f,a) -> new DataPoint(frequencyInGHz f,a.Mean)) |> accumulatedSeries.Points.AddRange
        self.UpdatePlot()

   
    let experiments = cwOdmrAgent.Experiments() |> Observable.observeOnContext ui |> Observable.observeOn System.Reactive.Concurrency.NewThreadScheduler.Default |> Observable.publish
    let scans = experiments |> Observable.map (snd >> Observable.onErrorConcat Observable.empty) |> Observable.mergeInner
    let pointCounts = scans |> Observable.map Observable.count |> Observable.mergeInner
    let scansWithParameters = experiments |> Observable.map (fun (parameters,scans) -> Observable.map (fun scan -> (parameters,scan)) (Observable.onErrorConcat Observable.empty scans)) |> Observable.mergeInner
    let completedScans' = experiments |> Observable.map (snd >> Observable.map (Observable.catch Observable.empty >> Observable.toArray))
    let completedScans = completedScans' |> Observable.map Observable.concatInner
    let scanCompleted = completedScans' |> Observable.map (Observable.map (Observable.map ignore) >> Observable.concatInner)
    let toDataPoint (f,i) = new OxyPlot.DataPoint(frequencyInGHz f,float i)
    let intensityScanToStatistics = Array.map (fun (f:Frequency,x:Intensity) -> (f,RunningStatistics([float x])))
    let statisticsToIntensityScan = Array.map (fun (f,x:RunningStatistics) -> (f,x.Mean |> Acquisition.floatToIntensity))
    let blankScan n : RunningStatistics[] = Array.zeroCreate n
    let accumulateScan (a:(Frequency*RunningStatistics)[]) (b:(Frequency*RunningStatistics)[]) =
        a |> Array.mapi (fun i (f,x) -> (f,x + (b.[i] |> snd)))
    let accumulatedScans = completedScans
                            |> Observable.map (Observable.map intensityScanToStatistics)
                            |> Observable.map (Observable.scan accumulateScan)


    let todBm = LanguagePrimitives.DecimalWithMeasure<dBm>

//    let scanStarting parameters trigger ctsStop ctsStopAfterScan =
//        runningScan <- Some (parameters,trigger,ctsStop,ctsStopAfterScan)
//        self.UpdateRunning()
//        resetPlot parameters

//
//        let runScan() =
//            Async.StartWithContinuations( scanWorkflow,
//                                          (fun _ -> logger.Info "Scan finished"),
//                                          (fun exn -> exn |> sprintf "Scan errored %A" |> logger.Error;
//                                                      Error exn |> odmrRun.Trigger; scanCleanup lease ),
//                                          (fun ce -> logger.Info "Scan cancelled";
//                                                     Error ce |> odmrRun.Trigger; scanCleanup lease ))
//                                          
//        scanStarting self.Parameters runScan stopNow stopAfterScan
//        
//        let scansWorkflow = async {
//            while not stopAfterScan.Token.IsCancellationRequested do
//                logger.Debug "Running scan"
//                do! scanWorkflow
//            scanCleanup lease }
//
//        do! scansWorkflow
//        
//        } 
//

//        while (not stopAfterScan.IsCancellationRequested) do
////             Get an observable of N counts
////             Emit scan sequence
//            let timeDelay = Observable.interval (TimeSpan.FromMilliseconds 10.0)
//            let rnd = System.Random()
//
//
//            let mockData = frequencies parameters |> Array.mapi (fun i f -> (f,i%50 + rnd.Next(20) |> Acquisition.intToIntensity)) |> Observable.ofSeq
//            let tmp : Scan = Observable.zip timeDelay mockData |> Observable.map snd
//            let waitUntilFinished = tmp |> Observable.last |> Observable.map ignore |> Async.AwaitObservable
//            wait until scan completed
//            do! data |> Observable.take (parameters.Points-5) |> Observable.last |>  Observable.map ignore |> Async.AwaitObservable
//            logger.Debug "Finished scan"
//            do! Async.Sleep 200
//        logger.Debug "Stopped after scan on request" }

        // repeat unless cancelled

//    let runOdmr ui = async {
//        let! lease = connectionManager.RequestInstrumentsWithTimeout (set [ detectionInstrument; rfSource ]) Active 1000
//        match lease with
//        | None -> failwith "Failed to get instruments for CW scan"
//        | Some lease ->
//
//            let stopNow = new CancellationTokenSource()
//            let stopAfterScan = new CancellationTokenSource()
//
//            let odmrRun = buildScanSetup self.Parameters lease stopNow stopAfterScan
//
//            Async.StartWithContinuations( async { do! Async.SwitchToNewThread()
//                                                  logger.Debug "Setting up experiment"
//                                                  do! odmrRun },
//                                         (fun _ -> logger.Info "Setup finished"),
//                                         (fun exn -> exn |> sprintf "Scan errored %A" |> logger.Error; scanCleanup lease ),
//                                         (fun ce -> logger.Info "Scan cancelled"; scanCleanup lease )) }
//

    let seqToArrays (zeroArray:'a[]) (o:IObservable<'a>) =
        o |> Observable.take zeroArray.Length
          |> Observable.mapi (fun i x -> sprintf "ix: %d" i |> logger.Debug; (i,x))
          |> Observable.scanInit zeroArray (fun acc (i,x) -> sprintf "Set %d to %A. Length %d" i x acc.Length |> logger.Debug; acc.[i] <- x; Array.copy acc)

    let experimentOutputConnection =

        // Update live scan
        scansWithParameters |> Observable.add (fun (p,s) -> sprintf "Scan with %d points" p.Points |> logger.Debug)
        let latestPoint = scansWithParameters |> Observable.map (fun (parameters,scans) -> scans |> Observable.mapi (fun i (f,intensity) -> (i,parameters,(f,intensity)))) |> Observable.mergeInner
        latestPoint |> Observable.add (fun (i,parameters,(frequency,intensity)) ->
                                                if i = 0 then resetLiveScan()
                                                addLivePoint (frequency,intensity))

        // Update accumulated
        accumulatedScans |> Observable.mergeInner |> Observable.add updateAccumulated
        scanCompleted |> Observable.mergeInner |> Observable.add (fun _ -> logger.Debug "Scan finished"; cwOdmrAgent.ScanCompleted() )
        scans |> Observable.map (Observable.scanInit 0 (fun t _ -> t+1)) |> Observable.mergeInner
              |> Observable.skip 1990
              |> Observable.add (sprintf "Point no: %d" >> logger.Debug)
        pointCounts |> Observable.add (sprintf "Received %d measurements" >> logger.Info)
        let scanNumber = completedScans |> Observable.map (Observable.scanInit 0 (fun n _ -> n+1)) |> Observable.mergeInner
        scanNumber |> Observable.add (sprintf "Scan no. %d" >> logger.Debug)
        cwOdmrAgent.Status() |> Observable.observeOnContext SynchronizationContext.Current
                             |> Observable.add (fun status -> self.UpdateRunning(); self.Status <- status)

        Observable.connect experiments

    let startScanning() = self.Parameters |> cwOdmrAgent.Start
    let stopScanningNow() = cwOdmrAgent.StopNow()
    let stopAfterScan() = cwOdmrAgent.StopAfterScan()

    member __.Parameters with get() = parameters.Value and set v = parameters.Value <- v
    member this.Power with get() = decimal this.Parameters.Power and set v = this.Parameters <- {this.Parameters with Power = todBm v}
    member this.StartFrequency with get() = this.Parameters.StartFrequency / (1e9m<Hz>) and set v = this.Parameters <- {this.Parameters with StartFrequency = v * (1e9m<Hz>)}
    member this.EndFrequency with get() = this.Parameters.EndFrequency / (1e9m<Hz>) and set v = this.Parameters <- {this.Parameters with EndFrequency = v * (1e9m<Hz>)}
    member this.Points with get() = this.Parameters.Points and set v = this.Parameters <- {this.Parameters with Points = v}
    member this.IntegrationTime with get() = this.Parameters.IntegrationTime * 1</ms> and set v = this.Parameters <- {this.Parameters with IntegrationTime = v * 1<ms>}
    member __.PlotModel with get() = plotModel and set v = plotModel <- v
    member __.Status with get() = status.Value and set v = status.Value <- v

    member this.UpdateRunning() = this.RaisePropertyChanged(<@ this.Start @>)
                                  this.RaisePropertyChanged(<@ this.Stop @>)
                                  this.RaisePropertyChanged(<@ this.StopAfterScan @>)
    member this.UpdatePlot() = plotModel.InvalidatePlot(true); this.RaisePropertyChanged(<@ this.PlotModel @>)

    member __.Start = self.Factory.CommandSyncChecked(startScanning, (fun _ -> (not <| cwOdmrAgent.IsRunning()) ))
    member __.Stop = self.Factory.CommandSyncChecked(stopScanningNow, (fun _ -> cwOdmrAgent.IsRunning() ))
    member __.StopAfterScan = self.Factory.CommandSyncChecked(stopAfterScan, (fun _ -> cwOdmrAgent.IsRunning() ))
