// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace PhotonDetection

open SwabianInstruments.TimeTagger
open Endorphin.Instrument.PicoQuant.PicoHarp300
open Endorphin.Instrument.Warwick.PhotonCounter
open Endorphin.Instruments
open System
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols

open FSharp.Control.Reactive

// interface for implementations that yield a window of live counts
// time per bin, number of bins, frequency of updates
// An implementation may be running a background async to resubscribe to the count source
// if it becomes unavailable but an implementation will only emit the latest counts 
type ILiveCounts =
    abstract LiveCounts : int -> float<ms> -> IObservable<float[]>

// interface that takes the number of counts expected and a frequency of updates and gives
// a finite observable sequence of count rates
// There may be several of these finite-sequence scans
type IScan =
    // number of measurements, frequency of 
    abstract SingleScan    : int -> IObservable<float>
    abstract RepeatingScan : int -> IObservable<float>

type TimeTagSource(liveCounts, countSequence) =
    let liveCountsSource = liveCounts :> ILiveCounts
    let countSequenceSource = countSequence :> ICountSequence

    member __.LiveCounts = liveCountsSource.LiveCounts
    member __.SingleCountSequence n = countSequenceSource.SingleCountSequence n
    member __.RepeatingCountSequence n = countSequenceSource.RepeatingCountSequence n


type TTSource = {
    LiveCounts             : int -> float<ms> -> IObservable<float[]>
    Scans
    
     }