// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace PhotonDetection

module TimeTagger =

    open SwabianInstruments.TimeTagger
    open System
    open FSharp.Control.Reactive

    type TimeTagger(serial : string, clickChannels : uint32[]) =

        let tt = TT.createTimeTagger serial

        let combiner = new Combiner(tt,clickChannels)
        let combinedChannel = combiner.getChannel()

        do combiner.start()
        member __.TimeTagger = tt
        member __.CombinedClicks = combinedChannel

        interface IDisposable with
            member __.Dispose() =
                combiner.stop()
                combiner.Dispose()
                TT.freeTimeTagger tt |> ignore

        interface ILiveCounts with
            member __.LiveCounts n bintime =
                let liveCounter = new SwabianInstruments.TimeTagger.Counter(tt,[|combinedChannel|],w,n)
                liveCounter.start()

                Observable.interval (TimeSpan.FromSeconds 0.2)
                |> Observable.map (fun _ -> liveCounter.getData().[0,0..n-1] |> Array.map (fun c -> (float c)/(1000. * float bintime)))

        interface ICountSequence with
            member __.SingleCountSequence triggerChannel n =
                let countBetween = new SwabianInstruments.TimeTagger.CountBetweenMarkers(tt,combinedChannel,triggerChannel,triggerChannel,n)
                let triggerCounter = new SwabianInstruments.TimeTagger.Counter(tt,[| triggerChannel |])
                countBetween.start()
                triggerCounter.start()

                Observable.interval inte
                |> Observable.map (fun _ -> triggerCounter.getData().[0,0])
                |> Observable.startWith [0]
                |> Observable.pairwise
                |> Observable.flatmapSeq (fun (lastCount,nextCount) ->
                    let data = countBetween.getData()
                    let width = countBetween.getBinWidths()
                    let countRate c t = (float c) / (float t)
                    Array.map2 countRate data.[lastCount .. nextCount-1] width.[lastCount .. nextCount-1]
                    |> Array.toSeq)
                


    let scanCountRateSequence (tt : SwabianInstruments.TimeTagger.TimeTagger, clickChannels : uint32[], triggerChannel : uint32) =


        member __.CountSequence n (interval:TimeSpan) =
