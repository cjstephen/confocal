// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Connection

open System
open System.Threading
open ViewModule
open ViewModule.FSharp

    // Consumer's end of the lease. The cancellation token is used to signal that the consumer must release
    // the instruments and let another task use them.
    [<NoComparison>]
    type Lease<'T when 'T : comparison>( instruments : Map<'T,IConnectionAgent>, ct : CancellationToken, release : unit -> unit) =
        member __.RevokedToken = ct
        member __.Instruments = instruments
        member __.IssueTime = DateTime.Now
        member __.Release() = release()
        override x.Equals(y) = match y with | :? Lease<'T> as y' -> x.IssueTime = y'.IssueTime && x.Instruments = y'.Instruments | _ -> false
        override x.GetHashCode() = (x.IssueTime,x.Instruments) |> hash
        interface System.IDisposable with
          member __.Dispose() = release() // release the instruments

    [<CustomEquality>]
    [<CustomComparison>]
    type Priority =
        | Passive     // user control, default activities when idle
        | Active      // experiment on a point, confocal scan, shared stream
        | Maintenance // autofocus, callibration, reset
        member x.Rank() = match x with | Passive -> 1 | Active -> 2 | Maintenance -> 3
        override x.Equals(y) = match y with | :? Priority as y' -> x.Rank() = y'.Rank() | _ -> false
        override x.GetHashCode() = x.Rank() |> hash
        interface IComparable with member x.CompareTo y = match y with :? Priority as y' -> compare (x.Rank()) (y'.Rank()) | _ -> 0

    type LeaseRecord<'T when 'T : comparison> = InterruptableLease of Set<'T> * Priority * Threading.CancellationTokenSource * issued : System.DateTime


module ConnectionManager =

    let instrumentAgents agents (instruments:Set<'T>) =
        let unavailable = Set.filter (fun x -> Map.containsKey x agents |> not) instruments
        if Set.isEmpty unavailable then
            Map.filter (fun key _ -> Set.contains key instruments) agents
        else
            failwithf "Missing instruments: %A" unavailable
        
    type ConnectionIndicator(identifier:string, ca:IConnectionAgent) as self =
        inherit ViewModelBase()
        do ca.Event |> Event.add (fun _ -> self.Update())
        member __.Id = identifier
        member __.IsConnected with get() = ca.IsConnected()
        member x.IsDisconnected = not x.IsConnected
        member x.Update() =
            self.RaisePropertyChanged("IsConnected")
            self.RaisePropertyChanged("IsDisconnected")

    type Request<'T when 'T : comparison> =
        | RequestLease of AsyncReplyChannel<Lease<'T> option> * Set<'T> * Priority     // may yield None or a delayed Some
        | Release of LeaseRecord<'T>

    type ConnectionManager<'T when 'T : comparison>(agents:Map<'T,IConnectionAgent>) =

        let logName = "InstrumentManager"
        let logger = log4net.LogManager.GetLogger logName

        let leasePriority = function InterruptableLease (_,priority,_,_) -> priority
        let leaseInstruments = function InterruptableLease (instruments,_,_,_) -> instruments
        let giveWay = function InterruptableLease (_,_,cts,_) -> logger.Debug "Giving way"; cts.Cancel()

        let indicators = agents |> Map.map (fun label c -> new ConnectionIndicator(label.ToString(),c))
        do agents |> Map.iter (fun k v -> v.Event.Add (fun e -> sprintf "Connection status change for %A: %A" k e |> logger.Debug; indicators.[k].Update()))

        let handler (mbox:MailboxProcessor<Request<'T>>) =
            let rec loop leases = async {

                // check these instruments exist and are currently connected
                let instrumentsConnected (instruments:Set<'T>) =
                    try (instrumentAgents agents instruments) |> Map.toList |> List.map snd
                        |> List.forall (fun x -> x.IsConnected())
                    with _ -> false

                let instrumentsWhichAreNotConnected (instruments:Set<'T>) =
                    (instrumentAgents agents instruments) |> Map.toList |> List.map snd
                        |> List.filter (fun x -> not <| x.IsConnected())

                // scan for requests to release leases first
                let handleReleases = function
                    | Release lease -> Some <| async {
                        // do release
                        return List.filter ((<>) lease) leases }
                    | _ -> None

                let scanTimeout = 1000

                let! updatedLeases = mbox.TryScan(handleReleases,scanTimeout)
                match updatedLeases with
                | Some leases' -> return! loop leases'
                | None -> ()

                // scan for new requests which can be granted
                let processRequests = function
                | RequestLease (rc,instruments,priority) ->
                    logger.Debug <| sprintf "Requested lease of %A, priority %A" instruments priority
                    if instrumentsConnected instruments then
                        // Check for conflicts with current leases
                        let conflictingLeases = List.filter (fun lease -> Set.intersect (leaseInstruments lease) instruments |> Set.isEmpty |> not) leases
                        let (weakConflicts,strongConflicts) = List.partition (leasePriority >> (>) priority) conflictingLeases
                        if List.isEmpty conflictingLeases then
                            // Issue lease
                            let cts = new CancellationTokenSource()
                            logger.Debug <| "Starting to issue lease"
                            let leaseAgents = instrumentAgents agents instruments
                            let exclusiveInstruments = leaseAgents |> Map.filter (fun k v -> v.IsShareable |> not) |> Map.toList |> List.map fst |> Set.ofList
                            let lease = InterruptableLease <| (exclusiveInstruments,priority,cts,DateTime.Now)
                            let release() = logger.Debug <| sprintf "Releasing %A" lease; Release lease |> mbox.Post
                            new Lease<'T>(leaseAgents,cts.Token,release) |> Some |> rc.Reply
                            logger.Debug <| sprintf "Issuing lease %A" priority
                            Some <| async.Return (lease::leases)
                        else
                            logger.Debug <| sprintf "Conflicts: weak %d strong %d" weakConflicts.Length strongConflicts.Length 
                            if List.isEmpty strongConflicts then
                                // Ask the weaker lease holders to yield until they do
                                logger.Debug "There are weak conflicts, requesting they give way"
                                weakConflicts |> List.iter giveWay
                                None // leave this message on the queue pending availability
                            else
                                // Another task has the instruments at the moment, stay on the queue
                                // If you want to fail instead, set a timeout
                                logger.Debug <| sprintf "There is a strong conflict with %A" strongConflicts
                                None
                    else
                        let notConnected = instrumentsWhichAreNotConnected instruments
                        logger.Debug <| sprintf "Not connected : %A" notConnected
                        None |> rc.Reply
                        Some <| async.Return leases
                | _ -> None

                let! updatedLeases = mbox.TryScan(processRequests,scanTimeout)
                match updatedLeases with
                | Some leases' -> return! loop leases'
                | None -> return! loop leases }

            async { do! Async.SwitchToNewThread()
                    return! loop [] }

        let agent = MailboxProcessor.Start handler

        /// Request handles for a set of instruments. If they're not available and can't be made available in time, return None
        member __.RequestInstrumentsWithTimeout instruments priority timeout = async {
            let! reply = agent.PostAndTryAsyncReply ((fun rc -> RequestLease (rc,instruments,priority)),timeout=timeout)
            let reply' = match reply with
                         | None -> None
                         | Some None -> None
                         | Some (Some x) -> Some x
            logger.Debug <| sprintf "RequestInstrumentWithTimeout returning %A" reply'
            return reply' }

        /// Request handles for a set of instruments. If they're not available and can't be made available, return None
        member __.RequestInstruments instruments priority =
            logger.Debug "Requesting Instruments"
            agent.PostAndAsyncReply (fun rc -> RequestLease (rc,instruments,priority))

        member __.ConnectionIndicators = indicators |> Map.toList |> List.map snd

        member __.ConnectAll() =
            agents |> Map.toList |> List.iter (fun (l,agent) -> logger.Debug <| sprintf "Connecting %A" l; agent.Connect())

        member __.DisconnectAll() =
            agents |> Map.toList |> List.iter (fun (l,agent) -> logger.Debug <| sprintf "Disconnectin %A" l; agent.Disconnect())
