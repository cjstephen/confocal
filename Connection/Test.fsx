// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

#r "./bin/Debug/Endorphin.Connection.dll"
#r "./bin/Debug/FSharp.Configuration.dll"

open Endorphin.Connection

open FSharp.Configuration

module Testing =

    // Serves as a schema to ensure sensible types for each parameter
    [<Literal>]
    let ConfigTemplate = """
Thing:
  - id: string
Whatsit:
  - id: string
Shareable:
  - id: string
"""

    let configBlank = """
Thing: []
Whatsit: []
Shareable: []"""

    let configTxt = """
Thing:
  - id: A
Whatsit:
  - id: C
Shareable:
  - id: X
"""


    type InstrumentConfig = YamlConfig<YamlText=ConfigTemplate>
    let baseConfiguration = new InstrumentConfig()
    baseConfiguration.LoadText(configBlank)

    let loadConfiguration (filename:string) =
        let config = new InstrumentConfig()
        config.LoadText(configBlank)
        config.Load(filename)
        config

    type Thing = unit
    type Whatsit = unit
    type Shareable = unit

    type InstrumentId =
        | ThingId of id : string
        | WhatsitId of id : string
        | ShareableId of id : string
        override x.ToString() = match x with
                                | ThingId id
                                | ShareableId id
                                | WhatsitId id -> id

    let configureInstruments (configuration:InstrumentConfig) =

        let thingAgent name = new ConnectionAgent<Thing>( name,
                                                         async {()},
                                                         (fun _ -> async {()})) :> IConnectionAgent
        let things = configuration.Thing
                     |> Seq.map (fun conf -> (ThingId conf.id, thingAgent conf.id))

        let whatsitAgent name = new ConnectionAgent<Whatsit>( name,
                                                              async {()},
                                                              (fun _ -> async {()})) :> IConnectionAgent
        let whatsits = configuration.Whatsit
                       |> Seq.map (fun conf -> (WhatsitId conf.id, whatsitAgent conf.id))

        let shareableAgent name = new ConnectionAgent<Shareable>( name,
                                                              async {()},
                                                              (fun _ -> async {()})) :> IConnectionAgent
        let shareables = configuration.Shareable
                       |> Seq.map (fun conf -> (ShareableId conf.id, shareableAgent conf.id))

        [ things; whatsits; shareables] |> Seq.concat |> Map.ofSeq

    let accessThing x (lease:Lease<InstrumentId>) = try (lease.Instruments.[ThingId x] :?> ConnectionAgent<Thing>).TryConnection() with | _ -> failwithf "Thing %s not found" x
    let accessWhatsit x (lease:Lease<InstrumentId>) = try (lease.Instruments.[WhatsitId x] :?> ConnectionAgent<Whatsit>).TryConnection() with | _ -> failwithf "Whatsit %s not found" x
    let accessShareable x (lease:Lease<InstrumentId>) = try (lease.Instruments.[ShareableId x] :?> ConnectionAgent<Shareable>).TryConnection() with | _ -> failwithf "Whatsit %s not found" x

    let connectionManager configuration =
        let agents = configureInstruments configuration
        new ConnectionManager.ConnectionManager<InstrumentId>(agents)

    let getInstruments (connectionManager:ConnectionManager.ConnectionManager<InstrumentId>) instruments priority = async {
        let! lease = connectionManager.RequestInstruments instruments priority
        let lease = match lease with
                    | None -> failwithf "Unable to get instruments: %A" instruments
                    | Some lease -> lease
        return lease }

    let getInstrumentsWithTimeout (connectionManager:ConnectionManager.ConnectionManager<InstrumentId>) instruments priority = async {
        return! connectionManager.RequestInstrumentsWithTimeout instruments priority 50 }

    let config =
        let config = new InstrumentConfig()
        config.LoadText(configBlank)
        config.LoadText(configTxt)
        config

    let connectionManager' = connectionManager config
    connectionManager'.ConnectAll()

    let rec foo (a:Lease<InstrumentId>) = async {
        if a.RevokedToken.IsCancellationRequested then
            printfn "Revoked!"
            return ()
        else
            printfn "Doing thing"
            do! Async.Sleep 1000
            do! foo a }

    let dothing = async {
        while true do
            use! a = getInstruments connectionManager' (set [ThingId "A"; WhatsitId "C";ShareableId "X"]) Priority.Passive
            do! foo a }

    let doShareableThing = async {
        do! Async.Sleep 1200
        printfn "Trying shareable thing"
        use! q = getInstruments connectionManager' (set [ShareableId "X"]) Priority.Passive
        let q' = accessShareable "X" q
        printfn "Done shareable thing" }

    let dootherthing = async {
        do! Async.Sleep 1600
        printfn "Trying to preempt"
        use! a = getInstruments connectionManager' (set [ThingId "A"; WhatsitId "C"]) Priority.Active
        let c = accessWhatsit "C" a
        printfn "Doing other thing"
        do! Async.Sleep 4000
        printfn "Done other thing"}
    
    let tryToDoThingWithTimeout = async {
        do! Async.Sleep 2000
        printfn "Trying thing with timeout"
        for i in [1..5] do
            let! q = getInstrumentsWithTimeout connectionManager' (set [ThingId "A"]) Priority.Passive
            match q with | None -> printfn "Timed out" | Some _ -> printfn "Granted" }
        

    Async.Start dothing
    Async.Start tryToDoThingWithTimeout
    Async.StartImmediate doShareableThing
    Async.StartImmediate dootherthing
