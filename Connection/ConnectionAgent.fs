// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Connection

open System.Threading

// Agent to connect to instruments and manage access to them

[<AutoOpen>]
module ConnectionAgent =

    // TODO: Handle allocation of connections, release, interruptions, leases, contracts

    type ConnectionNotification =
        | HasConnected
        | HasDisconnected
        | FailedToConnect of exn
        | FailedToDisconnect of exn

    type private Request<'T> =
        | Connect
        | Disconnect
        | DisconnectSuccessful
        | GetConnection of AsyncReplyChannel<'T option>
        | IsConnected of AsyncReplyChannel<bool>

    type private State<'T> =
        | Connected of 'T
        | Disconnecting of 'T
        | Disconnected

    type IConnectionAgent =
        abstract member Connect : unit -> unit
        abstract member Disconnect : unit -> unit
        abstract member IsConnected : unit -> bool
        abstract member IsShareable : bool
        abstract member Event : IEvent<ConnectionNotification>

    type ConnectionAgent<'T>(name,connect,disconnect,?shareable,?notificationContext:SynchronizationContext) as this =

        let logName = sprintf "Connection %A" <| typeof<'T>
        let logger = log4net.LogManager.GetLogger logName


        let notifier = new Event<ConnectionNotification>()
        let notify msg = match notificationContext with
                         | None -> msg |> notifier.Trigger
                         | Some cxt -> cxt.Post((fun _ -> msg |> notifier.Trigger), null)

        let messageHandlingLoop (inbox:MailboxProcessor<Request<'T>>) =
            let rec loop state = async {
                let! msg = inbox.Receive()
                match msg with
                | Connect ->
                    match state with
                    | Disconnected ->
                        sprintf "Attempting to connect to %s" name |> logger.Info
                        try
                            let! connection = connect
                            HasConnected |> notify
                            return! loop (Connected connection)
                        with error ->
                            sprintf "Failed to connect to %s: %s" name error.Message |> logger.Error
                            FailedToConnect error |> notify
                            return! loop Disconnected
                    | Disconnecting _
                    | Connected _ -> return! loop state
                | Disconnect ->
                    match state with
                    | Disconnected -> return! loop state
                    | Disconnecting connection
                    | Connected connection ->
                        sprintf "Disconnecting from %A" name |> logger.Info
                        async {
                            try
                                do! disconnect connection
                                HasDisconnected |> notify
                                inbox.Post DisconnectSuccessful
                            with error ->
                                sprintf "Failed to disconnect %A: %s" name error.Message |> logger.Error } |> Async.Start

                        return! loop <| Disconnecting connection
                | DisconnectSuccessful ->
                    return! loop Disconnected
                | GetConnection rc ->
                    let connection = match state with Connected c -> Some c | _ -> None
                    connection |> rc.Reply
                    return! loop state
                | IsConnected rc ->
                    rc.Reply <| match state with | Connected _ -> true | _ -> false
                    return! loop state }
            loop Disconnected

        let agent = MailboxProcessor.Start messageHandlingLoop

        member __.Connect()       = Connect       |> agent.Post
        member __.Disconnect()    = Disconnect    |> agent.Post
        member __.IsConnected()   = IsConnected   |> agent.PostAndReply
        member __.IsShareable     = Option.exists id shareable
        member __.Event = notifier.Publish
        member __.Connection()    = GetConnection |> agent.PostAndReply
        member x.TryConnection()  = match x.Connection() with Some connection -> connection | None -> failwithf "Not connected"

        interface IConnectionAgent with
            member __.Connect()       = this.Connect()
            member __.Disconnect()    = this.Disconnect()
            member __.IsConnected()   = this.IsConnected()
            member __.IsShareable     = this.IsShareable
            member __.Event           = this.Event


