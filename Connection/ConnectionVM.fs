// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Connection

open ViewModule
open ViewModule.FSharp

open FsXaml
open System.Collections.ObjectModel
open ConnectionManager

[<AbstractClass>]
type ConnectionViewModel() as self =
    inherit ViewModelBase()

    // Backing plumbs Parameters into WPF, validation and notifications using a quotation expression as a sort of reflection
    let isConnected    = self.Factory.Backing(<@ self.IsConnected @>, self.IsConnected)
    let isDisconnected = self.Factory.Backing(<@ self.IsDisconnected @>, not self.IsConnected)

    // The context needs to supply where the state comes from
    abstract IsConnected : bool
    member x.Update() =
        self.RaisePropertyChanged("IsConnected")
        self.RaisePropertyChanged("IsDisconnected")

    // Accessor methods providing bindings for each field
    member x.IsDisconnected = not x.IsConnected

type ConnectionsViewModel<'T when 'T : comparison>(connectionManager:ConnectionManager<'T>) =
    inherit ViewModelBase()
    member x.ConnectionIndicators =
        let a = connectionManager.ConnectionIndicators
        printfn "A: %d" a.Length
        a

type ConnectionView = XAML<"Connection.xaml">
type ConnectionsView = XAML<"Connections.xaml">
    
[<AutoOpen>]
module Connection =

    // Set up connections to instruments and associated connection manager and view model
    let connectionViewModel (src:ConnectionAgent<_>) = { new ConnectionViewModel()
                                                         with override x.IsConnected = src.IsConnected() }

    let connectionsViewModel<'T when 'T : comparison> (connectionManager:ConnectionManager<'T>) = new ConnectionsViewModel<'T>(connectionManager)