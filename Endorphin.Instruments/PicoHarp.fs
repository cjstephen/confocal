// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Instruments

open Endorphin.Instrument.PicoQuant
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.Control.Reactive
open System
open Endorphin.Core

// In addition to initialising a PicoHarp, create a time tag stream and keep emitting until stopped
// Other integrations are projections
module PicoHarp =

    type PicoHarpHandle = {
        PicoHarp : PicoHarp300.Model.PicoHarp300
        Acquisition : PicoHarp300.Streaming.StreamingAcquisition
        AcquisitionHandle : PicoHarp300.Streaming.StreamingAcquisitionHandle }

    let startPicoHarpStreamingAcquisition serial = async {
        let! picoHarp = PicoHarp300.PicoHarp.Initialise.initialise serial PicoHarp300.Model.SetupInputChannel.T2
        let streamingAcquisition = PicoHarp300.Streaming.Acquisition.create picoHarp
        let streamingAcquisitionHandle = PicoHarp300.Streaming.Acquisition.start streamingAcquisition
        return { PicoHarp = picoHarp
                 Acquisition = streamingAcquisition
                 AcquisitionHandle = streamingAcquisitionHandle } }

    let stopPicoHarpStreamingAcquisition (handle:PicoHarpHandle) = async {
        PicoHarp300.Streaming.Acquisition.stop handle.AcquisitionHandle
        do! PicoHarp300.PicoHarp.Initialise.closeDevice handle.PicoHarp }


    // Just a projection of the time tag observable
    let scan (picoHarpHandle:PicoHarpHandle) integrationTime numberOfPoints =

        // set picoharp on a long measurement
        // resolution = total time, so integrate into one bin for each point delineated by the marker
        let histogramParameters = PicoHarp300.Streaming.Acquisition.Histogram.Parameters.create (PicoHarp300.Model.Duration_ms integrationTime)
                                                                                                (PicoHarp300.Model.Duration.Duration_ms integrationTime)
                                                                                                PicoHarp300.Model.TTTRConfiguration.Marker2
        let histogramAcquisition = PicoHarp300.Streaming.Acquisition.Histogram.create picoHarpHandle.Acquisition histogramParameters

        // when new histogram is available, map it into a count rate
        PicoHarp300.Streaming.Acquisition.Histogram.HistogramsAvailable histogramAcquisition
        |> Observable.map (fun histogram ->
                                match histogram.Histogram with
                                | [(binNumber, intensity)] ->
                                    (float intensity) / (integrationTime * 1.e-3<s/ms>)
                                | [] -> 0.</s>
                                | _ -> raise (new Exception(sprintf "PicoHarp streaming histogram in a format other than (bin, intensity) - %A" histogram.Histogram)))
        |> Observable.take numberOfPoints

    let liveCounts (picoHarpHandle:PicoHarpHandle) =
            let liveCountAcquisition = PicoHarp300.Streaming.Acquisition.LiveCounts.create picoHarpHandle.Acquisition
            let normalizationFactor = (1./0.08)
            PicoHarp300.Streaming.Acquisition.LiveCounts.LiveCountAvailable liveCountAcquisition
            |> Observable.map (fun (ch0, ch1) -> ((float ch0) * normalizationFactor) * 0.1</s>, ((float ch1) * normalizationFactor) * 0.1</s>)


        // drop the wait to finish -> async
        // why not wait on the subscribed observable completing ?
        // zip the output with the position information