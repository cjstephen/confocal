// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Instruments

open Endorphin.Connection
open Endorphin.Instrument.FastScanningController
open Endorphin.Instrument.PicoQuant
open Endorphin.Instrument.FastScanningController.Instrument
open Endorphin.Instrument.Warwick.PhotonCounter
open Endorphin.Instrument.Keysight.N5172B
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open Endorphin.Instrument.RohdeSchwarz.HMC804x
//open Endorphin.Instrument.PicoTech.PicoScope3000
open SwabianInstruments.TimeTagger
open Endorphin.Instrument.Attocube.ANC300
open System.Windows

open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols.Units.Time
open FSharp.Data.UnitSystems.SI.UnitSymbols

open FSharp.Configuration

type InstrumentId =
    | ScanningControllerId of id : string
    | RfSourceId of id : string
    | PulseStreamerId of id : string
    | TimeTaggerId of id : string
    | PicoHarpId of id : string
    | PhotonCounterId of id : string
    | CurrentSourceId of id : string
    | AttocubeId of id : string
    override x.ToString() = match x with
                            | ScanningControllerId id
                            | PicoHarpId id
                            | RfSourceId id
                            | PulseStreamerId id
                            | TimeTaggerId id
                            | PhotonCounterId id
                            | CurrentSourceId id
                            | AttocubeId id -> id


module Configuration =


    // Serves as a schema to ensure sensible types for each parameter
    [<Literal>]
    let ConfigTemplate = """
RfSource:
  - id: string
    address : TCPIP0::192.168.1.101::inst0::INSTR
PhotonCounter:
  - id: string
    address : 192.168.1.1
PicoHarp:
  - id: string
    serial: string
ScanningController:
  - id: string
    port: COM3
    calibration:
      x: 8.0e-6 # m/V
      y: 8.0e-6
      z: 8.0e-6
    limits:
      x: 4 # V
      y: 4
      z: 4
PulseStreamer:
  - id: string
    address: string
TimeTagger:
  - id: string
    serial: string
HMC804x:
    - id: string
      address: string
ANC300:
    - id: string
      address: string
Detection:
  confocalImaging:
    source: string
    id: string 
"""

    let configBlank = """

RfSource: []
PhotonCounter: []
PicoHarp: []
ScanningController: []
PulseStreamer: []
TimeTagger: []
HMC804x: []
ANC300: []"""


    type InstrumentConfig = YamlConfig<YamlText=ConfigTemplate>
    let baseConfiguration = new InstrumentConfig()
    baseConfiguration.LoadText(configBlank)

    let loadConfiguration (filename:string) =
        let config = new InstrumentConfig()
        config.LoadText(configBlank)
        config.Load(filename)
        config

    let findScanningController identifier (configuration:InstrumentConfig) =
        configuration.ScanningController |> Seq.find (fun x -> x.id = identifier )

    let confocalDetectionInstrument (configuration:InstrumentConfig) =
        match (configuration.Detection.confocalImaging.source).ToLower() with
        | "photoncounter" ->
            let pcid = configuration.Detection.confocalImaging.id
            PhotonCounterId pcid
        | "picoharp" ->
            let phid = configuration.Detection.confocalImaging.id
            PicoHarpId phid
        | "timetagger" ->
            let ttid = configuration.Detection.confocalImaging.id
            TimeTaggerId ttid
        | _ -> failwithf "Confocal imaging detector not configured"


module Instruments =

    let configureInstruments (configuration:Configuration.InstrumentConfig) =

        // Now set up the instrument agents
        let rfSourceAgent name address = new ConnectionAgent<RfSource>( name,
                                                                        Rf.openInstrument address 1000<ms>,
                                                                        Rf.closeInstrument) :> IConnectionAgent
        let rfSources = configuration.RfSource
                        |> Seq.map (fun conf -> (RfSourceId conf.id, rfSourceAgent conf.id conf.address))

        // open any attached PicoHarp
        let picoHarps = configuration.PicoHarp
                        |> Seq.map (fun conf -> (PicoHarpId conf.id,
                                                 new ConnectionAgent<PicoHarp.PicoHarpHandle>(conf.id,
                                                                                              (PicoHarp.startPicoHarpStreamingAcquisition conf.serial),
                                                                                              PicoHarp.stopPicoHarpStreamingAcquisition,
                                                                                              true ) :> IConnectionAgent ))

        // init. fast scanning controller
        let openScanningController port = async { printfn "opening scanning controller"; return! ScanningController.openInstrument port 600000<ms> }
        let closeScanningController = ScanningController.closeInstrument
        let scanningControllers = configuration.ScanningController
                                  |> Seq.map (fun conf -> (ScanningControllerId conf.id,
                                                           new ConnectionAgent<ScanningController>(conf.id,
                                                                                                   (openScanningController conf.port),
                                                                                                   closeScanningController) :> IConnectionAgent ))
    
        let openPulseStreamer address = PulseStreamer.openDevice(address)
        let closePulseStreamer = PulseStreamer.close
        let pulseStreamers = configuration.PulseStreamer
                             |> Seq.map (fun conf -> (PulseStreamerId conf.id,
                                                      new ConnectionAgent<PulseStreamer8>(conf.id,
                                                                                         (openPulseStreamer conf.address),
                                                                                         closePulseStreamer) :> IConnectionAgent ))

        let openTimeTagger serial = async { return TT.createTimeTagger serial }
        let closeTimeTagger tt = async { return TT.freeTimeTagger tt |> ignore }
        let timeTaggers = configuration.TimeTagger
                          |> Seq.map (fun conf ->
                                        (TimeTaggerId conf.id,
                                         new ConnectionAgent<TimeTagger>(conf.id,
                                                                         (openTimeTagger conf.serial),
                                                                         closeTimeTagger) :> IConnectionAgent ))

        let openCurrentSource address = HMC804x.openInstrument address 1000<ms>
        let closeCurrentSource = HMC804x.closeInstrument
        let currentSources = configuration.HMC804x
                             |> Seq.map (fun conf -> (CurrentSourceId conf.id,
                                                      new ConnectionAgent<CurrentSource>(conf.id,
                                                                                         (openCurrentSource conf.address),
                                                                                         (closeCurrentSource)) :> IConnectionAgent ))

        let openAttocube address = async { return new ANC300(address) }
        let closeAttocube (ac:ANC300) = async { (ac :> System.IDisposable).Dispose() }
        let anc300Instances = configuration.ANC300
                              |> Seq.map (fun conf -> (AttocubeId conf.id,
                                                       new ConnectionAgent<ANC300>(conf.id,
                                                                                   (openAttocube conf.address),
                                                                                   closeAttocube) :> IConnectionAgent ))

        let openPhotonCounter port = async {
            let pc = new PhotonCounter(port,System.Threading.SynchronizationContext.Current)
            do! pc.Initialise
            return pc }
        let closePhotonCounter (pc:PhotonCounter) = async { pc.SilenceRate(); pc.Serial.Close(); (pc :> System.IDisposable).Dispose() }
        let photonCounters = configuration.PhotonCounter
                             |> Seq.map (fun conf -> (PhotonCounterId conf.id,
                                                      new ConnectionAgent<PhotonCounter>(conf.id,
                                                                                         (openPhotonCounter conf.address),
                                                                                         (closePhotonCounter)) :> IConnectionAgent))
                                                  
        [ rfSources; picoHarps; pulseStreamers; currentSources; scanningControllers; photonCounters; anc300Instances; timeTaggers ] |> Seq.concat |> Map.ofSeq

    let getRfSource x (lease:Lease<InstrumentId>) = try (lease.Instruments.[RfSourceId x] :?> ConnectionAgent<RfSource>).TryConnection() with | _ -> failwithf "RfSource %s not found" x
    let getScanningController x (lease:Lease<InstrumentId>) = try (lease.Instruments.[ScanningControllerId x] :?> ConnectionAgent<ScanningController>).TryConnection() with | _ -> failwithf "ScanningController %s not found" x
    let getPhotonCounter x (lease:Lease<InstrumentId>) = try (lease.Instruments.[PhotonCounterId x] :?> ConnectionAgent<PhotonCounter>).TryConnection() with | _ -> failwithf "PhotonCounter %s not found" x
    let getCurrentSource x (lease:Lease<InstrumentId>) = try (lease.Instruments.[CurrentSourceId x] :?> ConnectionAgent<CurrentSource>).TryConnection() with | _ -> failwithf "CurrentSource %s not found" x
    let getAttocube x (lease:Lease<InstrumentId>) = try (lease.Instruments.[AttocubeId x] :?> ConnectionAgent<ANC300>).TryConnection() with | _ -> failwithf "Attocube %s not found" x
    let getPulseStreamer x (lease:Lease<InstrumentId>) = try (lease.Instruments.[PulseStreamerId x] :?> ConnectionAgent<PulseStreamer8>).TryConnection() with | _ -> failwithf "PulseStreamer %s not found" x
    let getTimeTagger x (lease:Lease<InstrumentId>) = try (lease.Instruments.[TimeTaggerId x] :?> ConnectionAgent<TimeTagger>).TryConnection() with | _ -> failwithf "TimeTagger %s not found" x
    let getPicoHarp x (lease:Lease<InstrumentId>) = try (lease.Instruments.[PicoHarpId x] :?> ConnectionAgent<PicoHarp.PicoHarpHandle>).TryConnection() with | _ -> failwithf "PicoHarp %s not found" x

    type ConfocalLease(instruments,ct,release) =
        inherit Lease<InstrumentId>(instruments, ct, release)
        member __.RfSource(x) = (instruments.[RfSourceId x] :?> ConnectionAgent<RfSource>).TryConnection()
        member __.ScanningController(x) = (instruments.[ScanningControllerId x] :?> ConnectionAgent<ScanningController>).TryConnection()
        member __.PhotonCounter(x) = (instruments.[PhotonCounterId x] :?> ConnectionAgent<PhotonCounter>).TryConnection()
        member __.CurrentSource(x) = (instruments.[CurrentSourceId x] :?> ConnectionAgent<CurrentSource>).TryConnection()
        member __.Attocube(x) = (instruments.[AttocubeId x] :?> ConnectionAgent<ANC300>).TryConnection()
        member __.PulseStreamer(x) = (instruments.[PulseStreamerId x] :?> ConnectionAgent<PulseStreamer8>).TryConnection()
        member __.TimeTagger(x) = (instruments.[TimeTaggerId x] :?> ConnectionAgent<TimeTagger>).TryConnection()
        member __.PicoHarp(x) = (instruments.[PicoHarpId x] :?> ConnectionAgent<PicoHarp300.Model.PicoHarp300>).TryConnection()

    let createConnectionManager configuration =
        let agents = configureInstruments configuration
        new ConnectionManager.ConnectionManager<InstrumentId>(agents)

    let getInstruments (connectionManager:ConnectionManager.ConnectionManager<InstrumentId>) instruments priority = async {
        let! lease = connectionManager.RequestInstruments instruments priority
        let lease = match lease with
                    | None -> failwithf "Unable to get instruments: %A" instruments
                    | Some lease -> match lease with
                                    | :? ConfocalLease as lease -> lease
                                    | _ -> failwith "Invalid lease"
        return lease }

    let photonCounterFromLease (lease:Lease<InstrumentId>) name =
        (lease.Instruments.[PhotonCounterId name] :?> ConnectionAgent<PhotonCounter>).TryConnection()

//    let configuration() = System.Windows.Application.Current.Properties.Item("configuration") :?> Configuration.InstrumentConfig
//    let connectionManager() = Application.Current.Properties.Item("connections") :?> Endorphin.Connection.ConnectionManager.ConnectionManager<InstrumentId>
