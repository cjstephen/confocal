// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OxyPlot;
using System.Collections.ObjectModel;

namespace Endorphin.UI
{
    /// <summary>
    /// Interaction logic for LiveCounts.xaml
    /// </summary>
    public partial class LiveCounts : UserControl
    {
        public event EventHandler<EventArgs> StartLiveCounts;
        public event EventHandler<EventArgs> StopLiveCounts;

        private PlotModel chartModel;
        ObservableCollection<DataPoint> channel0Counts;
        ObservableCollection<DataPoint> channel1Counts;

        int i = 0;

        public LiveCounts()
        {
            InitializeComponent();

            chartModel = new PlotModel ();
            channel0Counts = new ObservableCollection<DataPoint>();
            channel1Counts = new ObservableCollection<DataPoint>();

            var channel0Series = new OxyPlot.Series.LineSeries { Title = "Channel 0" };
            channel0Series.ItemsSource = channel0Counts;

            var channel1Series = new OxyPlot.Series.LineSeries { Title =  "Channel 1" };
            channel1Series.ItemsSource = channel1Counts;

            // add series to model, and model to plot
            chartModel.Series.Add(channel0Series);
            chartModel.Series.Add(channel1Series);
            liveCountChart.Model = chartModel;
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            if (StartLiveCounts != null)
                StartLiveCounts(this, new EventArgs());
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            if (StopLiveCounts != null)
                StopLiveCounts(this, new EventArgs());
        }

        public void addPoints(int channel0Count, int channel1Count)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (i > 100)
                {
                    try
                    {
                        channel0Counts.RemoveAt(0);
                        channel1Counts.RemoveAt(0);
                    }
                    catch { }
                }
                channel0Counts.Add(new DataPoint(i, channel0Count));
                channel1Counts.Add(new DataPoint(i, channel1Count));

                chartModel.InvalidatePlot(true);
            }));

            i++;
        }
    }
}
