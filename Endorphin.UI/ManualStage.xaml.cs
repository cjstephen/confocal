// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

using System;
using System.Windows.Controls;

namespace Endorphin.UI
{
    /// <summary>
    /// Interaction logic for ManualStage.xaml
    /// </summary>
    public partial class ManualStage : UserControl
    { 
        public class Location
        {
            public Tuple<decimal, decimal, decimal> coordinate { get; set; }
        }

        private bool uiEnabled = false;

        public event EventHandler<Location> GoToLocation;

        public ManualStage()
        {
            InitializeComponent();
        }

        public bool UIEnabled
        {
            get
            {
                return uiEnabled;
            }

            set
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    X.IsEnabled = Y.IsEnabled = Z.IsEnabled = uiEnabled = value;
                }));
            }
        }

        /// <summary>
        /// Update the UI with the current stage coordinates
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void SetCurrentCoordinates(decimal x, decimal y, decimal z)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                X.Value = x;
                Y.Value = y;
                Z.Value = z;
            }));
        }

        private void MoveToNewLocation ()
        {
            if (GoToLocation != null)
            {
                Location target = new Location();
                target.coordinate = new Tuple<decimal, decimal, decimal>((decimal)X.Value, (decimal)Y.Value, (decimal)Z.Value);
                GoToLocation(this, target);
            }
        }

        /// <summary>
        /// When the enter button is pushed inside a coordinate box, fire event to target coordinate
        /// </summary>
        private void Coordinate_KeyDown (object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                MoveToNewLocation();
            }
        }

        /// <summary>
        /// When the value of a coordinate is changed, fire event to target coordinate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Coordinate_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<object> e)
        {
            MoveToNewLocation();
        }
    }
}