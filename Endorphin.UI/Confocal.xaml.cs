// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataPoint = System.Tuple<double, double, double>;
using OxyPlot;
using OxyPlot.Wpf;
using System.IO;

namespace Endorphin.UI
{
    public partial class Confocal : UserControl
    {
        public class MapParameters
        {
            public ScanAxes axes { get; set; }
            public DataPoint origin { get; set; }
            public int gridSize { get; set; }
            public double stepSize { get; set; }
            public int integrationTime { get; set; }
        }

        public class MapPoint
        {
            public Tuple<double, double, double> coordinate { get; set; }
        }

        /// <summary>
        /// Possible scannable axes
        /// </summary>
        public enum ScanAxes
        {
            XY,
            XZ,
            YZ
        }

        /// <summary>
        /// Event fired when user wants to start experiment
        /// </summary>
        public event EventHandler<MapParameters> StartExperiment;

        /// <summary>
        /// Event fired when user wants to stop experiment
        /// </summary>
        public event EventHandler<EventArgs> StopExperiment;

        public event EventHandler<MapPoint> GoToPoint;

        public event EventHandler<EventArgs> ClosePicoHarp;
        public event EventHandler<EventArgs> OpenPicoHarp;

        private PlotModel imageModel;
        private OxyPlot.Series.HeatMapSeries confocalSeries;
        private bool uiEnabled;
        private string informationText;

        public Confocal()
        {
            InitializeComponent();
            gridAxes.ItemsSource = Enum.GetValues(typeof(ScanAxes));

            imageModel = new PlotModel();
            imageModel.Axes.Add(new OxyPlot.Axes.LinearColorAxis
            {
                Position = OxyPlot.Axes.AxisPosition.Right,
                //Parula like palette
                //Palette = OxyPlot.OxyPalette.Interpolate(700,
                //OxyColors.DarkBlue,
                //OxyColors.Blue,
                //OxyColors.Cyan,
                //OxyColors.Green,
                //OxyColors.Olive,
                //OxyColors.Orange,
                //OxyColors.Yellow),
                //Jet Palette
                Palette = OxyPlot.OxyPalettes.Jet(700),

                //Palette = OxyPlot.OxyPalette.Interpolate(500, OxyColors.Black, OxyColors.Red), //two colour palette
                HighColor = OxyPlot.OxyColors.DarkRed,
                LowColor = OxyPlot.OxyColors.DarkBlue,
            });

            confocalSeries = new OxyPlot.Series.HeatMapSeries
            {
                X0 = 0,
                X1 = 60,
                Y0 = 0,
                Y1 = 60,
                Data = new Double[100, 100],
                Interpolate = false
            };

            imageModel.Series.Add(confocalSeries);
            confocalImage.Model = imageModel;
        }

        /// <summary>
        /// Set axis limits
        /// </summary>
        /// <param name="limits">Array of limits in the form [x0, x1, y0, y1]</param>
        public void SetAxisLimits(double[] limits)
        {
            confocalSeries.X0 = limits[0];
            confocalSeries.X1 = limits[1];
            confocalSeries.Y0 = limits[2];
            confocalSeries.Y1 = limits[3];
        }

        /// <summary>
        /// Data array for confocal image
        /// </summary>
        public Double[,] Intensity
        {
            get
            {
                return confocalSeries.Data;
            }
            set
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    confocalSeries.Data = value;
                }));

            }
        }

        /// <summary>
        /// Force the image to update after new data are provided
        /// </summary>
        public void UpdateImage()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                confocalSeries.Invalidate();
                confocalImage.Model.InvalidatePlot(true);
            }));
        }

        /// <summary>
        /// Update the UI with the current stage coordinates
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void SetCurrentCoordinates(double x, double y, double z)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                X.Value = (decimal)x;
                Y.Value = (decimal)y;
                Z.Value = (decimal)z;
            }));
        }

        /// <summary>
        /// Enable or disable the experimental parameters in the UI
        /// </summary>
        public bool UIEnabled
        {
            get
            {
                return uiEnabled;
            }
            set
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    X.IsEnabled = gridSizeControl.IsEnabled = stepSizeControl.IsEnabled = isLogScale.IsEnabled = saveButton.IsEnabled = 
                    Y.IsEnabled = Z.IsEnabled = integrationTimeControl.IsEnabled = startButton.IsEnabled = gridAxes.IsEnabled = value;
                }));
                uiEnabled = value;
            }
        }

        /// <summary>
        /// Text to display on the information panel on the UI
        /// </summary>
        public string InformationText
        {
            get
            {
                return informationText;
            }
            set
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    this.InformationLabel.Content = value;
                }));
                informationText = value;
            }
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            if (StartExperiment != null)
            {
                if ((int)Math.Pow((double)gridSizeControl.Value / (double)stepSizeControl.Value, 2.0) > 11000000)
                {
                    MessageBox.Show("Too many points - 11,000,000 is the upper limit");
                    return;
                }

                StartExperiment(this, new MapParameters
                {
                    axes = (ScanAxes)gridAxes.SelectedIndex,
                    origin = new DataPoint((double)X.Value, (double)Y.Value, (double)Z.Value),
                    gridSize = (int)gridSizeControl.Value,
                    stepSize = (double)stepSizeControl.Value,
                    integrationTime = (int)integrationTimeControl.Value
                });
                InformationText = "Running experiment...";
            }
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            if (StopExperiment != null)
            {
                StopExperiment(this, EventArgs.Empty);
                InformationText = "Stopping experiment...";
            }
        }

        private void confocalImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                Point xy = e.GetPosition(confocalImage);
                if (GoToPoint != null)
                {
                    MapPoint point = new MapPoint();

                    double imageX = confocalImage.Model.Axes[1].InverseTransform(xy.X);
                    double imageY = confocalImage.Model.Axes[2].InverseTransform(xy.Y);
                    switch ((ScanAxes)gridAxes.SelectedIndex)
                    {
                        case ScanAxes.XY:
                            point.coordinate = new Tuple<double, double, double>(imageX, imageY, (double)Z.Value);
                            break;
                        case ScanAxes.XZ:
                            point.coordinate = new Tuple<double, double, double>(imageX, (double)Y.Value, imageY);
                            break;
                        case ScanAxes.YZ:
                            point.coordinate = new Tuple<double, double, double>((double)X.Value, imageX, imageY);
                            break;
                    }

                    GoToPoint(this, point);
                }
            }
        }

        private void SetVis_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                imageModel.Axes.First().Maximum = (int)MaxVis.Value;
                imageModel.Axes.First().Minimum = (int)MinVis.Value;
                UpdateImage();
            }));
        }

        private void ResetVis_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                imageModel.Axes.First().Maximum = Double.NaN;
                imageModel.Axes.First().Minimum = Double.NaN;
                UpdateImage();
            }));
        }

        private void ClosePicoHarp_Click(object sender, RoutedEventArgs e)
        {
            if (ClosePicoHarp != null)
                ClosePicoHarp(this, new EventArgs());
        }

        private void OpenPicoHarp_Click(object sender, RoutedEventArgs e)
        {
            if (OpenPicoHarp != null)
                OpenPicoHarp(this, new EventArgs());
        }

        private void isLogScale_Checked(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                for (int i = 0; i < confocalSeries.Data.GetLength(0); i++)
                {
                    for (int j = 0; j < confocalSeries.Data.GetLength(1); j++)
                        confocalSeries.Data[i, j] = Math.Log10(confocalSeries.Data[i, j]);
                }

                UpdateImage();
            }));
        }

        private void isLogScale_Unchecked(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                for (int i = 0; i < confocalSeries.Data.GetLength(0); i++)
                {
                    for (int j = 0; j < confocalSeries.Data.GetLength(1); j++)
                        confocalSeries.Data[i, j] = Math.Pow(10, (confocalSeries.Data[i, j]));
                }

                UpdateImage();
            }));
        }

        private void is2Colour_Checked(object sender, RoutedEventArgs e)
        {
            var colourAxis = (OxyPlot.Axes.LinearColorAxis)imageModel.Axes[0];

            colourAxis.Palette = OxyPlot.OxyPalette.Interpolate(700, OxyColors.Black, OxyColors.DarkRed);
            colourAxis.HighColor = OxyPlot.OxyColors.DarkRed;
            colourAxis.LowColor = OxyPlot.OxyColors.Black;

            imageModel.Axes[0] = colourAxis;
            UpdateImage();
        }

        private void is2Colour_Unchecked(object sender, RoutedEventArgs e)
        {
            var colourAxis = (OxyPlot.Axes.LinearColorAxis)imageModel.Axes[0];

            colourAxis.Palette = OxyPlot.OxyPalettes.Jet(700);
            colourAxis.HighColor = OxyPlot.OxyColors.DarkRed;
            colourAxis.LowColor = OxyPlot.OxyColors.DarkBlue;

            imageModel.Axes[0] = colourAxis;
            UpdateImage();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            var parameterStream = new StringBuilder();

            parameterStream.AppendLine("X0:\t" + confocalSeries.X0.ToString());
            parameterStream.AppendLine("X1:\t" + confocalSeries.X1.ToString());
            parameterStream.AppendLine("Y0:\t " + confocalSeries.Y0.ToString());
            parameterStream.AppendLine("Y1:\t " + confocalSeries.Y1.ToString());
            parameterStream.AppendLine("Step:\t" + confocalSeries.Data.GetLength(0).ToString());
            parameterStream.AppendLine("Int (ms):\t" + integrationTimeControl.Value.ToString());

            File.WriteAllText(@"C:\Users\NQIT\Desktop\RawData\confocalImage" + DateTime.Now.ToString("hhmm_ddMMMyy") + ".txt", parameterStream.ToString());


            var csv = new StringBuilder();            

            for (int i = 0; i < confocalSeries.Data.GetLength(0); i++)
            {
                for (int j = 0; j < confocalSeries.Data.GetLength(1); j++)
                    csv.Append(confocalSeries.Data[i, j].ToString() + ",");

                csv.AppendLine();
            }

            File.WriteAllText(@"C:\Users\NQIT\Desktop\RawData\confocalImage" + DateTime.Now.ToString("hhmm_ddMMMyy") +".csv", csv.ToString());

            PngExporter.Export(imageModel, @"C:\Users\NQIT\Desktop\RawData\confocalImage" + DateTime.Now.ToString("hhmm_ddMMMyy") + ".png", 1000, 1000, OxyPlot.OxyColor.FromRgb(0xff,0xff,0xff));

            InformationText = "SAVED ALL THE DATA!";
        }
    }
}