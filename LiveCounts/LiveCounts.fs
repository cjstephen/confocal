﻿// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace LiveCounts

open ViewModule
open ViewModule.FSharp
open Endorphin.Instruments
open Endorphin.Connection
open FSharp.Control.Reactive
open System.Collections.ObjectModel
open System.Windows.Data
open System
open Acquisition
open Endorphin.Core
open Endorphin.Instrument.Warwick.PhotonCounter
    
    type LiveCountsState =
    | Unsubscribed
    | Normal of subscription : IDisposable * lease : Lease<InstrumentId>
    | Background of subscription : IDisposable

    type LiveCountsMsg =
    | Start
    | GiveWay
    | Disconnect

    type LiveCountsPhotonCounterVM(connectionManager : ConnectionManager.ConnectionManager<InstrumentId>) as self =
        inherit ViewModelBase()

        let n = 500

        let logger = log4net.LogManager.GetLogger "LiveCounts"
        let counts = Array.replicate n 0
        let points = Array.mapi (fun i x -> OxyPlot.DataPoint(float i,float x))
        let resetCountData() = Array.fill counts 0 n 0

        let maxCounts = self.Factory.Backing(<@ self.MaxCounts @>,1e6)
        let currentPosition = self.Factory.Backing(<@ self.Cursor @>,0)
        let ui = System.Threading.SynchronizationContext.Current

        let rec getLeaseWhenConnected _ = async {
            let! instruments = connectionManager.RequestInstruments (set [ PhotonCounterId "PhotonCounter" ]) Passive
            match instruments with
            | None ->
                logger.Debug "Photon Counter not available"
                do! Async.Sleep 500
                return! getLeaseWhenConnected()
            | Some lease ->
                return lease 
            }

        let setMax (maxCount:int) =
            let maxLog = maxCount |> float |> Math.Log10 |> (*)2.0 |> Math.Ceiling |> (*)0.5
            self.MaxCounts <- Math.Pow(10.0,maxLog)

        let liveCountsAgent = MailboxProcessor.Start(fun inbox ->
            let initialise (pc:PhotonCounter) = async {
                do! pc.Initialise
                
                pc.InternalTrigger 50
                pc.EmitRate() }

            let rec loop state = async {
                do! Async.SwitchToThreadPool()
                do! Async.SwitchToContext ui
                let! msg = inbox.Receive()
                logger.Debug <| sprintf "Received %A" msg
                match msg with
                | Start ->
                    match state with
                    | Unsubscribed ->
                        // get passive lease
                        logger.Debug "Subscribing to live counts"
                        let! lease = getLeaseWhenConnected()
                        let pc = Instruments.photonCounterFromLease lease "PhotonCounter"
                        logger.Debug "About to subscribe:"
                        let now = DateTime.Now
                        let countRates = pc.Rate()
                                        |> Observable.observeOn Reactive.Concurrency.CurrentThreadScheduler.Instance
                                        |> Observable.observeOnContext ui
                                        |> Observable.guard resetCountData
                                        |> Observable.mapi (fun i x -> (i%n,x) )

                        let subscription =
                             countRates
                             |> Observable.subscribeWithCallbacks (fun (i,x) -> counts.[i] <- x
                                                                                self.Cursor <- i
                                                                                self.RaisePropertyChanged(<@ self.LiveCountTotal @>) )
                                                                  (sprintf "Err: %A" >> logger.Debug)
                                                                  (fun _ -> logger.Debug "ended"; inbox.Post Disconnect)
                        do! initialise pc
                        logger.Debug "After subscribe"
                        logger.Debug "Starting counts"

                        // subscribe
                        return! loop <| Normal (subscription,lease)
                    | Normal (subscription,lease) ->
                        logger.Debug "Reinitialising live counts"
                        let pc = Instruments.photonCounterFromLease lease "PhotonCounter"
                        do! initialise pc
                        return! loop <| Normal (subscription,lease)
                    | Background subscription ->
                        logger.Debug "Try to get background control of live counts"
                        // get passive lease
                        let! lease = getLeaseWhenConnected()
                        let pc = Instruments.photonCounterFromLease lease "PhotonCounter"
                        do! initialise pc
                        // still subscribed in the background, so should be all that's needed
                        return! loop <| Background subscription 
             
                | GiveWay ->
                    match state with
                    | Normal (subscription,lease) ->
                        logger.Debug "Releasing lease, watch from the background"
                        lease.Release()
                        return! loop <| Background subscription
                    | _ ->
                        logger.Debug <| sprintf "Told to give way but not applicable in state %A" state
                        return! loop state
                | Disconnect ->
                    match state with
                    | Normal (subscription,lease) ->
                        logger.Debug "Disconnecting from normal operation"
                        subscription.Dispose()
                        lease.Release()
                        return! loop <| Unsubscribed
                    | Background subscription ->
                        logger.Debug "Disconnecting when in background"
                        subscription.Dispose()
                        return! loop <| Unsubscribed
                    | _ ->
                        return! loop state }
            loop Unsubscribed)

        member __.Start()   = Start |> liveCountsAgent.Post
        member __.GiveWay() = GiveWay |> liveCountsAgent.Post
        member __.Stop()    = Disconnect |> liveCountsAgent.Post
        member __.LiveCountTotal with get() = points counts
        member __.Cursor with get() = currentPosition.Value and set v= currentPosition.Value <- v
        member __.MaxCounts with get() = maxCounts.Value and set v = maxCounts.Value <- v
     