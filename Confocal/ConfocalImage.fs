﻿namespace Confocal

open ViewModule
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open System

type ConfocalImageView =
    | LinearImage
    | LogImage

type ConfocalImageEvent =
    | Click of decimal<um> * decimal<um>

type ConfocalImageVM(initialParameters) as this =
    inherit ViewModelBase()

    let logger = log4net.LogManager.GetLogger "ConfocalImaging"

    let event = new Event<ConfocalImageEvent>()

    let imageSeries (parameters:ImageParameters) =

        let confocalSeries = new OxyPlot.Series.HeatMapSeries()
        let (x0,y0) = Image.planeOrigin parameters
        let (x1,y1) = (x0+parameters.Width,y0+parameters.Height)
        let (xLabel,yLabel) = Image.axisLabels parameters.Axes
        confocalSeries.X0 <- x0 |> (*)1.m</um> |> float
        confocalSeries.X1 <- x1 |> (*)1.m</um> |> float
        confocalSeries.Y0 <- y0 |> (*)1.m</um> |> float
        confocalSeries.Y0 <- y1 |> (*)1.m</um> |> float
        let twoMinimum x = if x < 2 then 2 else x // Because of an OxyPlot bug, see issue 1031
        let uPoints = Image.uPoints parameters |> twoMinimum
        let vPoints = Image.vPoints parameters |> twoMinimum
        logger.Debug <| sprintf "Creating image array %d by %d" uPoints vPoints
        confocalSeries.Data <- Array2D.create uPoints vPoints 0.0
        confocalSeries.Interpolate <- false
        confocalSeries

    // http://www.kennethmoreland.com/color-advice/ Extended Kindlmann - more or less
    // Could use GradientStops to set the points to interpolate
    let extendedKindelmannPalette =
        let colours = [|
                        OxyPlot.OxyColor.FromRgb(36uy,3uy,62uy)
                        OxyPlot.OxyColor.FromRgb(44uy,5uy,109uy)
                        OxyPlot.OxyColor.FromRgb(5uy,49uy,98uy)
                        OxyPlot.OxyColor.FromRgb(3uy,72uy,61uy)
                        OxyPlot.OxyColor.FromRgb(4uy,91uy,30uy)
                        OxyPlot.OxyColor.FromRgb(5uy,110uy,6uy)
                        OxyPlot.OxyColor.FromRgb(82uy,122uy,6uy)
                        OxyPlot.OxyColor.FromRgb(159uy,121uy,8uy)
                        OxyPlot.OxyColor.FromRgb(245uy,91uy,50uy)
                        OxyPlot.OxyColor.FromRgb(249uy,121uy,134uy)
                        OxyPlot.OxyColor.FromRgb(250uy,144uy,224uy)
                        OxyPlot.OxyColor.FromRgb(230uy,181uy,251uy)
                        OxyPlot.OxyColor.FromRgb(231uy,209uy,253uy)
                        OxyPlot.OxyColor.FromRgb(235uy,234uy,254uy) |]
        OxyPlot.OxyPalette.Interpolate(800,colours)

    let plotModel =
        let model = new OxyPlot.PlotModel()

        model.PlotType <- OxyPlot.PlotType.Cartesian

        let intensityAxis = new OxyPlot.Axes.LinearColorAxis()
        intensityAxis.Position <- OxyPlot.Axes.AxisPosition.Right
        intensityAxis.Palette <- extendedKindelmannPalette
        intensityAxis.HighColor <- OxyPlot.OxyColors.DarkRed
        intensityAxis.LowColor <- OxyPlot.OxyColors.DarkBlue
        intensityAxis.Minimum <- 0.0
        model.Axes.Add(intensityAxis)
        initialParameters |> imageSeries |> model.Series.Add
        initialParameters |> imageSeries |> model.Series.Add
        model

    let ready() = plotModel.Series.Count = 2

    let linearSeries() = (plotModel.Series.Item(0) :?> OxyPlot.Series.HeatMapSeries)
    let logSeries() =    (plotModel.Series.Item(1) :?> OxyPlot.Series.HeatMapSeries)

    let floatToLength = decimal >> LanguagePrimitives.DecimalWithMeasure<um>

    let position (x:OxyPlot.ScreenPoint) =
        let p = linearSeries().InverseTransform(x)
        ( p.X |> floatToLength, p.Y |> floatToLength)

    do
        plotModel.MouseDown.Add (fun x ->
            x.Position |> position |> Click |> event.Trigger
            ())

    let updateImage() =
        this.RaisePropertyChanged("PlotModel")
        if ready() then
            try
                linearSeries().Invalidate()
                logSeries().Invalidate()
            with
            | _ -> logger.Warn "Failed to update plot. Not initialised"
        plotModel.InvalidatePlot(true)

    member __.SetPoint ((u,v):GridPoint) (intensity:Acquisition.Intensity) =
        if ready() then
            linearSeries().Data.[u,v] <- float intensity
            logSeries().Data.[u,v] <- Math.Log10 (float intensity)

    member __.Refresh() = updateImage()

    member __.NewImage imageParameters =
        plotModel.Series.Clear()
        imageParameters |> imageSeries |> plotModel.Series.Add
        imageParameters |> imageSeries |> plotModel.Series.Add
        updateImage()

    member __.View with get() = if ready() && linearSeries().IsVisible then LinearImage else LogImage
                    and set v = if ready() then
                                    match v with
                                    | LinearImage -> linearSeries().IsVisible <- true
                                                     logSeries().IsVisible <- false
                                    | LogImage    -> linearSeries().IsVisible <- false
                                                     logSeries().IsVisible <- true

    member __.Actions() = event.Publish

    member __.ImageModel with get() = plotModel

    member __.SaveImage filenameStem =
        OxyPlot.Wpf.PngExporter.Export(plotModel,filenameStem + ".png",1000,1000,OxyPlot.OxyColor.FromUInt32(0xffffffu))

