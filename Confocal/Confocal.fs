﻿// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Confocal

open FsXaml
open ViewModule
open ViewModule.FSharp
open Endorphin.Abstract
open System.Windows
open Endorphin.Connection
open Endorphin.Instruments
open Endorphin.Instrument.FastScanningController
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open System.Threading
open Endorphin.Core
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open System
open Acquisition
open Xceed.Wpf.Toolkit
open FSharp.Control.Reactive

type Position = decimal<um> * decimal<um> * decimal<um>
type GridPoint = int * int

type ImageParameters = {
    Axes : Position.Path.Plane
    X  : decimal<um>
    Y  : decimal<um>
    Z  : decimal<um>
    Width  : decimal<um>
    Height : decimal<um>
    StepSize : decimal<um>
    IntegrationTime : int<ms> }

type Mode =
    | Disabled
    | PositioningMode of Lease<InstrumentId>
    | ImagingMode of CancellationTokenSource

type Request =
    | TakeImage of ImageParameters
    | CancelImaging
    | FinishImaging
    | Disconnect
    | Connect
    | IsImaging of AsyncReplyChannel<bool>
    | GetPosition of AsyncReplyChannel<Position>
    | PositioningEnabled of AsyncReplyChannel<bool>
    | SetPosition of Position

type ConfocalImageOutput = {
    ImageParameters : ImageParameters
    Data : IObservable<GridPoint*Acquisition.Intensity> }

type ConfocalConfiguration = {
    CountInstrument : InstrumentId
    ScanningControllerSettings : ScanningControllerSettings }

module Image =

    open Position.Path

    // setup confocal image
    let uPoints parameters = parameters.Width / parameters.StepSize |> float |> floor |> int
    let vPoints parameters = parameters.Height / parameters.StepSize |> float |> floor |> int

    let origin parameters =
        let w = parameters.Width * 0.5m
        let h = parameters.Height * 0.5m
        match parameters.Axes, parameters.X, parameters.Y, parameters.Z with
        | Plane.XY, x, y, z -> (x-w,y-h,z)
        | Plane.XZ, x, y, z -> (x-w,y,z-h)
        | Plane.YZ, x, y, z -> (x,y-w,z-h)     

    let planeCentre parameters =
        match parameters.Axes, parameters.X, parameters.Y, parameters.Z with
        | Plane.XY, x, y, z -> (x,y)
        | Plane.XZ, x, y, z -> (x,z)
        | Plane.YZ, x, y, z -> (y,z)

    let planeOrigin parameters =
        let (a,b) = planeCentre parameters
        (a-0.5m*parameters.Width,b-0.5m*parameters.Height)

    let bounds parameters =
        let origin = planeOrigin parameters
        [| fst origin; fst origin + parameters.Width; snd origin; snd origin + parameters.Height |]
    
    let path (parameters:ImageParameters) =
        Position.Path.createRaster 5m<um> (origin parameters) (parameters.Width,parameters.Height) (parameters.StepSize,parameters.StepSize) parameters.Axes
     
    let X (x,y,z) = x
    let Y (x,y,z) = y
    let Z (x,y,z) = z

    let axisLabels = function
                     | Plane.XY -> ("X","Y")
                     | Plane.XZ -> ("X","Z")
                     | Plane.YZ -> ("Y","Z")

type ConfocalUIAgent( connectionManager : ConnectionManager.ConnectionManager<InstrumentId>, confocalConfiguration) =


    let logger = log4net.LogManager.GetLogger "ConfocalImaging"

    let confocalConfiguration = confocalConfiguration() // so that the low-temperature setup can vary calibration and limits with temperature
    let detectionInstrument =
        confocalConfiguration.CountInstrument
    let scannerSettings = confocalConfiguration.ScanningControllerSettings

    let connectPositionControl =
        connectionManager.RequestInstrumentsWithTimeout (set [ScanningControllerId "ScanningController"]) Priority.Passive 2000

    let connectForImaging =
        connectionManager.RequestInstrumentsWithTimeout (set [ScanningControllerId "ScanningController"; detectionInstrument]) Priority.Active 4000

    let status = new NotificationEvent<string>()
    let statusUpdate x =
        logger.Info x
        Next x |> status.Trigger

    let images = new NotificationEvent<ConfocalImageOutput>()
    let imageOutput = Next >> images.Trigger
    
    let takeImage (lease:Lease<InstrumentId>) (parameters:ImageParameters) (ct:CancellationToken) = async {

        logger.Debug "Taking images"
        do! Async.SwitchToNewThread()
        logger.Debug "On new thread"

        // We'll need positioning
        let scanningController = lease |> Instruments.getScanningController "ScanningController"

        logger.Debug "Have lease"

        // store current stage position and setup the dwell time on each point
        let! initialStagePosition = Instrument.Position.getPosition scanningController scannerSettings
        
        do! Instrument.Timing.setDwellTime scanningController (parameters.IntegrationTime)
        do! Instrument.Timing.setTriggerDelay scanningController 1.<ms>
        do! Instrument.Timing.setTriggerPulseLength scanningController 500<us>

        // create path
        statusUpdate "Writing path to controller"
        let path = Image.path parameters |> Position.Path.repeatLastPoint |> Position.Path.repeatLastPoint |> Position.Path.repeatFirstPoint
        do! Instrument.Position.Path.writePathToController scanningController scannerSettings path

        // and to turn on the excitation laser
//        statusUpdate "Turning on excitation laser"
//        let pulseStreamer = lease |> Instruments.getPulseStreamer "PulseStreamer"
//        do! PulseStreamer.PulseSequence.setState [Channel1] pulseStreamer

        // Now let the output stage know something is coming
        // TODO: emit an event

        // get hold of an acquistion stream
        let points = Position.Path.points path |> Array.map (fun p -> (p:GridPoint) ) |> (fun x -> Array.sub x 0 (x.Length - 1))
        let numberOfPoints = points.Length
        let confocalImageData = Acquisition.scan detectionInstrument lease points |> Observable.take numberOfPoints
//        let confocalImageData = Observable.zip (points |> Observable.ofSeq) intensities
//                                |> Observable.takeWhile (fun _ -> not ct.IsCancellationRequested)
//                                |> Observable.observeOn System.Reactive.Concurrency.NewThreadScheduler.Default
//                                |> Observable.replay
        
        confocalImageData |> Observable.first |> Observable.add (fun x -> logger.Info <| sprintf "Received first point")
        confocalImageData |> Observable.count |> Observable.add (fun x -> logger.Info <| sprintf "Received %d points" x)

//        let waitToFinish = confocalImageData |> Observable.last |> Async.AwaitObservable |> Async.Ignore

        imageOutput { ImageParameters = parameters
                      Data = confocalImageData }  

        statusUpdate "Moving to first point..."
        do! Instrument.Position.setPosition scanningController scannerSettings (Position.Path.coordinatesAtIndex 0 path)
//        let handle = Observable.connect confocalImageData
        do! Async.Sleep 200

        statusUpdate "Running scan"
        do! Instrument.Position.Path.runPath scanningController
        // as the scanner visits each point, trigger pulses are sent to the detection device to delimit points

        // The task isn't complete until all the points have been visited, or there's been cancellation/error
//        do! waitToFinish
        do confocalImageData |> Observable.wait |> ignore
        statusUpdate "Imaging scan finished"
    }
    
    let zero : Position = (0.0m<um>,0.0m<um>,0.0m<um>)
    
    let modeLoop (inbox:MailboxProcessor<Request>) =
        let rec loop mode position = async {

            let! msg = inbox.TryReceive(200)

            match mode with
            | Disabled ->
                match msg with
                | Some (GetPosition rc) ->
                    position |> rc.Reply
                    return! loop Disabled position
                | Some (PositioningEnabled rc) ->
                    false |> rc.Reply
                    return! loop Disabled position
                | Some (IsImaging rc) ->
                    false |> rc.Reply
                    return! loop Disabled position
                | Some Connect ->
                    // try to switch to Positioning
                    logger.Debug "Trying to take instruments for positioning"
                    let! lease = connectPositionControl
                    logger.Debug <| sprintf "Returned from lease request: %A" lease
                    match lease with
                    | Some lease ->
                        logger.Debug "Have lease for positioning"
                        statusUpdate "Ready for positioning"
                        return! loop (PositioningMode lease) position
                    | None ->
                        logger.Debug "Failed to get lease for positioning"
                        return! loop Disabled position
                | _ ->
                    return! loop Disabled position
            | PositioningMode lease ->
                match msg with
                | Some (TakeImage parameters) ->
                    logger.Debug "Taking an image"
                    // release positioning lease
                    lease.Release()
                    // request a lease for imaging
                    let! imagingLease = connectForImaging
                    match imagingLease with
                    // should now have lease, if not revert to disabled
                    | None ->
                        logger.Error "Failed to get lease for imaging"
                        return! loop Disabled position
                    // otherwise, get on with the image
                    | Some lease ->
                        let cts = new CancellationTokenSource()
                        // Start taking image in the background
                        let finishImaging _ =
                            lease.Release()
                            (lease :> IDisposable).Dispose()
                            FinishImaging |> inbox.Post
//                        let stopScan() = async {
//                            let scanningController = Instruments.getScanningController "ScanningController" lease
//                            do! Instrument.Position.Path.stopPath scanningController } |> Async.Start
                        Async.StartWithContinuations( takeImage lease parameters cts.Token,
                                                      (fun _ -> logger.Debug "Completed image"; finishImaging()),
                                                      (fun exn -> logger.Error <| sprintf "Failed to complete image %A" exn; finishImaging()),
                                                      (fun ct  -> logger.Info "Cancelled imaging"; finishImaging()),
                                                      cts.Token)
                        logger.Debug "Started taking image"
                        return! loop (ImagingMode cts) position
                | Some (GetPosition rc) ->
                    let scanner = Instruments.getScanningController "ScanningController" lease
                    let! position' = Instrument.Position.getPosition scanner scannerSettings
                    position' |> rc.Reply
                    return! loop mode position'
                | Some (IsImaging rc) ->
                    false |> rc.Reply
                    return! loop mode position
                | Some (PositioningEnabled rc) ->
                    true |> rc.Reply
                    return! loop mode position
                | Some (SetPosition position') -> 
                    let scanner = Instruments.getScanningController "ScanningController" lease
                    try
                        do! Instrument.Position.setPosition scanner scannerSettings position'
                        return! loop mode position'
                    with
                    | exn -> logger.Error "Failed to set position"
                             return! loop mode position
                | Some Disconnect ->
                    logger.Debug "Disconnecting from positioning mode"
                    lease.Release()
                    (lease :> IDisposable).Dispose()
                    return! loop Disabled position
                | _ ->
                    if lease.RevokedToken.IsCancellationRequested then
                        statusUpdate "Positioning disabled"
                        return! loop Disabled position
                    else
                        return! loop mode position
            | ImagingMode cts ->
                match msg with
                | Some CancelImaging ->
                    logger.Info "Cancelling image acquisition"
                    cts.Cancel()
                    Connect |> inbox.Post
                    return! loop Disabled position
                | Some Disconnect ->
                    logger.Info "Cancelling image acquisition, disconnecting"
                    cts.Cancel()
                    return! loop Disabled position
                | Some (GetPosition rc) ->
                    position |> rc.Reply
                    return! loop mode position
                | Some (IsImaging rc) ->
                    true |> rc.Reply
                    return! loop mode position
                | Some (PositioningEnabled rc) ->
                    false |> rc.Reply
                    return! loop mode position
                | Some FinishImaging ->
                    Connect |> inbox.Post
                    return! loop Disabled position
                | _ -> return! loop mode position

        }
        async {
            do! Async.SwitchToNewThread()
            return! loop Disabled zero }

    let agent = MailboxProcessor.Start modeLoop

    member __.Connect() = Connect |> agent.Post
    member __.Disconnect() = Disconnect |> agent.Post
    member __.TakeImage parameters = TakeImage parameters |> agent.Post
    member __.CancelImaging() = CancelImaging |> agent.Post
    member __.GetPosition() = GetPosition |> agent.PostAndReply
    member __.SetPosition position = SetPosition position |> agent.Post
    member __.PositioningEnabled() = PositioningEnabled |> agent.PostAndReply
    member __.IsImaging() = IsImaging |> agent.PostAndReply
    member __.Images = images.Publish |> Observable.fromNotificationEvent
    member __.StatusUpdates = status.Publish |> Observable.fromNotificationEvent

