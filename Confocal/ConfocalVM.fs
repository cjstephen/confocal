﻿namespace Confocal

open FsXaml
open ViewModule
open ViewModule.FSharp
open Endorphin.Abstract
open System.Windows
open Endorphin.Instruments
open Endorphin.Instrument.FastScanningController
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open System.Threading
open Endorphin.Core
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open System
open Xceed.Wpf
open FSharp.Control.Reactive

module Confocal =
    let defaultImageParameters = {
        Axes = Position.Path.XY
        X = 15.0m<um>
        Y = 15.0m<um>
        Z = 0.0m<um>
        Width = 30.0m<um>
        Height = 30.0m<um>
        StepSize = 0.4m<um>
        IntegrationTime = 10<ms> }


type ConfocalVM(connectionManager,confocalConfiguration) as self =
    inherit ConfocalImageVM(Confocal.defaultImageParameters)

    let logger = log4net.LogManager.GetLogger "ConfocalImaging"

    let informationText = self.Factory.Backing(<@ self.InformationText @>, "Starting up")
    let imageParameters = self.Factory.Backing(<@ self.ImageParameters @>, Confocal.defaultImageParameters)
    let lastImageParameters = self.Factory.Backing(<@ self.LastImageParameters @>, Confocal.defaultImageParameters)

    let position (p:ImageParameters) = (p.X, p.Y, p.Z)

    let confocalAgent = new ConfocalUIAgent(connectionManager,confocalConfiguration)

    let dummyData uPoints vPoints = Array2D.init uPoints vPoints (fun u v -> u + v |> float)
    let data = self.Factory.Backing(<@ self.Data @>,dummyData 30 30)

    let coerce a b x = if x < a then a else if x > b then b else x
    let atLeast a x = if x < a then a else x
    let atMost a x  = if x > a then a else x

    do
        logger.Debug "Subscribing to images"

        let imageOutput =
            confocalAgent.Images
            |> Observable.guard (fun _ -> logger.Debug "New subscription to images")
            |> Observable.observeOnContext(Threading.SynchronizationContext.Current)
            |> Observable.map (fun image ->
                                    logger.Debug "Starting new image"
                                    self.NewImage image.ImageParameters
                                    self.Refresh()
                                    image.Data |> Observable.iter (fun (p,i) -> self.SetPoint p i) |> Observable.map ignore) |> Observable.mergeInner
        imageOutput |> Observable.add ignore
        self.Actions().Add(function
                           | Click (u,v) ->
                                self.SetPositionInPlane (u,v))

        Observable.interval (TimeSpan.FromSeconds 0.3) |> Observable.add (fun _ -> self.Refresh())

    let allowImaging _ =
        let a = confocalAgent.PositioningEnabled() && (not <| confocalAgent.IsImaging())
        logger.Debug <| sprintf "Imaging enabled: %A" a
        a

    let updateEnabled() =
        self.RaisePropertyChanged("PositioningEnabled")
        self.RaisePropertyChanged("TakeImage")
        self.RaisePropertyChanged("StopImaging")

    let activeAxisConfig =
        function
        | Position.Path.Plane.XY -> (confocalConfiguration().ScanningControllerSettings.X.Value,confocalConfiguration().ScanningControllerSettings.Y.Value)
        | Position.Path.Plane.XZ -> (confocalConfiguration().ScanningControllerSettings.X.Value,confocalConfiguration().ScanningControllerSettings.Z.Value)
        | Position.Path.Plane.YZ -> (confocalConfiguration().ScanningControllerSettings.Y.Value,confocalConfiguration().ScanningControllerSettings.Z.Value)


    let axesMaximumDistance (uConf,vConf) =
        (uConf.VoltageLimit * uConf.Calibration * 1.0e6m<um/m>,vConf.VoltageLimit * vConf.Calibration * 1.0e6m<um/m>)

    let takeImage() =
        self.LastImageParameters <- self.ImageParameters
        confocalAgent.TakeImage self.LastImageParameters

    do
        self.DependencyTracker.AddPropertyDependency(<@ self.AxisU @>, <@ self.Axes @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.AxisV @>, <@ self.Axes @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.Axes @>, <@ self.ImageParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.U @>, <@ self.ImageParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.V @>, <@ self.ImageParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.X @>, <@ self.ImageParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.Y @>, <@ self.ImageParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.Z @>, <@ self.ImageParameters @>)

        confocalAgent.StatusUpdates.Add (fun msg -> updateEnabled(); self.InformationText <- msg)


    member __.Connect() = confocalAgent.Connect()
    member __.Disconnect() = confocalAgent.Disconnect()
    member __.PositioningEnabled with get() = confocalAgent.PositioningEnabled() // when scanner is available
    member __.LatestPosition with get() = confocalAgent.GetPosition()

    member this.Axes   with get() = this.ImageParameters.Axes   and set v = this.ImageParameters <- { this.ImageParameters with Axes = v }
    member this.Width  with get() = this.ImageParameters.Width  and set v = this.ImageParameters <- { this.ImageParameters with Width = v }
    member this.Height with get() = this.ImageParameters.Height and set v = this.ImageParameters <- { this.ImageParameters with Height = v }
    member this.MaxU with get() = this.Axes |> activeAxisConfig |> axesMaximumDistance |> fst
    member this.MaxV with get() = this.Axes |> activeAxisConfig |> axesMaximumDistance |> snd
    member __.StepSize with get() = imageParameters.Value.StepSize and set v = imageParameters.Value <- { imageParameters.Value with StepSize = v }
    member __.IntegrationTime with get() = imageParameters.Value.IntegrationTime and set v = imageParameters.Value <- { imageParameters.Value with IntegrationTime = v }

    member __.ImageParameters with get() = imageParameters.Value and set v = imageParameters.Value <- v
    member __.LastImageParameters with get() = lastImageParameters.Value and set v = lastImageParameters.Value <- v

    member __.InformationText with get() = informationText.Value and set text = informationText.Value <- text
    member __.TakeImage = self.Factory.CommandSyncChecked(takeImage, allowImaging)
    member this.SaveImage = self.Factory.CommandSync(this.SaveImage')
    member this.ResetAxes = self.Factory.CommandSync(fun _ -> this.ImageModel.ResetAllAxes())

    member __.StopImaging = self.Factory.CommandSync((fun _ -> confocalAgent.CancelImaging()))
    member __.SetPosition() = confocalAgent.SetPosition (position imageParameters.Value)

    member this.X with get() = imageParameters.Value.X and set v = imageParameters.Value <- {imageParameters.Value with X = v}; sprintf "X <- %A" v |> logger.Debug; this.SetPosition()
    member this.Y with get() = imageParameters.Value.Y and set v = imageParameters.Value <- {imageParameters.Value with Y = v}; sprintf "Y <- %A" v |> logger.Debug; this.SetPosition()
    member this.Z with get() = imageParameters.Value.Z and set v = imageParameters.Value <- {imageParameters.Value with Z = v}; sprintf "Z <- %A" v |> logger.Debug; this.SetPosition()

    member __.U with get() = (Image.planeCentre imageParameters.Value) |> fst
    member __.V with get() = (Image.planeCentre imageParameters.Value) |> snd

    member this.U0 with get() = this.U - (lastImageParameters.Value.Width * 0.5m) |> atLeast 0.0m<um>
    member this.U1 with get() = this.U + (lastImageParameters.Value.Width * 0.5m) |> atMost this.MaxU
    member this.V0 with get() = this.V - (lastImageParameters.Value.Height * 0.5m) |> atLeast 0.0m<um>
    member this.V1 with get() = this.V + (lastImageParameters.Value.Height * 0.5m) |> atMost this.MaxV

    member __.AvailablePlanes = [ Position.Path.XY; Position.Path.XZ; Position.Path.YZ ]
    member this.AxisU with get() = this.Axes |> Image.axisLabels |> fst
    member this.AxisV with get() = this.Axes |> Image.axisLabels |> snd

    member __.LogView with get() = match base.View with LogImage -> true | LinearImage -> false
                       and set v = if v then base.View <- LogImage else base.View <- LinearImage

    member __.Data with get() = logger.Debug "Asked for Data"; data.Value

    member this.SetPositionInPlane (a,b) =
        let (a',b') = (coerce this.U0 this.U1 a, coerce this.V0 this.V1 b)
        match self.Axes with
        | Position.Path.Plane.XY -> self.X <- a'; self.Y <- b'
        | Position.Path.Plane.XZ -> self.X <- a'; self.Z <- b'
        | Position.Path.Plane.YZ -> self.Y <- a'; self.Z <- b'

    member private __.SaveImage'() =
        let imageParameters = lastImageParameters
        let directory = "%HOMEPATH%\\Experiments\\Confocal\\Images" |> Environment.ExpandEnvironmentVariables
        IO.Directory.CreateDirectory directory |> ignore
        let stem = directory + "\\confocal-" + DateTime.Now.ToString("hhmm_ddMMMyy")
        logger.Debug <| sprintf "Saving image to %s.png" stem
        System.IO.File.WriteAllText(stem + ".txt", sprintf "%A" imageParameters)
        base.SaveImage stem
        self.InformationText <- sprintf "Image saved to %s.png" stem
