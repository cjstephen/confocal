﻿// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Acquisition

open Endorphin.Instrument.PicoQuant
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Core
open Endorphin.Instruments
open FSharp.Control.Reactive
open System

type Intensity = float</s>

module Acquisition =

    let logger = log4net.LogManager.GetLogger "PhotonAcquisition"

    let picoHarpScan (picoHarp:PicoHarp.PicoHarpHandle) integrationTime numberOfPoints =
        PicoHarp.scan picoHarp integrationTime numberOfPoints

    let toFloat (x:int<'T>) = x |> float |> LanguagePrimitives.FloatWithMeasure<'T>
    let intToIntensity x : Intensity = x |> float |> LanguagePrimitives.FloatWithMeasure</s>
    let floatToIntensity x : Intensity = x |> LanguagePrimitives.FloatWithMeasure</s>

    let minimumOf a b =
        if a < b then
            a 
        else
            b

    let scan detectorId lease (points:'a[]) =
        match detectorId with
//        | PicoHarpId picoHarp ->
//            let picoHarpHandle = Instruments.getPicoHarp picoHarp lease
//            let integrationTime = integrationTime |> toFloat
//            PicoHarp.scan picoHarpHandle integrationTime numberOfPoints
//        | TimeTaggerId tt ->
//            let timeTagger = Instruments.getTimeTagger tt lease
//            timeTagger
        | PhotonCounterId pc ->
            let pcHandle = Instruments.getPhotonCounter pc lease
            pcHandle.ExternalTrigger()
            pcHandle.EmitRate()
            let points' = Array.concat [ points ]
            pcHandle.Rate()
            |> Observable.map intToIntensity
            |> Observable.skip 1
            |> Observable.take points'.Length
            |> Observable.zip (Observable.ofSeq points')

        | _ -> failwith "No detector available for confocal scan"

    let liveCount detectorId lease =
        match detectorId with
        | PicoHarpId picoHarp ->
            let picoHarpHandle = Instruments.getPicoHarp picoHarp lease
            PicoHarp.liveCounts picoHarpHandle |> Observable.map (fun (a,b) -> a+b)
        | PhotonCounterId pc ->
            let pcHandle = Instruments.getPhotonCounter pc lease
            pcHandle.InternalTrigger 100 // ms
            pcHandle.EmitRate()
            pcHandle.Rate() |> Observable.throttle (TimeSpan.FromSeconds 0.1) |> Observable.map (float >> (*) 1.</s>)
        | _ -> failwith "Invalid detector for live counts"

//        let! waitToFinish =
//            histogramObservable
//            |> Observable.take numberOfPoints
//            |> Observable.last
//            |> Async.AwaitObservable
//            |> Async.Ignore
//            |> Async.StartChild
//        return waitToFinish

