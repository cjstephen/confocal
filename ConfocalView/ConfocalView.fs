﻿namespace Confocal

open FsXaml
open OxyPlot
open System.Windows.Input

type MouseEvent =
    | PlotClick of MouseButtonEventArgs
    | Ignorable
module EventConverters =

    let clickConverter (args : MouseButtonEventArgs) =
        printfn "click converter"
        PlotClick args

    let Default = Ignorable

type ClickConverter() = inherit EventArgsConverter<MouseButtonEventArgs,MouseEvent>(EventConverters.clickConverter, EventConverters.Default)
type ConfocalView = XAML<"Confocal.xaml">


