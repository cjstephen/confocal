// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Launcher

open System.Windows
open System.Linq
open System
open System.IO
open ViewModule
open ViewModule.FSharp
open FsXaml
open log4net
open Endorphin.Instruments
open Xceed.Wpf.Toolkit
open Endorphin.Instrument.FastScanningController
open FSharp.Data.UnitSystems.SI.UnitSymbols

module App =
    let baseDirectory = "%HOMEPATH%\\Experiments\\Confocal" |> Environment.ExpandEnvironmentVariables
    let log4netConfigPath = baseDirectory + "\\log4net.config"

    let log4netDefaultTemplate = """
<log4net>

  <appender name="FileAppender" type="log4net.Appender.FileAppender">
    <file type="log4net.Util.PatternString" value="__LOGDIR__\log.txt" />
    <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
    <encoding value="utf-8" />
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%date [%thread] %level %logger - %message%newline" />
    </layout>
  </appender>
  
  <appender name="ObservableAppender" type="Endorphin.LogConsole.ObservableAppender, Endorphin.LogConsole">
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%date [%thread] %level %logger - %message%newline" />
    </layout>
    <filter type="log4net.Filter.LoggerMatchFilter">
      <loggerToMatch value="Visa" />
      <acceptOnMatch value="false" />
    </filter>
    <filter type="log4net.Filter.LevelRangeFilter">
       <levelMin value="INFO" />
       <levelMax value="FATAL" />
    </filter>
  </appender>

  <root>
    <level value="ALL" />
    <appender-ref ref="ObservableAppender" />
    <appender-ref ref="FileAppender" additivity="false" />
  </root>

</log4net>
"""
    let log4netDefaultConfig = log4netDefaultTemplate.Replace("__LOGDIR__",baseDirectory)

    // Set up logging with a configuration file in the home directory
    do
        if not <| File.Exists(log4netConfigPath) then
            try
                File.WriteAllText(log4netConfigPath,log4netDefaultConfig)
            with
            | error -> failwithf "Failed to create log4net configuration file %A" log4netConfigPath
        log4net.Config.XmlConfigurator.Configure(new FileInfo(log4netConfigPath)) |> ignore

    let createWorkingDirectory() =
        try
            // Create a new curent directory and change to it
            let directoryInfo = IO.Directory.CreateDirectory baseDirectory
            Environment.CurrentDirectory <- baseDirectory

            // Reset the log4net configuration & reopen in the new location
            GlobalContext.Properties.Item("logdir") <- baseDirectory

        with
        | exn -> failwithf "Failed to create new working directory %s: %A" baseDirectory exn

    type LauncherWindow = XAML<"LauncherWindow.xaml">
    type ImagingWindow = XAML<"ImagingWindow.xaml">
    type LiveCountsWindow = XAML<"LiveCountsWindow.xaml">
    type LogWindow = XAML<"LogWindow.xaml">
    type cwOdmrWindow = XAML<"cwOdmrWindow.xaml">
    type App = XAML<"App.xaml">
    
    let logger = log4net.LogManager.GetLogger "Launcher"

    let createWindow() =
        new LauncherWindow() :> Window

    let openWindow<'T when 'T :> Window> (createWindow: unit -> Window) =
        let existing = Application.Current.Windows.OfType<'T>().Cast<'T>()
        if existing.Any() then
            printfn "Already open"
            Seq.head existing |> (fun win -> win.Activate() |> ignore)
        else
            // open a new one
            printfn "Creating"
            let window = createWindow()
            window.Show()
            window.Activate() |> printfn "Activated? %A"

    let openLogConsole() =
        printfn "Opening logging"
        openWindow<LogWindow> (fun () ->
                                   let w = new LogWindow()
                                   w :> Window )

    let scannerSettings configuration identifier =
        let conf = Configuration.findScanningController identifier configuration
        let xcal = conf.calibration.x |> decimal |> LanguagePrimitives.DecimalWithMeasure<m/V>
        let ycal = conf.calibration.y |> decimal |> LanguagePrimitives.DecimalWithMeasure<m/V>
        let zcal = conf.calibration.z |> decimal |> LanguagePrimitives.DecimalWithMeasure<m/V>
        let xmax = conf.limits.x |> decimal |> LanguagePrimitives.DecimalWithMeasure<V>
        let ymax = conf.limits.y |> decimal |> LanguagePrimitives.DecimalWithMeasure<V>
        let zmax = conf.limits.z |> decimal |> LanguagePrimitives.DecimalWithMeasure<V>
        { X = match xcal with 0.0m<m/V> -> None | _ -> Some { Calibration = xcal; VoltageLimit = xmax }
          Y = match xcal with 0.0m<m/V> -> None | _ -> Some { Calibration = ycal; VoltageLimit = ymax }
          Z = match xcal with 0.0m<m/V> -> None | _ -> Some { Calibration = zcal; VoltageLimit = zmax } }



    type LauncherVM(connectionManager:Endorphin.Connection.ConnectionManager.ConnectionManager<InstrumentId>,configuration) as self =
        inherit ViewModelBase()
        let confocalDetectionInstrument = Configuration.confocalDetectionInstrument configuration
        let confocalConfiguration = { Confocal.CountInstrument = confocalDetectionInstrument
                                      Confocal.ScanningControllerSettings = scannerSettings configuration "ScanningController" }
        let confocalVM = new Confocal.ConfocalVM(connectionManager,(fun _ -> confocalConfiguration))
        let cwOdmrVM = new cwODMR.CwOdmrVM(connectionManager)
        let liveCountsVM = new LiveCounts.LiveCountsPhotonCounterVM(connectionManager)

        let imagingWindow =
            let imagingWindow = new ImagingWindow()
            imagingWindow.DataContext <- confocalVM
            imagingWindow.Closing.Add (fun x -> x.Cancel <- true // Don't really close the window
                                                logger.Debug "Hiding imaging"
                                                imagingWindow.Hide()
                                                confocalVM.Disconnect())
            imagingWindow
        
        let openImagingWindow _ =
            confocalVM.Connect()
            let w = imagingWindow :> Window
            w.Show()
            w.Activate() |> ignore

        let liveCountsWindow =
            let liveCountsWindow = new LiveCountsWindow()
            liveCountsWindow.DataContext <- liveCountsVM
            liveCountsWindow.Closing.Add (fun x ->
                x.Cancel <- true
                logger.Debug "Hiding live counts"
                liveCountsWindow.Hide()
                liveCountsVM.Stop())
            liveCountsWindow

        let openLiveCountsWindow _ =
            logger.Debug "Showing live counts"
            liveCountsVM.Start()
            liveCountsWindow.Show()
            liveCountsWindow.Activate() |> ignore

        let logConsoleWindow =
            let logConsoleWindow = new LogWindow()
            logConsoleWindow.Closing.Add (fun x ->
                x.Cancel <- true
                logger.Debug "Hiding log window"
                logConsoleWindow.Hide())
            logConsoleWindow

        let openLogConsoleWindow _ =
            logConsoleWindow.Show()
            logConsoleWindow.Activate() |> ignore

        let cwOdmrWindow =
            let cwOdmrWindow = new cwOdmrWindow()
            cwOdmrWindow.DataContext <- cwOdmrVM
            cwOdmrWindow.Closing.Add (fun x ->
                                        x.Cancel <- true
                                        logger.Debug "Hiding cwODMR window"
                                        cwOdmrWindow.Hide())
            cwOdmrWindow

        let openCwOdmrWindow _ =
            cwOdmrWindow.Show()
            cwOdmrWindow.Activate() |> ignore

        member __.ConnectionIndicators = connectionManager.ConnectionIndicators
        member __.OpenImaging = self.Factory.CommandSync openImagingWindow
        member __.OpenCwOdmr = self.Factory.CommandSync openCwOdmrWindow
        member __.OpenLiveCounts = self.Factory.CommandSync openLiveCountsWindow
        member __.OpenLogging = self.Factory.CommandSync openLogConsoleWindow
        member __.ConnectAll = self.Factory.CommandSync connectionManager.ConnectAll
        member __.UpdateAll = self.Factory.CommandSync (fun _ -> connectionManager.ConnectionIndicators |> List.iter (fun x -> x.Update()))
        member __.DisconnectAll = self.Factory.CommandSync connectionManager.DisconnectAll

    [<STAThread>]
    [<EntryPoint>]
    let main _ =
        createWorkingDirectory()
//        let fs = new FileStream("fseprstdout.txt", FileMode.Create)

        // First, save the standard output.
//        let sw = new StreamWriter(fs)
//        sw.AutoFlush <- true
//        Console.SetOut(sw)

        Wpf.installSynchronizationContext()
        let logger = log4net.LogManager.GetLogger "Main"
        
        let configurationFile = Path.Combine [| baseDirectory ; "instruments.yaml" |]
        let configuration = Configuration.loadConfiguration configurationFile
        do printfn "Configuration in: %A" configuration
        let connectionManager = Instruments.createConnectionManager configuration

//        Application.Current.Properties.Add("connections",connectionManager)
//        Application.Current.Properties.Add("configuration",configuration)

        let launcherVM = new LauncherVM(connectionManager,configuration)
        Wpf.installSynchronizationContext()
        let launcherWindow = new LauncherWindow()
        launcherWindow.DataContext <- launcherVM
        launcherWindow |> App().Run
