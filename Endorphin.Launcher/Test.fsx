﻿#r "../packages/Endorphin.Core/lib/net452/Endorphin.Core.dll"
#r "../packages/log4net/lib/net45-full/log4net.dll"
#r "System.Core.dll"
#r "System.dll"
#r "System.Numerics.dll"
#r "../packages/System.Reactive.Core/lib/net45/System.Reactive.Core.dll"
#r "../packages/System.Reactive.Interfaces/lib/net45/System.Reactive.Interfaces.dll"
#r "../packages/System.Reactive.Linq/lib/net45/System.Reactive.Linq.dll"
#r "../packages/System.Reactive.PlatformServices/lib/net45/System.Reactive.PlatformServices.dll"
#r "../packages/FSharp.Control.Reactive/lib/net45/FSharp.Control.Reactive.dll"

open FSharp.Control.Reactive
open System.Reactive.Linq

let t = seq { 1 .. 20 } |> Observable.ofSeq

Observable.window 

Observable.Window( t, 5, 2)
|> Observable.map Observable.toArray
|> Observable.mergeInner
|> Observable.map (Array.map (sprintf "%d") >> Array.toSeq >> String.concat ", ")
|> Observable.add (printfn "%s") 