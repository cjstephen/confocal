// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

#I "../packages"
#r "FSharp.Data/lib/net40/Fsharp.Data.dll"
#r "Newtonsoft.Json/lib/net45/Newtonsoft.Json.dll"
#r "FifteenBelow.Json/lib/net40/FifteenBelow.Json.dll"
#r "Endorphin.Core/lib/net452/Endorphin.Core.dll"
#r "Endorphin.Core.NationalInstruments/lib/net452/Endorphin.Core.NationalInstruments.dll"
#r "Endorphin.Abstract/lib/net452/Endorphin.Abstract.dll"
#r "log4net/lib/net40-full/log4net.dll"
#r "Rx-Core/lib/net45/System.Reactive.Core.dll"
#r "Rx-Interfaces/lib/net45/System.Reactive.Interfaces.dll"
#r "Rx-Linq/lib/net45/System.Reactive.Linq.dll"
#r "Rx-PlatformServices/lib/net45/System.Reactive.PlatformServices.dll"
#r "FSharp.Control.Reactive/lib/net40/FSharp.Control.Reactive.dll"
#r "FSharp.ViewModule.Core/lib/portable-net45+netcore45+wpa81+wp8+MonoAndroid1+MonoTouch1/FSharp.ViewModule.dll"
#r "Extended.Wpf.Toolkit/lib/net40/Xceed.Wpf.Toolkit.dll"
#r "FsXaml.Wpf/lib/net45/FsXaml.Wpf.dll"
#r "FsXaml.Wpf/lib/net45/FsXaml.Wpf.TypeProvider.dll"
#r "OxyPlot.Core/lib/net45/OxyPlot.dll"
#r "OxyPlot.Wpf/lib/net45/OxyPlot.Wpf.dll"
#r "Endorphin.Instrument.SwabianInstruments.PulseStreamer8/lib/net452/Endorphin.Instrument.SwabianInstruments.PulseStreamer8.dll"
#r "Endorphin.Instrument.PicoQuant.PicoHarp300/lib/net452/Endorphin.Instrument.PicoQuant.PicoHarp300.dll"
#r "../Endorphin.Instrument.FastScanningController/bin/Debug/Endorphin.Instrument.FastScanningController.dll"
#r "Endorphin.Instrument.Keysight.N5172B/lib/net452/Endorphin.Instrument.Keysight.N5172B.dll"
#r "../Endorphin.Experiment/bin/debug/Endorphin.Experiment.dll"
#r "../Endorphin.Experiment.Confocal/bin/debug/Endorphin.Experiment.Confocal.dll"
#r "../Endorphin.UI/bin/Debug/Endorphin.UI.dll"
#r "WindowsBase.dll"

#load "../paket-files/WarwickEPR/Endorphin.Core/src/Endorphin.Core.Control.fs"
#load "../paket-files/WarwickEPR/Endorphin.Core/src/Endorphin.Core.Reactive.fs"

open System
open Endorphin.Core
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Instrument.PicoQuant
open Endorphin.Instrument.FastScanningController.Model
open Endorphin.Instrument.FastScanningController.Instrument
open Endorphin.Instrument.Keysight.N5172B
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open Endorphin.Experiment.Confocal.Autofocus
open Endorphin.Instrument.SwabianInstruments
open Endorphin.Instrument

let picoHarpStreamingAcquisition =
    let picoHarp = PicoHarp300.PicoHarp.Initialise.initialise "1020854" PicoHarp300.Model.SetupInputChannel.T2 |> Async.RunSynchronously 
    let streamingAcquisition = PicoHarp300.Streaming.Acquisition.create picoHarp
    let streamingAcquisitionHandle = PicoHarp300.Streaming.Acquisition.start streamingAcquisition 
    streamingAcquisition

// init. fast scanning controller
let scanningControllerAgent = 
    let calibration = {X = 8e-6m<m/V>; Y = 8e-6m<m/V>; Z = 8e-6m<m/V>}
    let scanningController = ScanningController.openInstrument "COM3" 1000<ms> calibration |> Async.RunSynchronously
    Endorphin.Core.QueuedAccessAgent.create scanningController

let pulseGeneratorAgent = Async.RunSynchronously <| async {
    let! pulseStreamer = PulseStreamer.openDevice("http://192.168.1.100:8050/json-rpc")
    return Endorphin.Core.QueuedAccessAgent.create pulseStreamer
}

let keysightAgent = Async.RunSynchronously <| async {
    let! keysight = RfSource.openInstrument "TCPIP0::192.168.1.101::inst0::INSTR" 100000<ms>
    return Endorphin.Core.QueuedAccessAgent.create keysight
}

let keysightAgent2 = Async.RunSynchronously <| async {
    let! keysight = RfSource.openInstrument "TCPIP0::192.168.1.102::inst0::INSTR" 100000<ms>
    return Endorphin.Core.QueuedAccessAgent.create keysight
}


// load autofocus
let autofocusWindow = Ui.createAutofocusWindow picoHarpStreamingAcquisition pulseGeneratorAgent scanningControllerAgent
let dispatcher = System.Windows.Threading.Dispatcher.CurrentDispatcher
let autofocusAutomator = Automation.createAgent autofocusWindow dispatcher
autofocusWindow.Show()

#load "../Endorphin.Experiment/Confocal/Confocal.fsx"
#load "../Endorphin.Experiment/Confocal/Stage.fsx"
#load "../Endorphin.Experiment/Confocal/LiveCounts.fsx"
#load "../Endorphin.Experiment.Confocal/Autocorrelation/Autocorrelation.fsx"
#load "../Endorphin.Experiment.Odmr/Odmr/Odmr.fsx"
#load "../Endorphin.Experiment.Odmr/Rabi.fsx"