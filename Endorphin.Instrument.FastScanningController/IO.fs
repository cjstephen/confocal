﻿// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Instrument.FastScanningController

open Endorphin.Core
open Endorphin.Abstract.Position.Path
open Endorphin.Abstract.Position.Point
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols

module internal IO =

    let private logger = log4net.LogManager.GetLogger("ScanningController")

    /// Performs a query for the value corresponding to the given key and parses it with the given
    /// parsing function.
    let private queryValue parseFunc key (ScanningController scanningController) = async {
        let! response = (sprintf "%s\r\n" key) |> Visa.String.query scanningController
        return parseFunc response }
    
    /// Sets the value corresponding to the given key to the instrument after converting it to a
    /// string with the provided string conversion function.
    let private setValue stringFunc (ScanningController scanningController) value = async {
        (sprintf "%s\r\n" (stringFunc value)) |> Visa.String.write scanningController }

    let private writeCommand (ScanningController scanningController) command = async {
        (sprintf "%s\r\n" command) |> Visa.String.write scanningController }

    let private writePathVoltage (ScanningController scanningController) (voltages : VoltagePoint) =
        (sprintf "%s%.4M,%.4M,%.4M\r\n" Keys.voltagePoint (tfst voltages / 1m<V>) (tsnd voltages / 1m<V>) (ttrd voltages / 1m<V>)) |> Visa.String.write scanningController

    let private writeDacValues (ScanningController scanningController) pointByteArray = 
         do pointByteArray |> Visa.Bytes.write scanningController

    let private readUploadAcknowledgement parseFunc (ScanningController scanningController) = async {
        let! response = Visa.String.read scanningController ()
        return parseFunc response }

    let private scanningControllerHandle (ScanningController scanningController) = scanningController

    let setDwell = setValue dwellString

    let setTriggerDelay = setValue triggerDelayString

    let setTriggerPulseLength = setValue triggerLengthString

    let getCurrentVoltages = queryValue parseVoltages (sprintf "%s?" Keys.voltagePoint)

    let setCurrentVoltages = setValue voltageString

    let getNumberOfPoints = queryValue parseNumberOfPoints (sprintf "%s?" Keys.numberQuery) 

    let beginUpload numberOfElements = queryValue parseUploadAcknowledgement (uploadString numberOfElements)

    let beginAsciiUpload numberOfElements = queryValue parseUploadAcknowledgement (uploadAsciiString numberOfElements)

    let finishUpload = readUploadAcknowledgement parseUploadCompletion

    let writePath (controller : ScanningController) (settings: ScanningControllerSettings) (path : Path)  = async {
        let numPoints = Array.length (points path)

        let pointArray = 
            points path
            |> Array.map (fun point -> pointToDacValue settings (path |> coordinateForPoint point))
            |> Array.concat
            |> Array.chunkBySize 64

        do! beginUpload numPoints controller

        logger.Debug <| sprintf "writing %d points" numPoints

        (*points path
        |> Array.map (fun point -> pointToDacValue controller (path |> coordinateForPoint point))
        |> Array.reduce (fun points point -> Array.append points point)
        |> Array.chunkBySize 64*)
        pointArray
        |> Array.iter (writeDacValues controller)

        do! finishUpload controller }

    let writeAsciiPath (controller : ScanningController) (settings:ScanningControllerSettings) (path : Path)  = async {
        let numPoints = Array.length (points path)
        do! beginAsciiUpload numPoints controller

        logger.Debug <| sprintf "writing %d ascii points" numPoints

        // Write each point to the controller. A periodic delay must be added because the 
        // write speed to EEPROM on the device is slower than the baud rate of the serial connection
        // and the buffer is only approximately 8k points
        let mutable i = 0
        for point in points path do
            i <- i + 1
            let voltages = pointToVoltage settings (path |> coordinateForPoint point)
            do writePathVoltage controller voltages

        do! finishUpload controller }

    let runPath (controller : ScanningController) = async {
        do! writeCommand controller <| sprintf "%s" Keys.runPath }

    let stopPath (controller : ScanningController) = async {
        do! writeCommand controller <| sprintf "%s" Keys.stopPath }