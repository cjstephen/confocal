// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Instrument.FastScanningController
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Core

type AxisSettings = {
    Calibration  : decimal<m/V>
    VoltageLimit : decimal<V> }

type ScanningControllerSettings = {
    X : AxisSettings option
    Y : AxisSettings option
    Z : AxisSettings option }

type ScanningController = internal ScanningController of scanningController : Visa.Instrument