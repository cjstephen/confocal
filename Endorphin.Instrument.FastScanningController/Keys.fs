// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Instrument.FastScanningController

[<RequireQualifiedAccess>]
/// Command keys for instrument.
module internal Keys =
    /// Dwell time
    let dwellTime = "DWELL"
    
    /// Delay time
    let delayTime = "DELAY"

    /// Number of points to upload to controller 
    let uploadNumber = "DL"

    /// Number of points to upload to controller (binary)
    let binaryUploadNumber = "DL BIN"

    /// Set or get a voltage
    let voltagePoint = "V"

    /// Number of points uploaded to controller
    let numberQuery = "N"

    /// Acknowledgement that path upload can begin
    let uploadAcknowledgement = "DL ACK"

    /// Acknowledgement that path upload has completed successfully
    let uploadComplete = "DL DONE"

    /// Run path
    let runPath = "RUN"

    /// Stop path
    let stopPath = "STOP"

    let triggerLength = "TRIG"