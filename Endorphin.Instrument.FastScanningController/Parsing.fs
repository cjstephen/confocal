// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Instrument.FastScanningController

open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Abstract.Position.Point
open Endorphin.Core
open MiscUtil

[<AutoOpen>]
module internal Parsing =
    let logger = log4net.LogManager.GetLogger("ScanningController")
 
    let dwellString (dwellTime : int<ms>) = 
        sprintf "%s %d" Keys.dwellTime dwellTime

    let triggerDelayString (triggerDelay : float<ms>) = 
        sprintf "%s %.1f" Keys.delayTime triggerDelay

    let triggerLengthString (duration : int<us>) = 
        sprintf "%s %dus" Keys.triggerLength duration

    let uploadString (numberOfPoints : int) = 
        sprintf "%s %d" Keys.binaryUploadNumber numberOfPoints

    let uploadAsciiString (numberOfPoints : int) = 
        sprintf "%s %d" Keys.uploadNumber numberOfPoints

    let voltageString (point : VoltagePoint) = 
        sprintf "%s %.3M,%.3M,%.3M" Keys.voltagePoint (tfst point / 1m<V>) (tsnd point / 1m<V>) (ttrd point / 1m<V>)

    let parseVoltages (response : string) : VoltagePoint = 
        try
            logger.Debug <| sprintf "parse Voltage %s" response
            let voltageString = response.Trim().Split [| ':' |] 
            let voltageStringArray = voltageString.[1].Split [| ',' |]

            (decimal voltageStringArray.[0] * 1m<V>, decimal voltageStringArray.[1] * 1m<V>, decimal voltageStringArray.[2] * 1m<V>)
        with exn ->
            failwith (sprintf "Bad response from the fast scanning controller: %s; exception: %A" response exn)

    let parseUploadAcknowledgement (response : string) = 
        if response.Trim() <> Keys.uploadAcknowledgement then
            failwith <| sprintf "Bad command to controller - will not acknowledge path upload. Response was: %s" response

    let parseUploadCompletion (response : string) = 
        if response.Trim() <> Keys.uploadComplete then
            failwith <| sprintf "Could not complete path upload. Response was: %s" response

    let parseNumberOfPoints (response : string) : int = 
        int <| response.Substring 4

[<AutoOpen>]
module internal Convert = 
    let round (x : decimal) = uint16 (System.Math.Round x)

    let componentToVoltage (settings:AxisSettings option) coordinate =
        match settings with
        | Some settings ->
            let v = coordinate * 1e-6m<m/um> / settings.Calibration
            if v <= settings.VoltageLimit then
                v
            else
                failwithf "Voltage %A exceeds limit %A" v settings.VoltageLimit
        | None ->
            // axis disabled
            0.0m<V>

    let voltageToDistance (settings:AxisSettings option) coordinate =
        match settings with
        | Some settings ->
            coordinate * 1e6m<um/m> * settings.Calibration
        | None ->
            // axis disabled
            0.0m<um>

    let pointToVoltage (settings:ScanningControllerSettings) ((x,y,z):Point) =
        (x |> componentToVoltage settings.X,
         y |> componentToVoltage settings.Y,
         z |> componentToVoltage settings.Z):VoltagePoint

    let pointToDacValue (settings:ScanningControllerSettings) (point:Point) =
        let voltages = pointToVoltage settings point
        let toDac x = (65535m / 11m) * (x / 1m<V>) |> round
        let toDacTuple (vx,vy,vz) = (toDac vx, toDac vy, toDac vz)
        let ushortToBigEndian  (x : uint16)  = Conversion.EndianBitConverter.Big.GetBytes x
    
        let bytes = Array.zeroCreate 6
        let (dacX,dacY,dacZ) = toDacTuple voltages
        bytes.[0..1] <- ushortToBigEndian dacX
        bytes.[2..3] <- ushortToBigEndian dacY
        bytes.[4..5] <- ushortToBigEndian dacZ
        bytes

    let voltageToPoint settings ((vx,vy,vz):VoltagePoint) =
        (voltageToDistance settings.X vx,
         voltageToDistance settings.Y vy,
         voltageToDistance settings.Z vz):Point
