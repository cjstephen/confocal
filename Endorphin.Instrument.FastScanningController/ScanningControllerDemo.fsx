// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

#r "../packages/Endorphin.Core/lib/net452/Endorphin.Core.dll"
#r "../packages/Endorphin.Abstract/lib/net452/Endorphin.Abstract.dll"
#r "./bin/Debug/Endorphin.Instrument.FastScanningController.dll"

open Endorphin.Instrument.FastScanningController.Instrument
open Endorphin.Instrument.FastScanningController
open Endorphin.Abstract.Position
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols

let calibration = {X = 8e-6m<m/V>; Y = 8e-6m<m/V>; Z = 8e-6m<m/V>};
async {
    printfn "connecting"
    let! scanningController = ScanningController.openInstrument "COM3" 5000<ms> calibration
    
    printfn "Moving to (10, 10, 10)"
    do! Position.setPosition scanningController (10m<um>, 10m<um>, 10m<um>)

    printfn "getting position"
    let! initialPosition = Position.getPosition scanningController
    printfn "Initial position is: %A" initialPosition

    let! finalPosition = Position.getPosition scanningController
    printfn "Final position is: %A" finalPosition

    printfn "Now going to write a path"
    let path = Path.createSquareSnake (0m<um>, 0m<um>, 60m<um>) 20m<um> 0.1m<um> Path.Plane.XY
    do! Position.Path.writePathToController scanningController path

    printfn "Setting dwell time"
    do! Timing.setDwellTime scanningController 10<ms>

    printfn "Setting trigger delay"
    do! Timing.setTriggerDelay scanningController 3.0<ms>

    printfn "Running path"
    do! Position.Path.runPath scanningController

    let! numPoints = Position.Path.getNumberOfPoints scanningController
    printfn "Number of points: %d" numPoints
} |> Async.RunSynchronously
