// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.


#r "../packages/Endorphin.Core/lib/net452/Endorphin.Core.dll"
#r "../packages/FSharp.Control.Reactive/lib/net40/FSharp.Control.Reactive.dll"
#r "../packages/FSharp.ViewModule.Core/lib/portable-net45+netcore45+wpa81+wp8+MonoAndroid1+MonoTouch1/FSharp.ViewModule.dll"
#r "../packages/Extended.Wpf.Toolkit/lib/net40/Xceed.Wpf.Toolkit.dll"
#r "../packages/FsXaml.Wpf/lib/net45/FsXaml.Wpf.dll"
#r "../packages/FsXaml.Wpf/lib/net45/FsXaml.Wpf.TypeProvider.dll"
#r "../packages/OxyPlot.Core/lib/net45/OxyPlot.dll"
#r "../packages/OxyPlot.Wpf/lib/net45/OxyPlot.Wpf.dll"
#r "../packages/OxyPlot.Wpf/lib/net45/OxyPlot.Xps.dll"
#r "PresentationCore.dll"
#r "PresentationFramework.dll"
#r "System.Core.dll"
#r "System.dll"
#r "System.Numerics.dll"
#r "../packages/Rx-Core/lib/net45/System.Reactive.Core.dll"
#r "../packages/Rx-Interfaces/lib/net45/System.Reactive.Interfaces.dll"
#r "../packages/Rx-Linq/lib/net45/System.Reactive.Linq.dll"
#r "../packages/Expression.Blend.Sdk/lib/net45/System.Windows.Interactivity.dll"
#r "System.Xaml.dll"
#r "System.Xml.dll"
#r "WindowsBase.dll"
#r "../packages/Endorphin.Instrument.SwabianInstruments.PulseStreamer8/lib/net452/Endorphin.Instrument.SwabianInstruments.PulseStreamer8.dll"
#r "../packages/Endorphin.Instrument.PicoQuant.PicoHarp300/lib/net452/Endorphin.Instrument.PicoQuant.PicoHarp300.dll"
#r "../packages/Endorphin.Instrument.Keysight.N5172B/lib/net452/Endorphin.Instrument.Keysight.N5172B.dll"
#r "../Endorphin.Experiment.Confocal/bin/Debug/Endorphin.Experiment.Confocal.dll"
#r "bin/Debug/Endorphin.Experiment.Odmr.dll"

open System
open System.Threading
open FsXaml
open Endorphin.Experiment.Odmr
open Endorphin.Experiment.Odmr.Tau
open Endorphin.Experiment.Odmr.Tau.TauExperiment
open Endorphin.Instrument
open Endorphin.Instrument.Keysight.N5172B
open Endorphin.Instrument.SwabianInstruments
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Core

let deerSequence (parameters : TauExperimentParameters) = 
   seq {   for j in 0..(parameters.ShotsPerPoint - 1) do      
               yield Pulse.empty
                   <| 200u
               yield Pulse.create 
                   <| [parameters.PulseStreamerParameters.MicrowaveXChannel] 
                   <| uint32 (parameters.AdditionalParameters.["pi2length"] :?> int)
               yield Pulse.empty
                   <| uint32 ((parameters.AdditionalParameters.["tauTime"] :?> int) - (((parameters.AdditionalParameters.["deerpilength"] :?> int) - (parameters.AdditionalParameters.["pilength"] :?> int))/2))
               yield Pulse.create
                   <| [Channel5]
                   <| uint32 (((parameters.AdditionalParameters.["deerpilength"] :?> int) - (parameters.AdditionalParameters.["pilength"] :?> int)) / 2)
               yield Pulse.create 
                   <| [parameters.PulseStreamerParameters.MicrowaveXChannel; Channel5] 
                   <| uint32 (parameters.AdditionalParameters.["pilength"] :?> int)
               yield Pulse.create
                   <| [Channel5]
                   <| uint32 (((parameters.AdditionalParameters.["deerpilength"] :?> int) - (parameters.AdditionalParameters.["pilength"] :?> int)) / 2)
               yield Pulse.empty
                   <| uint32 ((parameters.AdditionalParameters.["tauTime"] :?> int) - (((parameters.AdditionalParameters.["deerpilength"] :?> int) - (parameters.AdditionalParameters.["pilength"] :?> int))/2))
               yield Pulse.create
                   <| [parameters.PulseStreamerParameters.MicrowaveXChannel]
                   <| uint32 (parameters.AdditionalParameters.["pi2length"] :?> int)
               yield Pulse.empty
                   <| 200u
               yield Pulse.create
                   <| [parameters.PulseStreamerParameters.LaserChannel; parameters.PulseStreamerParameters.AcquisitionChannel]
                   <| 30u
               yield Pulse.create 
                   <| [parameters.PulseStreamerParameters.LaserChannel]
                   <| (uint32 (parameters.DetectionParameters.LaserDuration))
           if (Parameters.twoPhaseAcquisition parameters) then
               for j in 0..(parameters.ShotsPerPoint - 1) do      
                   yield Pulse.empty
                       <| 200u
                   yield Pulse.create 
                       <| [parameters.PulseStreamerParameters.MicrowaveXChannel] 
                       <| uint32 (parameters.AdditionalParameters.["pi2length"] :?> int)
                   yield Pulse.empty
                       <| uint32 ((parameters.AdditionalParameters.["tauTime"] :?> int) - (((parameters.AdditionalParameters.["deerpilength"] :?> int) - (parameters.AdditionalParameters.["pilength"] :?> int))/2))
                   yield Pulse.create
                       <| [Channel5]
                       <| uint32 (((parameters.AdditionalParameters.["deerpilength"] :?> int) - (parameters.AdditionalParameters.["pilength"] :?> int)) / 2)
                   yield Pulse.create 
                       <| [parameters.PulseStreamerParameters.MicrowaveXChannel; Channel5] 
                       <| uint32 (parameters.AdditionalParameters.["pilength"] :?> int)
                   yield Pulse.create
                       <| [Channel5]
                       <| uint32 (((parameters.AdditionalParameters.["deerpilength"] :?> int) - (parameters.AdditionalParameters.["pilength"] :?> int)) / 2)
                   yield Pulse.empty
                       <| uint32 ((parameters.AdditionalParameters.["tauTime"] :?> int) - (((parameters.AdditionalParameters.["deerpilength"] :?> int) - (parameters.AdditionalParameters.["pilength"] :?> int))/2))
                   yield Pulse.create
                       <| [parameters.PulseStreamerParameters.MicrowaveXChannel]
                       <| uint32 (parameters.AdditionalParameters.["pi3by2length"] :?> int)
                   yield Pulse.empty
                       <| 200u
                   yield Pulse.create
                       <| [parameters.PulseStreamerParameters.LaserChannel; parameters.PulseStreamerParameters.AcquisitionChannel]
                       <| 30u
                   yield Pulse.create 
                       <| [parameters.PulseStreamerParameters.LaserChannel]
                       <| (uint32 (parameters.DetectionParameters.LaserDuration))
           yield Pulse.create 
                   <| [Channel7; parameters.PulseStreamerParameters.LaserChannel] 
                   <| 30u
           yield Pulse.create
                   <| [parameters.PulseStreamerParameters.LaserChannel]
                   <| 1000000u }
   |> List.ofSeq
   |> Pulse.Transform.compensateHardwareDelays [parameters.PulseStreamerParameters.LaserChannel; parameters.PulseStreamerParameters.AcquisitionChannel; Channel7] (uint32 parameters.LaserSwitchDelay)
   |> Pulse.Transform.compensateHardwareDelays [parameters.PulseStreamerParameters.MicrowaveXChannel; parameters.PulseStreamerParameters.MicrowaveYChannel; Channel5] (uint32 parameters.MicrowaveSwitchDelay)

let startupFunction keysightAgent2 (experiment : TauExperiment) = async {
        let parameters = experiment.Parameters
        let startFreq = (float parameters.InitialTau) * 1E6<Hz>
        let stopFreq = (float parameters.InitialTau + float (parameters.TauInterval *(decimal parameters.NumberOfPoints))) * 1E6<Hz>

        use! deerSourceAgent = QueuedAccessAgent.requestControl keysightAgent2
        
        let sweepSettings =
            Sweep.Configure.frequencyStepSweepInHz startFreq stopFreq
            |> Sweep.Configure.withPoints (Parameters.numberOfPoints experiment.Parameters)
            |> Sweep.Configure.withFixedPowerInDbm (float (parameters.AdditionalParameters.["deerAmplitude"] :?> decimal) * 1.<dBm>)
            |> Sweep.Configure.withListTrigger (Some (External (Trigger1, Positive)))
        
        do! Sweep.Apply.stepSweep deerSourceAgent.Contents sweepSettings
        do! Basic.setOutput deerSourceAgent.Contents On }

let stopFunction keysightAgent2 _ = async { 
        use! deerSourceAgent = QueuedAccessAgent.requestControl keysightAgent2
        do! Basic.setOutput deerSourceAgent.Contents Off }
  
let tauVariableParameters = 
    { Unit    = "MHz"
      DefaultInitialValue = 10m
      DefaultInterval     = 0.1m
      Precision           = 0.1m }

let experimentBuilderParameters = TauExperimentBuilder.Parameters.create 
                                  <| "DEER"
                                  <| "DEER Frequency (MHz)"
                                  <| tauVariableParameters
                                  <| true
                                  <| ([("pi2length", ("π/2 length", OdmrExperimentBuilder.IntParameterInRange (0, 10000, 1, 30), "ns"));            // readout spin pi/2
                                       ("pilength", ("π length", OdmrExperimentBuilder.IntParameterInRange (0, 10000, 1, 60), "ns"));               // readout spin pi
                                       ("pi3by2length", ("3π/2 length", OdmrExperimentBuilder.IntParameterInRange (0, 10000, 1, 90), "ns"));        // readout spin 3pi/2
                                       ("tauTime", ("Evo. time", OdmrExperimentBuilder.IntParameterInRange (0, 10000, 1, 300), "ns"));              // spin evo. time (tau; pulse separation)
                                       ("deerAmplitude", ("RF ampl.", OdmrExperimentBuilder.DecimalParameterInRange (-50m, 0m, 0.1m, -5.5m), "dBm"));  // RF amplitude
                                       ("deerpilength", ("πRF length", OdmrExperimentBuilder.IntParameterInRange (0, 10000, 1, 100), "ns"))]        // RF pi length
                                       |> TauExperimentBuilder.CommonTauExperimentFunctions.addOrderId 
                                       |> Map.ofList)
                                  <| deerSequence
                                  <| TauExperimentBuilder.CommonTauExperimentFunctions.interleavedTwoPhaseCurveProjectionCalculator
                                  <| (startupFunction keysightAgent2)
                                  <| (stopFunction keysightAgent2)

let imagePath = String.concat "/" [__SOURCE_DIRECTORY__; "images/deer.png"]
let deerWindow = TauExperimentBuilder.Experiment.createExperimentWindow experimentBuilderParameters 1400. 860. imagePath picoHarpStreamingAcquisition keysightAgent pulseGeneratorAgent autofocusAutomator
deerWindow.Show()
