// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr

open System
open System.Threading
open System.Windows.Controls
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.Control.Reactive
open FSharp.ViewModule.Validation
open FSharp.Control.Reactive
open Xceed.Wpf.Toolkit

open Endorphin.Core
open Endorphin.Instrument.PicoQuant.PicoHarp300
open Endorphin.Instrument.Keysight
open Endorphin.Instrument.SwabianInstruments

module OdmrExperimentBuilder = 
    type ParameterField = 
        | IntParameterInRange       of minimum : int * maximum : int * step : int * defaultValue : int
        | DecimalParameterInRange   of minimum : decimal * maximum : decimal * step : decimal * defaultValue : decimal

    [<AbstractClass>]
    type IParameterFieldControl () = 
        inherit Grid ()
    
        abstract member Value : ValueType with get, set
    
    type ParameterFieldIntegerUpDownControl (parameterName, label, min, max, step, defaultValue, unit) as self = 
        inherit IParameterFieldControl ()
    
        let nameLabel = new Label()
        let integerControl = new IntegerUpDown()
        let unitLabel = new Label()

        do 
            nameLabel.Content <- label
       
            integerControl.Margin <- Windows.Thickness (5.)
            integerControl.Minimum <- Nullable(min)
            integerControl.Maximum <- Nullable(max)
            integerControl.Increment <- Nullable(step)
            integerControl.Value <- Nullable (defaultValue)

            unitLabel.Content <- unit
    
            let leftColumn = new ColumnDefinition()
            leftColumn.Width <- Windows.GridLength(75.)
            self.ColumnDefinitions.Add(leftColumn)
            
            self.ColumnDefinitions.Add(new ColumnDefinition())

            let rightColumn = new ColumnDefinition()
            rightColumn.Width <- Windows.GridLength(50.)
            self.ColumnDefinitions.Add(rightColumn)

            Grid.SetColumn(nameLabel, 0)
            Grid.SetColumn(integerControl, 1)
            Grid.SetColumn(unitLabel, 2)
    
            self.Children.Add(nameLabel) |> ignore
            self.Children.Add(integerControl) |> ignore
            self.Children.Add(unitLabel) |> ignore
            self.HorizontalAlignment <- Windows.HorizontalAlignment.Stretch

            self.Name <- parameterName

        let __ = 
            integerControl.ValueChanged.Subscribe (fun x ->
                self.Value <- x.NewValue :?> ValueType )
    
        override self.Value 
            with get()     = integerControl.Value :> ValueType
            and  set value = integerControl.Value <- Nullable (value :?> int)

    type ParameterFieldDecimalUpDownControl (parameterName, label, min, max, step, defaultValue, unit) as self = 
         inherit IParameterFieldControl ()
     
         let nameLabel = new Label()
         let decimalControl = new DecimalUpDown()
         let unitLabel = new Label()
    
         do 
             nameLabel.Content <- label
        
             decimalControl.Margin <- Windows.Thickness (5.)
             decimalControl.Minimum <- Nullable(min)
             decimalControl.Maximum <- Nullable(max)
             decimalControl.Increment <- Nullable(step)
             decimalControl.Value <- Nullable (defaultValue)
    
             unitLabel.Content <- unit
     
             let leftColumn = new ColumnDefinition()
             leftColumn.Width <- Windows.GridLength(75.)
             self.ColumnDefinitions.Add(leftColumn)
             
             self.ColumnDefinitions.Add(new ColumnDefinition())
    
             let rightColumn = new ColumnDefinition()
             rightColumn.Width <- Windows.GridLength(50.)
             self.ColumnDefinitions.Add(rightColumn)
    
             Grid.SetColumn(nameLabel, 0)
             Grid.SetColumn(decimalControl, 1)
             Grid.SetColumn(unitLabel, 2)
     
             self.Children.Add(nameLabel) |> ignore
             self.Children.Add(decimalControl) |> ignore
             self.Children.Add(unitLabel) |> ignore
             self.HorizontalAlignment <- Windows.HorizontalAlignment.Stretch
    
             self.Name <- parameterName
    
         let __ = 
             decimalControl.ValueChanged.Subscribe (fun x ->
                 self.Value <- x.NewValue :?> ValueType )
     
         override self.Value 
             with get()     = decimalControl.Value :> ValueType
             and  set value = decimalControl.Value <- Nullable (value :?> decimal)
    
    let ParameterControlFactory (parameterName : string) (controlData : (string * ParameterField * string)) : IParameterFieldControl =
        let label, parameterType, unit = controlData
    
        match parameterType with
        | IntParameterInRange (min, max, step, defaultValue) ->
            ParameterFieldIntegerUpDownControl (parameterName, label, min, max, step, defaultValue, unit) :> IParameterFieldControl
        | DecimalParameterInRange (min, max, step, defaultValue) ->
            ParameterFieldDecimalUpDownControl (parameterName, label, min, max, step, defaultValue, unit) :> IParameterFieldControl

    type ExperimentParameterFields = Map<string, int * string * ParameterField * string> // display order, unique name, label, type, unit