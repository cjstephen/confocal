// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr

[<AutoOpen>]
module ExperimentUtilities =
    module DuHelper = 
        /// convert a DU element to the index of that element in the parent type
        let findDuStringIndexFromValue (item: 'T) = 
            let itemName = 
                match Microsoft.FSharp.Reflection.FSharpValue.GetUnionFields (item, typeof<'T>) with
                | case, _   -> case.Name

            let duNames = 
                Microsoft.FSharp.Reflection.FSharpType.GetUnionCases typeof<'T>
                |> Array.map(fun e -> e.Name)  

            Array.findIndex (fun name -> name = itemName) duNames

        /// find the appropriate DU element from the index of its string representation
        let findDuValueFromStringIndex<'T> (index: int) =  
            let case = 
                Microsoft.FSharp.Reflection.FSharpType.GetUnionCases typeof<'T>
                |> Array.item index

            Microsoft.FSharp.Reflection.FSharpValue.MakeUnion (case, [||]) :?> 'T
