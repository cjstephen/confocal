// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr.Odmr

open System
open System.Threading
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.Control.Reactive
open FSharp.ViewModule.Validation
open FSharp.Control.Reactive

open Endorphin.Core
open Endorphin.Instrument.PicoQuant.PicoHarp300
open Endorphin.Instrument.Keysight.N5172B
open Endorphin.Instrument.SwabianInstruments
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open OdmrExperiment

module PulsedOdmrExperiment =
    [<AutoOpen>]
    module Model =        
        type PulsedOdmrDetectionParameters = 
            { HistogramLength            : int<ns>
              LaserDuration              : int<ns> }

        type PulsedOdmrCurveProjectionParameters = 
            { IntegrationWindowOffset    : int<ns>
              IntegrationWindowWidth     : int<ns> }

        type PulsedOdmrExperimentParameters = 
            { InitialFrequency           : float<Hz>
              FrequencyDelta             : float<Hz>
              NumberOfPoints             : int
              Amplitude                  : float<dBm>
              Integrate                  : bool
              PulseStreamerParameters    : PulseStreamerParameters
              PiLength                   : int<ns>
              ShotsPerPoint              : int
              LaserSwitchDelay           : int<ns>
              MicrowaveSwitchDelay       : int<ns>
              PulsedDetectionParameters  : PulsedOdmrDetectionParameters 
              Notes                      : Notes
              Date                       : DateTime }

        type Histograms = int[,]

        type Curve = (int64 * int)[]

        type PulsedOdmrSignalUpdate =
            | HistogramsAvailable  of histograms : Histograms
            | CurveAvailable of odmrCurve : Curve

        type PulsedOdmrExperimentStatus = 
            | StartingExperiment
            | PerformingExperiment
            | FinishedExperiment

        type PulsedOdmrExperimentResult =
            | ExperimentCompleted
            | ExperimentError of exn

        type PulsedOdmrExperiment = 
            { Parameters                   : PulsedOdmrExperimentParameters
              PicoHarpStreamingAcquisition : Streaming.StreamingAcquisition
              PulseGeneratorAgent          : QueuedAccessAgent<PulseStreamer8.Model.PulseStreamer8>
              MicrowaveSourceAgent         : QueuedAccessAgent<Endorphin.Instrument.Keysight.N5172B.Model.RfSource>
              StopHandle                   : ManualResetEvent
              AcquisitionTokenSource       : CancellationTokenSource
              StatusChanged                : NotificationEvent<PulsedOdmrExperimentStatus>
              DataAvailable                : Event<PulsedOdmrSignalUpdate>
              ProjectionParameters         : Event<PulsedOdmrCurveProjectionParameters> }

    module Parameters = 
        // query functions
        let initialFrequency (parameters : PulsedOdmrExperimentParameters) = parameters.InitialFrequency

        let frequencyDelta (parameters : PulsedOdmrExperimentParameters) = parameters.FrequencyDelta

        let numberOfPoints (parameters : PulsedOdmrExperimentParameters) = parameters.NumberOfPoints

        let amplitude (parameters : PulsedOdmrExperimentParameters) = parameters.Amplitude

        let piLength (parameters : PulsedOdmrExperimentParameters) = parameters.PiLength

        let integrate (parameters : PulsedOdmrExperimentParameters) = parameters.Integrate
              
        let pulseStreamerParameters (parameters : PulsedOdmrExperimentParameters) = parameters.PulseStreamerParameters

        let shotsPerPoint parameters = parameters.ShotsPerPoint
        
        let laserSwitchDelay parameters = parameters.LaserSwitchDelay

        let microwaveSwitchDelay parameters = parameters.MicrowaveSwitchDelay

        let pulsedDetectionParameters parameters = parameters.PulsedDetectionParameters
        
        let histogramLength parameters = (pulsedDetectionParameters parameters).HistogramLength

        let laserDuration parameters = (pulsedDetectionParameters parameters).LaserDuration

        let acquisitionChannel parameters = (pulseStreamerParameters parameters).AcquisitionChannel

        let laserChannel parameters = (pulseStreamerParameters parameters).LaserChannel

        let microwaveChannel parameters = (pulseStreamerParameters parameters).MicrowaveChannel

        let frequencyChangeTriggerChannel parameters = (pulseStreamerParameters parameters).FrequencyChangeTriggerChannel

        let integrationWindowOffset projectionParameters = projectionParameters.IntegrationWindowOffset

        let integrationWindowWidth projectionParameters = projectionParameters.IntegrationWindowWidth

        let notes parameters = parameters.Notes

        let date parameters = parameters.Date
      
        // modify functions
        let withInitialFrequency frequency (parameters : PulsedOdmrExperimentParameters) = { parameters with InitialFrequency = frequency }

        let withFrequencyDelta frequencyDelta (parameters : PulsedOdmrExperimentParameters) = { parameters with FrequencyDelta = frequencyDelta }

        let withNumberOfPoints numberOfPoints (parameters : PulsedOdmrExperimentParameters) = { parameters with NumberOfPoints = numberOfPoints }

        let withAmplitude amplitude (parameters : PulsedOdmrExperimentParameters) = { parameters with Amplitude = amplitude }

        let withPiLength piLength parameters = { parameters with PiLength = piLength }

        let withIntegrate integrate (parameters : PulsedOdmrExperimentParameters) = { parameters with Integrate = integrate }

        let withPulseStreamerParameters pulseStreamerParameters (parameters : PulsedOdmrExperimentParameters) = { parameters with PulseStreamerParameters = pulseStreamerParameters }
    
        let withShotsPerPoints shotsPerPoint (parameters : PulsedOdmrExperimentParameters) = { parameters with ShotsPerPoint = shotsPerPoint }

        let withLaserSwitchDelay laserSwitchDelay parameters = { parameters with LaserSwitchDelay = laserSwitchDelay }

        let withMicrowaveSwitchDelay microwaveSwitchDelay parameters = { parameters with MicrowaveSwitchDelay = microwaveSwitchDelay }
              
        let withPulsedDetectionParameters detectionParameters parameters = { parameters with PulsedDetectionParameters = detectionParameters }

        let withHistogramLength detectionHistogramLength parameters = { parameters with PulsedDetectionParameters = { (pulsedDetectionParameters parameters) with HistogramLength = detectionHistogramLength } }

        let withLaserDuration laserDuration parameters = { parameters with PulsedDetectionParameters = { (pulsedDetectionParameters parameters) with LaserDuration = laserDuration } }

        let withAcquisitionChannel acquisitionChannel (parameters : PulsedOdmrExperimentParameters) = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with AcquisitionChannel = acquisitionChannel }}

        let withLaserChannel laserChannel (parameters : PulsedOdmrExperimentParameters) = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with LaserChannel = laserChannel }}

        let withMicrowaveChannel microwaveChannel (parameters : PulsedOdmrExperimentParameters) = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with MicrowaveChannel = microwaveChannel }}

        let withFrequencyChangeTriggerChannel frequencyChangeTriggerChannel (parameters : PulsedOdmrExperimentParameters) = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with FrequencyChangeTriggerChannel = frequencyChangeTriggerChannel }}

        let withIntegrationWindowOffset integrationWindowOffset projectionParameters = { projectionParameters with IntegrationWindowOffset = integrationWindowOffset } 

        let withIntegrationWidth integrationWindowWidth projectionParameters = { projectionParameters with IntegrationWindowWidth = integrationWindowWidth }

        let withNotes notes (parameters : PulsedOdmrExperimentParameters) = { parameters with Notes = notes }

        let withDate date (parameters : PulsedOdmrExperimentParameters) = { parameters with Date = date }

        // creation functions
        let createPulsedOdmrParameters initialFrequency frequencyDelta numberOfPoints amplitude integrate piLength pulseStreamerParameters shotsPerPoint laserSwitchDelay microwaveSwitchDelay detectionParameters = 
            { InitialFrequency          = initialFrequency
              FrequencyDelta            = frequencyDelta
              Amplitude                 = amplitude
              NumberOfPoints            = numberOfPoints
              PiLength                  = piLength
              Integrate                 = integrate
              PulseStreamerParameters   = pulseStreamerParameters
              ShotsPerPoint             = shotsPerPoint
              LaserSwitchDelay          = laserSwitchDelay
              MicrowaveSwitchDelay      = microwaveSwitchDelay
              PulsedDetectionParameters = detectionParameters 
              Notes                     = { SampleNotes = ""; ExperimentNotes = "" }
              Date                      = DateTime.Now }

        let createPulsedDetectionParameters histogramLength laserDuration = 
            { HistogramLength         = histogramLength
              LaserDuration           = laserDuration }

        let createProjectionParameters integrationWindowOffset integrationWindowWidth = 
            { IntegrationWindowOffset = integrationWindowOffset; IntegrationWindowWidth = integrationWindowWidth }

    [<AutoOpen>]
    module SignalProcessor =
        type ScanIndex = 
            { NumberOfPoints            : int
              PointIndex                : int
              ShotsRemaining            : int
              ScanNumber                : int }

        type Signal =
            { AccumulatedScans  : int[,]
              CurrentSnapshot   : int[,] }
    
        type SignalProcessorMessage =
            | SignalUpdate    of scanIndex : ScanIndex * histogram : Streaming.Histogram 
            | SnapshotRequest of replyChannel : AsyncReplyChannel<int[,]>
    
        type SignalProcessor = SignalProcessor of agent : Agent<SignalProcessorMessage>
    
        // this accumulator will create a snapshot at the end of every scan, on the basis that any useful scan must be at least
        // some large fraction of a second due to the frequency switch time of the microwave source (~ 1 ms). Will create a snapshot
        // at every point
        let internal accumulateHistogram (histogram : Streaming.Histogram) (scanIndex : ScanIndex) integrate (signal : Signal) : Signal =
            let accumulatedScans = signal.AccumulatedScans
    
            histogram.Histogram
            |> List.iter (fun (bin, counts) -> accumulatedScans.[bin, scanIndex.PointIndex] <- accumulatedScans.[bin, scanIndex.PointIndex] + counts) 

            if (scanIndex.ShotsRemaining = 0) && (scanIndex.PointIndex = scanIndex.NumberOfPoints - 1) then
                if integrate then
                    { AccumulatedScans = accumulatedScans; CurrentSnapshot = Array2D.copy accumulatedScans }
                else 
                    { AccumulatedScans = Array2D.zeroCreate<int> (Array2D.length1 accumulatedScans) (Array2D.length2 accumulatedScans); CurrentSnapshot = Array2D.copy accumulatedScans }
            else 
                { AccumulatedScans = accumulatedScans; CurrentSnapshot = signal.CurrentSnapshot }

        let takeSnapshot signal = signal.CurrentSnapshot
     
        /// agent which accumulates signals internally and provides copies of data for projection processing on request
        let createAgent (parameters : PulsedOdmrExperimentParameters) = 
            let accumulatedScans = Array2D.zeroCreate<int> (int (Parameters.histogramLength parameters)) (Parameters.numberOfPoints parameters)
            let currentSnapshot = Array2D.zeroCreate<int> (Array2D.length1 accumulatedScans) (Array2D.length2 accumulatedScans)

            let integrate = Parameters.integrate parameters
    
            Agent.Start(fun mailbox -> 
                let rec loop signal = async { 
                    let! message = mailbox.Receive ()
                    match message with 
                    | SignalUpdate (scanIndex, histogram) -> return! loop (accumulateHistogram histogram scanIndex integrate signal)
                    | SnapshotRequest (replyChannel)      -> replyChannel.Reply (takeSnapshot signal); return! loop signal }

                loop { AccumulatedScans = accumulatedScans; CurrentSnapshot = currentSnapshot } )

    module Signal = 
        let pointIndexAndShotsRemaining (parameters : PulsedOdmrExperimentParameters) i = 
            let pointIndex = (i / parameters.ShotsPerPoint) % (Parameters.numberOfPoints parameters)
            let shotsRemaining = parameters.ShotsPerPoint - ((i % ((Parameters.shotsPerPoint parameters) * (Parameters.numberOfPoints parameters))) - (pointIndex * (Parameters.shotsPerPoint parameters))) - 1
            let scanNumber = (i / ((Parameters.shotsPerPoint parameters) * (Parameters.numberOfPoints parameters)))
            { NumberOfPoints = (Parameters.numberOfPoints parameters); PointIndex = pointIndex; ShotsRemaining = shotsRemaining; ScanNumber = scanNumber }
                        
        let createProcessingAgent parameters = 
            SignalProcessor.createAgent parameters

        let private incomingHistogramsWithIndex (experiment : PulsedOdmrExperiment) acquisition = 
            Streaming.Acquisition.Histogram.HistogramsAvailable acquisition
            |> Observable.mapi (fun i histogram -> (pointIndexAndShotsRemaining experiment.Parameters i, histogram) )
    
        let initialiseProcessingAgent experiment acquisition (SignalProcessor agent) = 
            incomingHistogramsWithIndex experiment acquisition
            |> Observable.subscribe (SignalUpdate >> agent.Post)

        let singlePhaseCurveProjectionCalculator (experiment : PulsedOdmrExperiment) (histogramsAndProjectionParameters : Histograms * PulsedOdmrCurveProjectionParameters) = 
            let (histograms, projectionParameters) = histogramsAndProjectionParameters
            
            let numberOfPoints = Array2D.length2 histograms
            let curveProjection = Array.zeroCreate numberOfPoints

            for point in 0..(numberOfPoints - 1) do
                let intensity = histograms.[int projectionParameters.IntegrationWindowOffset..(int projectionParameters.IntegrationWindowOffset + int projectionParameters.IntegrationWindowWidth), point]
                                |> Seq.sum
                curveProjection.[point] <- intensity

            curveProjection
            |> Array.mapi (fun point intensity -> ((int64 (Parameters.initialFrequency experiment.Parameters) + (int64 point) * (int64 (Parameters.frequencyDelta experiment.Parameters))), intensity))

        let curveProjection (experiment : PulsedOdmrExperiment) (projectionCalculator : PulsedOdmrExperiment -> Histograms * PulsedOdmrCurveProjectionParameters -> Curve) (SignalProcessor agent) =
                let histogram =
                    Observable.interval (TimeSpan.FromMilliseconds 300.)
                    |> Observable.flatmapAsync (fun _ -> async { return! agent.PostAndAsyncReply(SnapshotRequest)})

                let projectionParameterObservable = experiment.ProjectionParameters.Publish
                        
                Observable.combineLatest histogram projectionParameterObservable
                |> Observable.sample (TimeSpan.FromMilliseconds 300.)
                |> Observable.iter (fun x -> experiment.DataAvailable.Trigger (HistogramsAvailable (fst x))) // trigger new histograms available
                |> Observable.map (fun x -> projectionCalculator experiment x) // transform to curve projection
                |> Observable.subscribe (fun x -> experiment.DataAvailable.Trigger (CurveAvailable x)) // trigger new curve available

    let createPulsedExperiment pulsedParameters picoHarpStreamingAcquisition pulseGeneratorAgent microwaveSourceAgent : PulsedOdmrExperiment = 
        { Parameters                   = pulsedParameters
          PicoHarpStreamingAcquisition = picoHarpStreamingAcquisition
          PulseGeneratorAgent          = pulseGeneratorAgent
          MicrowaveSourceAgent         = microwaveSourceAgent
          StopHandle                   = new ManualResetEvent(false)
          AcquisitionTokenSource       = new CancellationTokenSource()
          StatusChanged                = NotificationEvent<_>()
          DataAvailable                = Event<_>()
          ProjectionParameters         = Event<_>() }

    let pulsedOdmrSequence (parameters : PulsedOdmrExperimentParameters) = 
        let acquisitionChannel =     Parameters.acquisitionChannel parameters
        let laserChannel =           Parameters.laserChannel parameters
        let microwaveChannel =       Parameters.microwaveChannel parameters
        let frequencyChangeChannel = Parameters.frequencyChangeTriggerChannel parameters

        seq { for __ in 0..(parameters.ShotsPerPoint - 1) do
                    yield Pulse.create    [laserChannel]                         (uint32 ((Parameters.laserDuration parameters) / 2))
                    yield Pulse.empty                                            200u
                    yield Pulse.create    [microwaveChannel]                     (uint32 (Parameters.piLength parameters))
                    yield Pulse.empty                                            50u
                    yield Pulse.create    [acquisitionChannel; laserChannel]     20u
                    yield Pulse.create    [laserChannel]                         (uint32 ((Parameters.laserDuration parameters) / 2)) 
              yield Pulse.create      [frequencyChangeChannel]            30u
              yield Pulse.empty                                           1000000u } // long delay for frequency switching times
        |> List.ofSeq
        |> PulseStreamer8.Pulse.Transform.compensateHardwareDelays [parameters.PulseStreamerParameters.LaserChannel; parameters.PulseStreamerParameters.AcquisitionChannel] (uint32 parameters.LaserSwitchDelay)
        |> PulseStreamer8.Pulse.Transform.compensateHardwareDelays [parameters.PulseStreamerParameters.MicrowaveChannel] (uint32 parameters.MicrowaveSwitchDelay)

    let status experiment = 
        experiment.StatusChanged.Publish
        |> Observable.fromNotificationEvent

    let statusMessage = function
        | StartingExperiment    -> "Starting experiment"
        | PerformingExperiment  -> "Performing experiment"
        | FinishedExperiment    -> "Finished experiment"

    let setupHardware (experiment : PulsedOdmrExperiment) pulseGeneratorAgent (microwaveSourceAgent :DisposableBox<RfSource>)  = async {
        let startFreq = Parameters.initialFrequency experiment.Parameters
        let stopFreq = ((Parameters.initialFrequency experiment.Parameters) + (Parameters.frequencyDelta experiment.Parameters) * ((Parameters.numberOfPoints experiment.Parameters) |> int |> float))
        let dwellTime = (Parameters.laserDuration experiment.Parameters) * (Parameters.shotsPerPoint experiment.Parameters)

        let sweepSettings =
            Sweep.Configure.frequencyStepSweepInHz startFreq stopFreq
            |> Sweep.Configure.withPoints (Parameters.numberOfPoints experiment.Parameters)
            |> Sweep.Configure.withFixedPowerInDbm (Parameters.amplitude experiment.Parameters)
            |> Sweep.Configure.withDwellTime (Some (Duration_sec ((1.<ns> * (float dwellTime)) / Time.nanosecondsPerSecond)))   
            |> Sweep.Configure.withListTrigger (Some (External (Trigger1, Positive)))

        do! Sweep.Apply.stepSweep sweepSettings microwaveSourceAgent.Contents

        let histogramParameters = Streaming.Acquisition.Histogram.Parameters.create (Model.Duration_ns 1.<ns>) (Model.Duration_ns (1.<ns> * float (Parameters.histogramLength experiment.Parameters))) Model.TTTRConfiguration.Marker0
        return Streaming.Acquisition.Histogram.create (experiment.PicoHarpStreamingAcquisition) histogramParameters
    }

    let stop experiment =
        experiment.StopHandle.Set() |> ignore
        experiment.AcquisitionTokenSource.Cancel()

    let run (experiment : PulsedOdmrExperiment) initialProjectionParameters =
        let resultChannel = new ResultChannel<_>()

        let experimentWorkflow = async {
            experiment.StatusChanged.Trigger <| Next StartingExperiment
            
            use! microwaveSourceAgent = QueuedAccessAgent.requestControl experiment.MicrowaveSourceAgent
            use! pulseGeneratorAgent  = QueuedAccessAgent.requestControl experiment.PulseGeneratorAgent

            // switch off pulse streamer before writing new sequence
            do! PulseStreamer8.PulseStreamer.PulseSequence.setState [] pulseGeneratorAgent.Contents

            // setup time tagger acquisition and microwave source frequency / power; initialise processing pipeline
            let! histogramAcquisition = setupHardware experiment pulseGeneratorAgent microwaveSourceAgent
            use agent = Signal.createProcessingAgent experiment.Parameters
            use __ = Signal.initialiseProcessingAgent experiment histogramAcquisition (SignalProcessor agent)
            use __ = Signal.curveProjection experiment Signal.singlePhaseCurveProjectionCalculator (SignalProcessor agent)

            // trigger initial projection parameters to initialise curve 
            experiment.ProjectionParameters.Trigger initialProjectionParameters

            // start time tagger and then write pulse sequence to pulse generator
            do! Basic.setOutput On microwaveSourceAgent.Contents

            let finalState = PulseStreamer8.Pulse.empty 0u
            do! PulseStreamer8.PulseStreamer.PulseSequence.writeSequence (pulsedOdmrSequence experiment.Parameters) 0u finalState finalState Immediate pulseGeneratorAgent.Contents   

            experiment.StatusChanged.Trigger <| Next PerformingExperiment

            do! Async.AwaitWaitHandle (experiment.StopHandle) |> Async.Ignore
            do! PulseStreamer8.PulseStreamer.PulseSequence.setState [(Parameters.laserChannel experiment.Parameters)] pulseGeneratorAgent.Contents 
            do! Basic.setOutput Off microwaveSourceAgent.Contents }

        Async.StartWithContinuations (experimentWorkflow,
            (fun () -> 
                experiment.StatusChanged.Trigger (Next FinishedExperiment)
                experiment.StatusChanged.Trigger Completed
                resultChannel.RegisterResult ExperimentCompleted),
            (fun exn ->
                experiment.StatusChanged.Trigger (Error exn)
                resultChannel.RegisterResult (ExperimentError exn)),
            ignore)

        resultChannel.AwaitResult()