// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr.Odmr

open System
open System.Threading
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.Control.Reactive
open FSharp.ViewModule.Validation
open FSharp.Control.Reactive

open Endorphin.Core
open Endorphin.Instrument.PicoQuant.PicoHarp300
open Endorphin.Instrument.Keysight
open Endorphin.Instrument.SwabianInstruments
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8

module OdmrExperiment =
    [<AutoOpen>]
    module Model =        
        type PulseStreamerParameters = 
            { AcquisitionChannel            : PulseStreamer8.Model.Channel
              LaserChannel                  : PulseStreamer8.Model.Channel
              MicrowaveChannel              : PulseStreamer8.Model.Channel
              FrequencyChangeTriggerChannel : PulseStreamer8.Model.Channel }

        type Notes = { SampleNotes : string ; ExperimentNotes : string }

    module Parameters = 
        let acquisitionChannel parameters            = parameters.AcquisitionChannel
        let laserChannel parameters                  = parameters.LaserChannel
        let microwaveChannel parameters              = parameters.MicrowaveChannel
        let frequencyChangeTriggerChannel parameters = parameters.FrequencyChangeTriggerChannel

        let withAcquisitionChannel acquisitionChannel parameters                = { parameters with AcquisitionChannel = acquisitionChannel }
        let withLaserChannel laserChannel parameters                            = { parameters with LaserChannel = laserChannel }
        let withMicrowaveChannel microwaveChannel parameters                     = { parameters with MicrowaveChannel = microwaveChannel }
        let withFrequencyChangeTriggerChannel triggerChannel parameters         = { parameters with FrequencyChangeTriggerChannel = triggerChannel }

        let createPulseStreamerParameters acquisitionChannel laserChannel microwaveChannel frequencyChangeTriggerChannel = 
            { AcquisitionChannel            = acquisitionChannel
              LaserChannel                  = laserChannel
              MicrowaveChannel              = microwaveChannel
              FrequencyChangeTriggerChannel = frequencyChangeTriggerChannel }

    module Notes = 
        let sampleNotes notes = notes.SampleNotes

        let experimentNotes notes = notes.ExperimentNotes

        let withSampleNotes sampleNotes notes = { notes with SampleNotes = sampleNotes }

        let withExperimentNotes experimentNotes notes = { notes with ExperimentNotes = experimentNotes }