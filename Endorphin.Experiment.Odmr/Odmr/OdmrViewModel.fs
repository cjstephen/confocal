// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr.Odmr

open System
open System.IO
open System.Threading
open System.Windows
open System.Windows.Data
open System.Windows.Input
open System.Windows.Forms
open Microsoft.Win32
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols

open FsXaml
open OxyPlot
open Nessos.FsPickler
open Nessos.FsPickler.Json
open Endorphin.Experiment.Odmr
open Endorphin.Experiment.Odmr.OdmrExperimentBuilder
open Endorphin.Core
open Endorphin.Instrument.PicoQuant.PicoHarp300
open Endorphin.Instrument.Keysight
open Endorphin.Instrument.SwabianInstruments

open System
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.Control.Reactive
open FSharp.ViewModule
open FSharp.ViewModule.Validation
open FSharp.Control.Reactive
open PulsedOdmrExperiment
open CwOdmrExperiment
open OdmrExperiment

type OdmrExperimentState = 
    | Waiting
    | RunningPulsedExperiment of experiment : PulsedOdmrExperiment
    | RunningCwExperiment of experiment : CwOdmrExperiment
    | FinishedPulsedExperiment of parameters : PulsedOdmrExperimentParameters
    | FinishedCwExperiment of parameters : CwOdmrExperimentParameters

type OdmrExperimentType = 
    | Pulsed
    | Cw

type OdmrViewModel(picoHarpStreamingAcquisition, microwaveSourceAgent, pulseGeneratorAgent) as self = 
    inherit ViewModelBase()

    let defaultStartFrequency = 2.8025e9<Hz>
    let defaultFrequencyStep = 0.7e6<Hz>
    let defaultNumberOfPoints = 200

    let defaultPulseStreamerParameters = Parameters.createPulseStreamerParameters
                                         <| PulseStreamer8.Model.Channel0
                                         <| PulseStreamer8.Model.Channel1
                                         <| PulseStreamer8.Model.Channel2
                                         <| PulseStreamer8.Model.Channel4

    let defaultPulsedDetectionParameters = Parameters.createPulsedDetectionParameters 3000<ns> 4000<ns>
    
    let defaultPulsedParameters = Parameters.createPulsedOdmrParameters
                                  <| defaultStartFrequency
                                  <| defaultFrequencyStep
                                  <| defaultNumberOfPoints
                                  <| 0.<dBm>
                                  <| true
                                  <| 60<ns>
                                  <| defaultPulseStreamerParameters
                                  <| 100
                                  <| 1300<ns>
                                  <| 410<ns>
                                  <| defaultPulsedDetectionParameters
     
    let defaultPulsedProjectionParameters = Parameters.createProjectionParameters 1280<ns> 400<ns>

    let defaultCwDetectionParameters = Parameters.createCwDetectionParameters 50000000<ns>

    let defaultCwParameters = Parameters.createCwOdmrParameters
                              <| defaultStartFrequency
                              <| defaultFrequencyStep
                              <| defaultNumberOfPoints
                              <| -3.<dBm>
                              <| true
                              <| defaultPulseStreamerParameters
                              <| defaultCwDetectionParameters

    let histogramSeries = new Series.HeatMapSeries()
    do histogramSeries.Interpolate <- true
    do histogramSeries.Data <- Array2D.zeroCreate 1 1

    let histogramSeriesDefaultPlotModel = 
        let model = new PlotModel()
        model.Series.Add histogramSeries
        
        let axes = new OxyPlot.Axes.LinearColorAxis()
        axes.IsAxisVisible <- false
        model.Axes.Add axes

        let bottomAxis = new OxyPlot.Axes.LinearAxis()
        bottomAxis.IsAxisVisible <- true
        bottomAxis.Position <- OxyPlot.Axes.AxisPosition.Bottom
        bottomAxis.Title <- "Time (ns)"
        model.Axes.Add bottomAxis

        let leftAxis = new OxyPlot.Axes.LinearAxis()
        leftAxis.IsAxisVisible <- true
        leftAxis.Position <- OxyPlot.Axes.AxisPosition.Left
        leftAxis.Title <- "Frequency (GHz)"
        model.Axes.Add leftAxis

        model

    let projectedCurveSeries = new Series.LineSeries()
    let projectedCurveDefaultPlotModel = 
        let model = new PlotModel()
        model.Series.Add projectedCurveSeries

        let bottomAxis = new OxyPlot.Axes.LinearAxis()
        bottomAxis.IsAxisVisible <- true
        bottomAxis.Position <- OxyPlot.Axes.AxisPosition.Bottom
        bottomAxis.Title <- "Frequency (GHz)"
        model.Axes.Add bottomAxis

        let leftAxis = new OxyPlot.Axes.LinearAxis()
        leftAxis.IsAxisVisible <- true
        leftAxis.Position <- OxyPlot.Axes.AxisPosition.Left
        leftAxis.Title <- "Intensity (counts)"
        model.Axes.Add leftAxis
        
        model

    // variables common to both pulsed and cw are implemented as local variables,
    // and are used to modify the default parameters at experiment start time
    let startFrequency              = self.Factory.Backing(<@ self.StartFrequency @>, defaultStartFrequency)
    let frequencyStep               = self.Factory.Backing(<@ self.FrequencyStep @>, defaultFrequencyStep)
    let numberOfPoints              = self.Factory.Backing(<@ self.NumberOfPoints @>, defaultNumberOfPoints)
    let microwaveAmplitude          = self.Factory.Backing(<@ self.MicrowaveAmplitude @>, -10.<dBm>)
    let notes                       = self.Factory.Backing(<@ self.Notes @>, { ExperimentNotes = "" ; SampleNotes = "" }) 
    let experimentType              = self.Factory.Backing(<@ self.IsPulsed @>, Cw)
    let pulseStreamerSettings       = self.Factory.Backing(<@ self.PulseStreamerSettings @>, defaultPulseStreamerParameters)
    let integrate                   = self.Factory.Backing(<@ self.Integrate @>, true)
    
    let pulsedExperimentParameters  = self.Factory.Backing(<@ self.PulsedExperimentParameters @>, defaultPulsedParameters)
    let pulsedProjectionParameters  = self.Factory.Backing(<@ self.ProjectionParameters @>, defaultPulsedProjectionParameters)

    let cwExperimentParameters      = self.Factory.Backing(<@ self.CwExperimentParameters @>, defaultCwParameters)

    let experimentState             = self.Factory.Backing(<@ self.ExperimentState @>, Waiting)
    let histogramPlotModel          = self.Factory.Backing(<@ self.HistogramPlotModel @>, histogramSeriesDefaultPlotModel)
    let projectedCurvePlotModel     = self.Factory.Backing(<@ self.ProjectedCurvePlotModel @>, projectedCurveDefaultPlotModel)
    let statusMessage               = self.Factory.Backing(<@ self.StatusMessage @>, "")

    let serialiser = FsPickler.CreateJsonSerializer (indent = true)

    /// save the recorded data
    let save () =
        match self.ExperimentState with
        | FinishedPulsedExperiment (parameters) ->
            let saveFile = new SaveFileDialog (Filter="Pulsed ODMR experiment|*.podmr")
            let result = saveFile.ShowDialog()
            if result.HasValue && result.Value then
                use paramsFile = new StreamWriter (saveFile.FileName)
                parameters |> PulsedOdmrExperiment.Parameters.withNotes self.Notes |> serialiser.PickleToString |> paramsFile.Write
        
                use histogramDataFile = new StreamWriter(Path.Combine ((Path.GetDirectoryName saveFile.FileName), ((Path.GetFileNameWithoutExtension saveFile.FileName) + "_histograms.csv")))
                for i in 0..(histogramSeries.Data.GetLength(0) - 1) do
                    for j in 0..(histogramSeries.Data.GetLength(1) - 1) do
                        histogramSeries.Data.[i,j] |> histogramDataFile.Write
                        "," |> histogramDataFile.Write
                    histogramDataFile.WriteLine ()
        
                use projectedCurveDataFile = new StreamWriter(Path.Combine ((Path.GetDirectoryName saveFile.FileName), ((Path.GetFileNameWithoutExtension saveFile.FileName) + "_projectedCurve.csv")))
                Seq.iter (fun (point : DataPoint) ->
                    point.X |> projectedCurveDataFile.Write
                    "," |> projectedCurveDataFile.Write
                    point.Y |> projectedCurveDataFile.WriteLine) projectedCurveSeries.Points
        | FinishedCwExperiment (parameters) -> 
            let saveFile = new SaveFileDialog (Filter="CW ODMR experiment|*.odmr")
            let result = saveFile.ShowDialog()
            if result.HasValue && result.Value then
                use paramsFile = new StreamWriter (saveFile.FileName)
                parameters |> CwOdmrExperiment.Parameters.withNotes self.Notes |> serialiser.PickleToString |> paramsFile.Write
                
                use projectedCurveDataFile = new StreamWriter(Path.Combine ((Path.GetDirectoryName saveFile.FileName), ((Path.GetFileNameWithoutExtension saveFile.FileName) + "_projectedCurve.csv")))
                Seq.iter (fun (point : DataPoint) ->
                    point.X |> projectedCurveDataFile.Write
                    "," |> projectedCurveDataFile.Write
                    point.Y |> projectedCurveDataFile.WriteLine) projectedCurveSeries.Points
        | _ -> "No data to save!" |> MessageBox.Show |> ignore

    let performPulsedExperiment ui = async { 
        let cts = new CancellationTokenSource()

        let parameters = self.PulsedExperimentParameters 
                         |> PulsedOdmrExperiment.Parameters.withInitialFrequency startFrequency.Value
                         |> PulsedOdmrExperiment.Parameters.withFrequencyDelta frequencyStep.Value
                         |> PulsedOdmrExperiment.Parameters.withAmplitude microwaveAmplitude.Value
                         |> PulsedOdmrExperiment.Parameters.withNumberOfPoints numberOfPoints.Value
                         |> PulsedOdmrExperiment.Parameters.withIntegrate integrate.Value
                         |> PulsedOdmrExperiment.Parameters.withPulseStreamerParameters pulseStreamerSettings.Value
                         |> PulsedOdmrExperiment.Parameters.withDate DateTime.Now 
        let experiment = PulsedOdmrExperiment.createPulsedExperiment parameters picoHarpStreamingAcquisition pulseGeneratorAgent microwaveSourceAgent
        
        use __ = 
            PulsedOdmrExperiment.status experiment
            |> Observable.observeOnContext ui
            |> Observable.subscribeWithError
                (fun status -> self.StatusMessage <- PulsedOdmrExperiment.statusMessage status)
                (fun exn    -> self.StatusMessage <- sprintf "Experiment failed: %s" exn.Message)

        use __ = 
            experiment.DataAvailable.Publish
            |> Observable.observeOnContext ui
            |> Observable.subscribe (function
                | PulsedOdmrExperiment.Model.PulsedOdmrSignalUpdate.CurveAvailable (curve) -> 
                    self.ProjectedCurveData.Clear()
                    curve |> Array.map (fun (x, y) -> DataPoint((float x)/1.e9, float y)) |> self.ProjectedCurveData.AddRange
                    self.ProjectedCurvePlotModel.InvalidatePlot true
                | HistogramsAvailable (histograms) ->
                    histogramSeries.Data <- Array2D.map float histograms
                    self.HistogramPlotModel.InvalidatePlot true)

        histogramSeries.Data <- Array2D.zeroCreate 1 1
        histogramSeries.X0 <- float 0
        histogramSeries.X1 <- float <| Parameters.histogramLength parameters
        histogramSeries.Y0 <- float <| PulsedOdmrExperiment.Parameters.initialFrequency parameters
        histogramSeries.Y1 <- (float (PulsedOdmrExperiment.Parameters.initialFrequency parameters) + (float (PulsedOdmrExperiment.Parameters.numberOfPoints parameters)) * (float (PulsedOdmrExperiment.Parameters.frequencyDelta parameters)))

        self.ExperimentState <- RunningPulsedExperiment(experiment)
        do! Async.SwitchToThreadPool()
 
        let! experimentResult = PulsedOdmrExperiment.run 
                                <| experiment 
                                <| self.ProjectionParameters

        do! Async.SwitchToContext ui
        self.ExperimentState <- FinishedPulsedExperiment(parameters)
    }

    let performCwExperiment ui = async {
        let cts = new CancellationTokenSource()

        let parameters = self.CwExperimentParameters 
                         |> CwOdmrExperiment.Parameters.withInitialFrequency startFrequency.Value
                         |> CwOdmrExperiment.Parameters.withFrequencyDelta frequencyStep.Value
                         |> CwOdmrExperiment.Parameters.withAmplitude microwaveAmplitude.Value
                         |> CwOdmrExperiment.Parameters.withNumberOfPoints numberOfPoints.Value
                         |> CwOdmrExperiment.Parameters.withIntegrate integrate.Value
                         |> CwOdmrExperiment.Parameters.withPulseStreamerParameters pulseStreamerSettings.Value
                         |> CwOdmrExperiment.Parameters.withDate DateTime.Now 
        let experiment = CwOdmrExperiment.createCwExperiment parameters picoHarpStreamingAcquisition pulseGeneratorAgent microwaveSourceAgent
        
        use __ = 
            CwOdmrExperiment.status experiment
            |> Observable.observeOnContext ui
            |> Observable.subscribeWithError
                (fun status -> self.StatusMessage <- CwOdmrExperiment.statusMessage status)
                (fun exn    -> self.StatusMessage <- sprintf "Experiment failed: %s" exn.Message)

        use __ = 
            experiment.DataAvailable.Publish
            |> Observable.observeOnContext ui
            |> Observable.subscribe (function
                | CwOdmrExperiment.Model.CwOdmrSignalUpdate.CurveAvailable (curve) -> 
                    self.ProjectedCurveData.Clear()
                    curve |> Array.map (fun (x, y) -> DataPoint((float x)/1.e9, float y)) |> self.ProjectedCurveData.AddRange
                    self.ProjectedCurvePlotModel.InvalidatePlot true )

        self.ExperimentState <- RunningCwExperiment(experiment)
        do! Async.SwitchToThreadPool()
 
        let! experimentResult = CwOdmrExperiment.run 
                                <| experiment 

        do! Async.SwitchToContext ui
        self.ExperimentState <- FinishedCwExperiment(parameters)
    }

    let performExperiment ui = async {
        do! Async.SwitchToContext ui
        self.ProjectedCurveData.Clear()

        match experimentType.Value with
        | Cw            -> do! performCwExperiment ui
        | Pulsed        -> do! performPulsedExperiment ui
    }

    // commands
    let startExperimentCommand = self.Factory.CommandAsync performExperiment

    let stopExperimentCommand = 
        self.Factory.CommandSyncParamChecked(
            (function 
                | RunningPulsedExperiment (experiment) -> PulsedOdmrExperiment.stop experiment
                | RunningCwExperiment (experiment) -> CwOdmrExperiment.stop experiment
                | _ -> ()), 
            (fun _ -> self.IsPerformingExperiment), 
            [ <@ self.IsPerformingExperiment @> ])

    let saveCommand =   
        self.Factory.CommandSyncChecked(
            save,
            (fun () -> match self.ExperimentState with 
                       | FinishedPulsedExperiment _ -> true 
                       | FinishedCwExperiment _ -> true
                       | _ -> false),
            [ <@ self.ExperimentState @> ])

    do
        self.DependencyTracker.AddPropertyDependency(<@ self.AcquisitionChannel @>,                 <@ self.PulseStreamerChannels @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.LaserChannel @>,                       <@ self.PulseStreamerChannels @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.MicrowaveChannel @>,                   <@ self.PulseStreamerChannels @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.FrequencyChangeTriggerChannel @>,      <@ self.PulseStreamerChannels @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.StopFrequency@>,                       <@ self.StartFrequency @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.StopFrequency @>,                      <@ self.FrequencyStep @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.StopFrequency @>,                      <@ self.NumberOfPoints @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IntegrationLength @>,                  <@ self.CwExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.LaserSwitchDelay @>,                   <@ self.PulsedExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.MicrowaveSwitchDelay @>,               <@ self.PulsedExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.PiLength @>,                           <@ self.PulsedExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.ShotsPerPoint @>,                      <@ self.PulsedExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IntegrationWindowOffset @>,            <@ self.ProjectionParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IntegrationWindowWidth @>,             <@ self.ProjectionParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.SampleNotes @>,                        <@ self.Notes @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.ExperimentNotes @>,                    <@ self.Notes @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IsPerformingExperiment @>,             <@ self.ExperimentState @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IsReadyToStart @>,                     <@ self.IsPerformingExperiment @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.ProjectionControlsEnabled @>,          <@ self.IsPulsed @>)
        self.DependencyTracker.AddPropertyDependencies(<@@ self.CwControlsEnabled @@>, [ <@@ self.IsReadyToStart @@>; <@@ self.IsPulsed @@> ])
        self.DependencyTracker.AddPropertyDependencies(<@@ self.PulsedControlsEnabled @@>, [ <@@ self.IsReadyToStart @@>; <@@ self.IsPulsed @@> ])
    member x.HistogramPlotModel : PlotModel = histogramPlotModel.Value
    member x.HistogramData : Double[,] = histogramSeries.Data
    member x.ProjectedCurvePlotModel : PlotModel = projectedCurvePlotModel.Value
    member x.ProjectedCurveData : ResizeArray<DataPoint> = projectedCurveSeries.Points
 
    member x.IsPerformingExperiment = x.ExperimentState |> (function 
                                                            | RunningPulsedExperiment _ -> true
                                                            | RunningCwExperiment _ -> true 
                                                            | _ -> false)
    member x.IsReadyToStart = not x.IsPerformingExperiment

    member x.PulsedExperimentParameters
        with get()     = pulsedExperimentParameters.Value
        and  set value = pulsedExperimentParameters.Value <- value

    member x.ProjectionParameters
        with get()     = pulsedProjectionParameters.Value
        and  set value = pulsedProjectionParameters.Value <- value

    member x.CwExperimentParameters
        with get()      = cwExperimentParameters.Value
        and  set value  = cwExperimentParameters.Value <- value

    member x.PulseStreamerSettings
        with get()      = pulseStreamerSettings.Value
        and  set value  = pulseStreamerSettings.Value <- value 
    
    member x.ExperimentState
        with get()     = experimentState.Value
        and  set value = experimentState.Value <- value

    member x.StatusMessage
        with get()     = statusMessage.Value
        and  set value = statusMessage.Value <- value

    member x.Integrate
        with get()     = integrate.Value
        and  set value = integrate.Value <- value

    member x.StartFrequency
        with get()                  = (decimal (startFrequency.Value) / 1E9m)
        and  set (value : decimal)  = startFrequency.Value <- ((float (value * 1E9m)) * 1.<Hz>)

    member x.FrequencyStep
        with get()                  = (decimal (frequencyStep.Value) / 1E6m)
        and  set (value : decimal)  = frequencyStep.Value <- ((float (value * 1E6m)) * 1.<Hz>)

    member x.StopFrequency
        with get()                  = ((decimal (startFrequency.Value)) + ((decimal self.NumberOfPoints) * (decimal (frequencyStep.Value)))) / 1E9m
        and  set (value : decimal)  = 
           self.NumberOfPoints <- int((1E9m * value - (decimal (startFrequency.Value))) / (decimal (frequencyStep.Value)))

    member x.NumberOfPoints 
        with get()                  = numberOfPoints.Value
        and  set value              = numberOfPoints.Value <- value

    member x.MicrowaveAmplitude
        with get()                  = decimal (microwaveAmplitude.Value)
        and  set (value : decimal)  = microwaveAmplitude.Value <- ((float value) * 1.<dBm>)

    member x.IntegrationLength
        with get()                  = (int (Parameters.integrationLength x.CwExperimentParameters) / 1000000)
        and  set value              = x.CwExperimentParameters <- Parameters.withIntegrationLength (1<ns> * (value * 1000000)) x.CwExperimentParameters       

    member x.PiLength
        with get()     = int (Parameters.piLength x.PulsedExperimentParameters)
        and  set value = x.PulsedExperimentParameters <- Parameters.withPiLength (1<ns> * value) x.PulsedExperimentParameters

    member x.LaserDuration
        with get()     = Parameters.laserDuration x.PulsedExperimentParameters
        and  set value = x.PulsedExperimentParameters <- Parameters.withLaserDuration value x.PulsedExperimentParameters

    member x.ShotsPerPoint
        with get()     = Parameters.shotsPerPoint x.PulsedExperimentParameters
        and  set value = x.PulsedExperimentParameters <- Parameters.withShotsPerPoints value x.PulsedExperimentParameters

    member x.IntegrationWindowOffset
        with get()     = int (Parameters.integrationWindowOffset x.ProjectionParameters)
        and  set value = 
            x.ProjectionParameters <- Parameters.withIntegrationWindowOffset (value * 1<ns>) x.ProjectionParameters
            match x.ExperimentState with 
            | RunningPulsedExperiment (experiment) -> experiment.ProjectionParameters.Trigger x.ProjectionParameters
            | _                                    -> () 

    member x.IntegrationWindowWidth
        with get()     = int (Parameters.integrationWindowWidth x.ProjectionParameters)
        and  set value =
            x.ProjectionParameters <- Parameters.withIntegrationWidth (value * 1<ns>) x.ProjectionParameters
            match x.ExperimentState with
            | RunningPulsedExperiment (experiment) -> experiment.ProjectionParameters.Trigger x.ProjectionParameters
            | _                                    -> ()

    member x.LaserSwitchDelay 
        with get()     = int (Parameters.laserSwitchDelay x.PulsedExperimentParameters)
        and  set value = x.PulsedExperimentParameters <- Parameters.withLaserSwitchDelay (value * 1<ns>) x.PulsedExperimentParameters

    member x.MicrowaveSwitchDelay
        with get()     = int (Parameters.microwaveSwitchDelay x.PulsedExperimentParameters)
        and  set value = x.PulsedExperimentParameters <- Parameters.withMicrowaveSwitchDelay (value * 1<ns>) x.PulsedExperimentParameters

    member x.AcquisitionChannel
        with get()     = DuHelper.findDuStringIndexFromValue (Parameters.acquisitionChannel x.PulseStreamerSettings)
        and  set value = 
            let duValue = DuHelper.findDuValueFromStringIndex<PulseStreamer8.Model.Channel> value
            x.PulseStreamerSettings <- Parameters.withAcquisitionChannel duValue x.PulseStreamerSettings
        
    member x.LaserChannel
        with get()     =  DuHelper.findDuStringIndexFromValue (Parameters.laserChannel x.PulseStreamerSettings)
        and  set value = 
            let duValue = DuHelper.findDuValueFromStringIndex<PulseStreamer8.Model.Channel> value
            x.PulseStreamerSettings <- Parameters.withLaserChannel duValue x.PulseStreamerSettings

    member x.MicrowaveChannel
        with get()     = DuHelper.findDuStringIndexFromValue (Parameters.microwaveChannel x.PulseStreamerSettings)
        and  set value = 
            let duValue = DuHelper.findDuValueFromStringIndex<PulseStreamer8.Model.Channel> value
            x.PulseStreamerSettings <- Parameters.withMicrowaveChannel duValue x.PulseStreamerSettings

    member x.FrequencyChangeTriggerChannel
        with get()     = DuHelper.findDuStringIndexFromValue (Parameters.frequencyChangeTriggerChannel x.PulseStreamerSettings)
        and  set value = 
            let duValue = DuHelper.findDuValueFromStringIndex<PulseStreamer8.Model.Channel> value
            x.PulseStreamerSettings <- Parameters.withFrequencyChangeTriggerChannel duValue x.PulseStreamerSettings

    member x.PulseStreamerChannels = 
        Microsoft.FSharp.Reflection.FSharpType.GetUnionCases typeof<PulseStreamer8.Model.Channel>
        |> Array.map(fun e -> e.Name)           

    member x.Notes
        with get()     = notes.Value
        and  set value = notes.Value <- value

    member x.SampleNotes
        with get()     = Notes.sampleNotes x.Notes
        and  set value = x.Notes <- x.Notes |> Notes.withSampleNotes value

    member x.ExperimentNotes
        with get()     = Notes.experimentNotes x.Notes
        and  set value = x.Notes <- x.Notes |> Notes.withExperimentNotes value

    member x.IsPulsed 
        with get()      = match experimentType.Value with
                          | Pulsed       -> true
                          | Cw           -> false
        and  set value  = if value then
                              experimentType.Value <- Pulsed
                          else
                              experimentType.Value <- Cw

    member x.PulsedControlsEnabled
        with get()     = x.IsPulsed && x.IsReadyToStart

    member x.CwControlsEnabled
        with get()     = (not x.IsPulsed) && x.IsReadyToStart

    member x.ProjectionControlsEnabled
        with get()     = x.IsPulsed

    member x.StartExperiment = startExperimentCommand
    member x.StopExperiment = stopExperimentCommand
    member x.Save = saveCommand