// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr.Odmr

open System
open System.Windows.Controls
open System.Windows.Forms.Integration
open Endorphin.Core
open Endorphin.Experiment.Odmr.Odmr

module Ui =
    let createOdmrExperimentWindow width height picoHarpStreamingAcquisition microwaveSourceAgent pulseGeneratorAgent =   
        let odmrWindow = System.Windows.Window()
        odmrWindow.Title    <- "ODMR"
        odmrWindow.Width    <- width
        odmrWindow.Height   <- height

        let odmrView = Endorphin.Experiment.Odmr.Odmr.View.OdmrMainView()
        odmrView.DataContext <- Endorphin.Experiment.Odmr.Odmr.OdmrViewModel(picoHarpStreamingAcquisition, microwaveSourceAgent, pulseGeneratorAgent)
        odmrWindow.Content <- odmrView
        ElementHost.EnableModelessKeyboardInterop(odmrWindow)
        odmrWindow