// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr.Odmr

open System
open System.Threading
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.Control.Reactive
open FSharp.ViewModule.Validation
open FSharp.Control.Reactive

open Endorphin.Core
open Endorphin.Abstract
open Endorphin.Instrument.PicoQuant.PicoHarp300
open Endorphin.Instrument.Keysight.N5172B
open Endorphin.Instrument.SwabianInstruments
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open OdmrExperiment

module CwOdmrExperiment =
    [<AutoOpen>]
    module Model =
        type CwOdmrDetectionParameters =
            { IntegrationLength          : int<ns> }

        type CwOdmrExperimentParameters =
            { InitialFrequency           : float<Hz>
              FrequencyDelta             : float<Hz>
              NumberOfPoints             : int
              Amplitude                  : float<dBm>
              Integrate                  : bool
              PulseStreamerParameters    : PulseStreamerParameters
              CwDetectionParameters      : CwOdmrDetectionParameters
              Notes                      : Notes
              Date                       : DateTime }

        type Curve = (int64 * int)[]

        type CwOdmrSignalUpdate =
            | CurveAvailable of odmrCurve : Curve

        type CwOdmrExperimentStatus =
            | StartingExperiment
            | PerformingExperiment
            | FinishedExperiment

        type CwOdmrExperimentResult =
            | ExperimentCompleted
            | ExperimentError of exn

        type CwOdmrExperiment =
            { Parameters                   : CwOdmrExperimentParameters
              PicoHarpStreamingAcquisition : Streaming.StreamingAcquisition
              PulseGeneratorAgent          : QueuedAccessAgent<PulseStreamer8.Model.PulseStreamer8>
              MicrowaveSourceAgent         : QueuedAccessAgent<Endorphin.Instrument.Keysight.N5172B.Model.RfSource>
              StopHandle                   : ManualResetEvent
              AcquisitionTokenSource       : CancellationTokenSource
              StatusChanged                : NotificationEvent<CwOdmrExperimentStatus>
              DataAvailable                : Event<CwOdmrSignalUpdate> }

    module Parameters =
        // query functions
        let initialFrequency (parameters : CwOdmrExperimentParameters) = parameters.InitialFrequency

        let frequencyDelta (parameters : CwOdmrExperimentParameters) = parameters.FrequencyDelta

        let numberOfPoints (parameters : CwOdmrExperimentParameters) = parameters.NumberOfPoints

        let amplitude (parameters : CwOdmrExperimentParameters) = parameters.Amplitude

        let integrate (parameters : CwOdmrExperimentParameters) = parameters.Integrate

        let cwDetectionParameters parameters = parameters.CwDetectionParameters

        let integrationLength parameters = (cwDetectionParameters parameters).IntegrationLength

        let pulseStreamerParameters (parameters : CwOdmrExperimentParameters) = parameters.PulseStreamerParameters

        let acquisitionChannel parameters = (pulseStreamerParameters parameters).AcquisitionChannel

        let laserChannel parameters = (pulseStreamerParameters parameters).LaserChannel

        let microwaveChannel parameters = (pulseStreamerParameters parameters).MicrowaveChannel

        let frequencyChangeTriggerChannel parameters = (pulseStreamerParameters parameters).FrequencyChangeTriggerChannel

        let notes parameters = parameters.Notes

        let date parameters = parameters.Date

        // modify functions
        let withInitialFrequency frequency (parameters : CwOdmrExperimentParameters) = { parameters with InitialFrequency = frequency }

        let withFrequencyDelta frequencyDelta (parameters : CwOdmrExperimentParameters) = { parameters with FrequencyDelta = frequencyDelta }

        let withNumberOfPoints numberOfPoints (parameters : CwOdmrExperimentParameters) = { parameters with NumberOfPoints = numberOfPoints }

        let withAmplitude amplitude (parameters : CwOdmrExperimentParameters) = { parameters with Amplitude = amplitude }

        let withIntegrate integrate (parameters : CwOdmrExperimentParameters) = { parameters with Integrate = integrate }

        let withPulseStreamerParameters pulseStreamerParameters (parameters : CwOdmrExperimentParameters) = { parameters with PulseStreamerParameters = pulseStreamerParameters }

        let withCwDetectionParameters detectionParameters parameters = { parameters with CwDetectionParameters = detectionParameters }

        let withIntegrationLength integrationLength parameters = { parameters with CwDetectionParameters = { (cwDetectionParameters parameters) with IntegrationLength = integrationLength } }

        let withAcquisitionChannel acquisitionChannel (parameters : CwOdmrExperimentParameters) = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with AcquisitionChannel = acquisitionChannel }}

        let withLaserChannel laserChannel (parameters : CwOdmrExperimentParameters) = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with LaserChannel = laserChannel }}

        let withMicrowaveChannel microwaveChannel (parameters : CwOdmrExperimentParameters) = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with MicrowaveChannel = microwaveChannel }}

        let withFrequencyChangeTriggerChannel frequencyChangeTriggerChannel (parameters : CwOdmrExperimentParameters) = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with FrequencyChangeTriggerChannel = frequencyChangeTriggerChannel }}

        let withNotes notes (parameters : CwOdmrExperimentParameters) = { parameters with Notes = notes }

        let withDate date (parameters : CwOdmrExperimentParameters) = { parameters with Date = date }

        // creation functions
        let createCwOdmrParameters initialFrequency frequencyDelta numberOfPoints amplitude integrate pulseStreamerParameters detectionParameters =
            { InitialFrequency          = initialFrequency
              FrequencyDelta            = frequencyDelta
              Amplitude                 = amplitude
              NumberOfPoints            = numberOfPoints
              Integrate                 = integrate
              PulseStreamerParameters   = pulseStreamerParameters
              CwDetectionParameters     = detectionParameters
              Notes                     = { SampleNotes = ""; ExperimentNotes = "" }
              Date                      = DateTime.Now }

        let createCwDetectionParameters integrationLength =
            { IntegrationLength         = integrationLength }

    [<AutoOpen>]
    module SignalProcessor =
        type ScanIndex =
            { NumberOfPoints            : int
              PointIndex                : int
              ScanNumber                : int }

        type Signal =
            { AccumulatedScans  : int[]
              CurrentSnapshot   : int[] }

        type SignalProcessorMessage =
            | SignalUpdate    of scanIndex : ScanIndex * histogram : Streaming.Histogram
            | SnapshotRequest of replyChannel : AsyncReplyChannel<int[]>

        type SignalProcessor = SignalProcessor of agent : Agent<SignalProcessorMessage>

        // this accumulator will create a snapshot at the end of every scan, on the basis that any useful scan must be at least
        // some large fraction of a second due to the frequency switch time of the microwave source (~ 1 ms). Will create a snapshop
        // at every point
        let internal accumulateHistogram (histogram : Streaming.Histogram) (scanIndex : ScanIndex) integrate (signal : Signal) : Signal =
            let accumulatedScans = signal.AccumulatedScans

            histogram.Histogram
            |> List.iter (fun (bin, counts) ->
                if integrate then
                    accumulatedScans.[scanIndex.PointIndex] <- accumulatedScans.[scanIndex.PointIndex] + counts
                else
                    accumulatedScans.[scanIndex.PointIndex] <- counts)

            { AccumulatedScans = accumulatedScans; CurrentSnapshot = Array.copy accumulatedScans }

        let takeSnapshot signal = signal.CurrentSnapshot

        /// agent which accumulates signals internally and provides copies of data for projection processing on request
        let createAgent (parameters : CwOdmrExperimentParameters) =
            let accumulatedScans = Array.zeroCreate<int> (Parameters.numberOfPoints parameters)
            let currentSnapshot = Array.zeroCreate<int> (Array.length accumulatedScans)

            let integrate = Parameters.integrate parameters

            Agent.Start(fun mailbox ->
                let rec loop signal = async {
                    let! message = mailbox.Receive ()
                    match message with
                    | SignalUpdate (scanIndex, histogram) -> return! loop (accumulateHistogram histogram scanIndex integrate signal)
                    | SnapshotRequest (replyChannel)      -> replyChannel.Reply (takeSnapshot signal); return! loop signal }

                loop { AccumulatedScans = accumulatedScans; CurrentSnapshot = currentSnapshot } )

    module Signal =
        let pointIndex (parameters : CwOdmrExperimentParameters) i =
            let pointIndex = i % (Parameters.numberOfPoints parameters)
            let scanNumber = i / (Parameters.numberOfPoints parameters)
            { NumberOfPoints = (Parameters.numberOfPoints parameters); PointIndex = pointIndex; ScanNumber = scanNumber }

        let createProcessingAgent parameters =
            SignalProcessor.createAgent parameters

        let private incomingHistogramsWithIndex (experiment : CwOdmrExperiment) acquisition =
            Streaming.Acquisition.Histogram.HistogramsAvailable acquisition
            |> Observable.mapi (fun i histogram -> (pointIndex experiment.Parameters i, histogram) )

        let initialiseProcessingAgent experiment acquisition (SignalProcessor agent) =
            incomingHistogramsWithIndex experiment acquisition
            |> Observable.subscribe (SignalUpdate >> agent.Post)

        let singlePointProjectionCalculator (experiment : CwOdmrExperiment) (pointData : int[]) : Curve =
            pointData
            |> Array.mapi (fun point intensity -> ((int64 (Parameters.initialFrequency experiment.Parameters) + (int64 point) * (int64 (Parameters.frequencyDelta experiment.Parameters))), intensity))

        let curveProjection (experiment : CwOdmrExperiment) (projectionCalculator : CwOdmrExperiment -> int[] -> Curve) (SignalProcessor agent) =
                let pointData =
                    Observable.interval (TimeSpan.FromMilliseconds 300.)
                    |> Observable.flatmapAsync (fun _ -> async { return! agent.PostAndAsyncReply(SnapshotRequest)})

                pointData
                |> Observable.map (fun x -> projectionCalculator experiment x)
                |> Observable.subscribe (fun x -> experiment.DataAvailable.Trigger (CurveAvailable x))

    let createCwExperiment cwParameters picoHarpStreamingAcquisition pulseGeneratorAgent microwaveSourceAgent : CwOdmrExperiment =
        { Parameters                   = cwParameters
          PicoHarpStreamingAcquisition = picoHarpStreamingAcquisition
          PulseGeneratorAgent          = pulseGeneratorAgent
          MicrowaveSourceAgent         = microwaveSourceAgent
          StopHandle                   = new ManualResetEvent(false)
          AcquisitionTokenSource       = new CancellationTokenSource()
          StatusChanged                = NotificationEvent<_>()
          DataAvailable                = Event<_>()}

    let cwOdmrSequence (parameters : CwOdmrExperimentParameters) =
        let acquisitionChannel =     Parameters.acquisitionChannel parameters
        let laserChannel =           Parameters.laserChannel parameters
        let microwaveChannel =       Parameters.microwaveChannel parameters
        let frequencyChangeChannel = Parameters.frequencyChangeTriggerChannel parameters

        let integrationLength = Parameters.integrationLength parameters

        seq {   yield Pulse.create      [acquisitionChannel; laserChannel; microwaveChannel]    20u
                yield Pulse.create      [laserChannel; microwaveChannel]                        (uint32 integrationLength)
                yield Pulse.create      [frequencyChangeChannel; laserChannel]                  30u
                yield Pulse.create      [laserChannel]                                          800000u } // long delay for frequency switching times
        |> List.ofSeq

    let status experiment =
        experiment.StatusChanged.Publish
        |> Observable.fromNotificationEvent

    let statusMessage = function
        | StartingExperiment    -> "Starting experiment"
        | PerformingExperiment  -> "Performing experiment"
        | FinishedExperiment    -> "Finished experiment"

    let setupHardware (experiment : CwOdmrExperiment) pulseGeneratorAgent (microwaveSourceAgent :DisposableBox<RfSource>)  = async {
        let startFreq = Parameters.initialFrequency experiment.Parameters
        let stopFreq = ((Parameters.initialFrequency experiment.Parameters) + (Parameters.frequencyDelta experiment.Parameters) * ((Parameters.numberOfPoints experiment.Parameters) |> int |> float))
        let dwellTime = (Parameters.integrationLength experiment.Parameters)

        let sweepSettings =
            Sweep.Configure.frequencyStepSweepInHz startFreq stopFreq
            |> Sweep.Configure.withPoints (Parameters.numberOfPoints experiment.Parameters)
            |> Sweep.Configure.withFixedPowerInDbm (Parameters.amplitude experiment.Parameters)
            |> Sweep.Configure.withDwellTime (Some (Duration_sec ((1.<ns> * (float dwellTime)) / Time.nanosecondsPerSecond)))
            |> Sweep.Configure.withListTrigger (Some (External (Trigger1, Positive)))

        do! Sweep.Apply.stepSweep sweepSettings microwaveSourceAgent.Contents

        //return Streaming.Acquisition.create timeTaggerAgent.Contents (Streaming.Parameters.create (Model.Duration_ns (1.<ns> * (float dwellTime))) (Model.Duration_ns (1.<ns> * (float dwellTime))))
        let histogramParameters = Streaming.Acquisition.Histogram.Parameters.create (Model.Duration_ns (1.<ns> * (float dwellTime))) (Model.Duration_ns (1.<ns> * (float dwellTime))) Model.TTTRConfiguration.Marker0
        return Streaming.Acquisition.Histogram.create (experiment.PicoHarpStreamingAcquisition) histogramParameters
    }

    let stop experiment =
        experiment.StopHandle.Set() |> ignore
        experiment.AcquisitionTokenSource.Cancel()

    let run (experiment : CwOdmrExperiment) =
        let resultChannel = new ResultChannel<_>()

        let experimentWorkflow = async {
            experiment.StatusChanged.Trigger <| Next StartingExperiment

            use! microwaveSourceAgent = QueuedAccessAgent.requestControl experiment.MicrowaveSourceAgent
            use! pulseGeneratorAgent  = QueuedAccessAgent.requestControl experiment.PulseGeneratorAgent

            // switch off pulse streamer before writing new sequence
            do! PulseStreamer8.PulseStreamer.PulseSequence.setState [] pulseGeneratorAgent.Contents

            // setup time tagger acquisition and microwave source frequency / power; initialise processing pipeline
            let! histogramAcquisition = setupHardware experiment pulseGeneratorAgent microwaveSourceAgent

            use agent = Signal.createProcessingAgent experiment.Parameters
            use __ = Signal.initialiseProcessingAgent experiment histogramAcquisition (SignalProcessor agent)
            use __ = Signal.curveProjection experiment Signal.singlePointProjectionCalculator (SignalProcessor agent)

            // start time tagger and then write pulse sequence to pulse generator
            do! Basic.setOutput On microwaveSourceAgent.Contents

            let finalState = PulseStreamer8.Pulse.empty 0u
            do! PulseStreamer8.PulseStreamer.PulseSequence.writeSequence (cwOdmrSequence experiment.Parameters) 0u finalState finalState Immediate pulseGeneratorAgent.Contents

            experiment.StatusChanged.Trigger <| Next PerformingExperiment

            do! Async.AwaitWaitHandle (experiment.StopHandle) |> Async.Ignore
            do! PulseStreamer8.PulseStreamer.PulseSequence.setState [(Parameters.laserChannel experiment.Parameters)] pulseGeneratorAgent.Contents
            do! Basic.setOutput Off microwaveSourceAgent.Contents }

        Async.StartWithContinuations (experimentWorkflow,
            (fun () ->
                experiment.StatusChanged.Trigger (Next FinishedExperiment)
                experiment.StatusChanged.Trigger Completed
                resultChannel.RegisterResult ExperimentCompleted),
            (fun exn ->
                experiment.StatusChanged.Trigger (Error exn)
                resultChannel.RegisterResult (ExperimentError exn)),
            ignore)

        resultChannel.AwaitResult()