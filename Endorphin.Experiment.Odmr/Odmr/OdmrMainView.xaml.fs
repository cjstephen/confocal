// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr.Odmr.View

open Endorphin.Experiment.Odmr.Odmr
open FsXaml

type OdmrMainView = XAML<"Odmr/OdmrMainView.xaml">