// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr.Tau

open System
open System.Threading
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.Control.Reactive
open FSharp.ViewModule.Validation
open FSharp.Control.Reactive

open Endorphin.Core
open Endorphin.Instrument.PicoQuant.PicoHarp300
open Endorphin.Instrument.Keysight.N5172B
open Endorphin.Instrument.SwabianInstruments
open Endorphin.Experiment.Confocal

module TauExperiment = 

    [<AutoOpen>]
    module Model = 

        type Notes = { SampleNotes : string ; ExperimentNotes : string }

        type DetectionParameters = 
            { HistogramLength            : int<ns>
              LaserDuration              : int<ns> }

        type PulseStreamerParameters = 
            { AcquisitionChannel         : PulseStreamer8.Model.Channel
              LaserChannel               : PulseStreamer8.Model.Channel
              MicrowaveXChannel          : PulseStreamer8.Model.Channel
              MicrowaveYChannel          : PulseStreamer8.Model.Channel }

        type TauCurveProjectionParameters = 
            { IntegrationWindowOffset    : int<ns>
              IntegrationWindowWidth     : int<ns> }

        type TauExperimentParameters = 
            { Frequency                  : float<Hz>
              Amplitude                  : float<dBm>
              InitialTau                 : decimal
              TauInterval                : decimal
              NumberOfPoints             : int
              ShotsPerPoint              : int
              LaserSwitchDelay           : int<ns>
              MicrowaveSwitchDelay       : int<ns>
              TwoPhaseAcquisition        : bool
              PerformAutofocus           : bool
              PulseStreamerParameters    : PulseStreamerParameters
              DetectionParameters        : DetectionParameters
              Notes                      : Notes
              Date                       : DateTime
              AdditionalParameters       : Map<string, ValueType> }

        type Histograms = int[,]

        type Curve = (int * int)[]

        type TauSignalUpdate =
            | HistogramsAvailable  of histograms : Histograms * scanNumber : int64
            | CurveAvailable of projectedCurve : Curve list

        type TauExperimentStatus = 
            | StartingExperiment
            | PerformingExperiment
            | FinishedExperiment

        type TauExperimentResult =
            | ExperimentCompleted
            | ExperimentError of exn

        type TauExperiment = 
            { Parameters                   : TauExperimentParameters
              PicoHarpStreamingAcquisition : Streaming.StreamingAcquisition
              PulseGeneratorAgent          : QueuedAccessAgent<PulseStreamer8.Model.PulseStreamer8>
              MicrowaveSourceAgent         : QueuedAccessAgent<Endorphin.Instrument.Keysight.N5172B.Model.RfSource>
              StopHandle                   : ManualResetEvent
              AcquisitionTokenSource       : CancellationTokenSource
              StatusChanged                : NotificationEvent<TauExperimentStatus>
              DataAvailable                : Event<TauSignalUpdate>
              ProjectionParameters         : Event<TauCurveProjectionParameters> }

        type ScanIndex = 
            { NumberOfPoints            : int
              PointIndex                : int
              ShotsRemaining            : int
              ScanNumber                : int64 }

        type TauVariableParameters =
            { Unit                  : string        // unit name
              DefaultInitialValue   : decimal       // default initial tau
              DefaultInterval       : decimal       // default tau interval (in experiment)
              Precision             : decimal }     // tau increment size (i.e. the precision with which tau values can be defined)

        // type required to build generic Tau experiments
        type TauExperimentBuilderParameters =
              { Name                      : string // name of experiment (and window)
                AbscissaTitle             : string // name of x-axis in projected curve (pulse length (ns) for Rabi etc)
                TauParameters             : TauVariableParameters
                TwoPhaseAvailable         : bool
                AdditionalParameterFields : Endorphin.Experiment.Odmr.OdmrExperimentBuilder.ExperimentParameterFields // additional experimental parameters required beyond the default tau parameters
                PulseSequenceBuilder      : TauExperimentParameters -> PulseStreamer8.Model.PulseSequence // pulse sequence generator
                CurveProjectionCalculator : TauExperiment -> Histograms * TauCurveProjectionParameters -> Curve list // function which generates a curve projection from the accumulated histograms
                StartupFunction           : TauExperiment -> Async<unit> // function which is called before experiment is started to e.g. initialise extra hardware etc
                CleanupFunction           : TauExperiment -> Async<unit> } // function which is called after experiment is finished to e.g. stop extra hardware etc

    module Parameters = 
        // query functions
        let frequency parameters = parameters.Frequency

        let amplitude parameters = parameters.Amplitude

        let initialTau parameters = parameters.InitialTau

        let tauInterval parameters = parameters.TauInterval

        let numberOfPoints (parameters : TauExperimentParameters) = parameters.NumberOfPoints
              
        let additionalParameters parameters = parameters.AdditionalParameters

        let laserSwitchDelay parameters = parameters.LaserSwitchDelay

        let microwaveSwitchDelay parameters = parameters.MicrowaveSwitchDelay

        let detectionParameters parameters = parameters.DetectionParameters

        let shotsPerPoint parameters = parameters.ShotsPerPoint

        let performAutofocus parameters = parameters.PerformAutofocus

        let notes parameters = parameters.Notes

        let date parameters = parameters.Date
        
        let histogramLength parameters = (detectionParameters parameters).HistogramLength

        let laserDuration parameters = (detectionParameters parameters).LaserDuration

        let twoPhaseAcquisition parameters = parameters.TwoPhaseAcquisition

        let pulseStreamerParameters parameters = parameters.PulseStreamerParameters

        let acquisitionChannel parameters = (pulseStreamerParameters parameters).AcquisitionChannel

        let laserChannel parameters = (pulseStreamerParameters parameters).LaserChannel

        let microwaveXChannel parameters = (pulseStreamerParameters parameters).MicrowaveXChannel

        let microwaveYChannel parameters = (pulseStreamerParameters parameters).MicrowaveYChannel

        let integrationWindowOffset projectionParameters = projectionParameters.IntegrationWindowOffset

        let integrationWindowWidth projectionParameters = projectionParameters.IntegrationWindowWidth
      
        // modify functions
        let withFrequency frequency parameters : TauExperimentParameters = { parameters with Frequency = frequency }

        let withAmplitude amplitude parameters : TauExperimentParameters = { parameters with Amplitude = amplitude }

        let withInitialTau initialTau parameters = { parameters with InitialTau  = initialTau }

        let withTauInterval tauInterval parameters = { parameters with TauInterval = tauInterval }

        let withNumberOfPoints numberOfPoints (parameters : TauExperimentParameters) = { parameters with NumberOfPoints = numberOfPoints }

        let withAdditionalParameters additionalParameters parameters = { parameters with AdditionalParameters = additionalParameters }

        let withLaserSwitchDelay laserSwitchDelay parameters = { parameters with LaserSwitchDelay = laserSwitchDelay }

        let withMicrowaveSwitchDelay microwaveSwitchDelay parameters = { parameters with MicrowaveSwitchDelay = microwaveSwitchDelay }

        let withDetectionParameters detectionParameters parameters = { parameters with DetectionParameters = detectionParameters }

        let withShotsPerPoints shotsPerPoint parameters : TauExperimentParameters = { parameters with ShotsPerPoint = shotsPerPoint }

        let withPerformAutofocus performAutofocus parameters = { parameters with PerformAutofocus = performAutofocus }

        let withNotes notes parameters = { parameters with Notes = notes }

        let withDate date parameters = { parameters with Date = date }

        let withHistogramLength detectionHistogramLength parameters = withDetectionParameters { (detectionParameters parameters) with HistogramLength = detectionHistogramLength } parameters

        let withLaserDuration laserDuration parameters = withDetectionParameters { (detectionParameters parameters) with LaserDuration = laserDuration } parameters

        let withTwoPhaseAcquisition twoPhaseAcquisition parameters = { parameters with TwoPhaseAcquisition = twoPhaseAcquisition }

        let withPulseStreamerParameters pulseStreamerParameters parameters = { parameters with PulseStreamerParameters = pulseStreamerParameters }

        let withIntegrationWindowOffset integrationWindowOffset projectionParameters = { projectionParameters with IntegrationWindowOffset = integrationWindowOffset } 

        let withIntegrationWidth integrationWindowWidth projectionParameters = { projectionParameters with IntegrationWindowWidth = integrationWindowWidth }

        let withAcquisitionChannel acquisitionChannel parameters = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with AcquisitionChannel = acquisitionChannel }}

        let withLaserChannel laserChannel parameters = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with LaserChannel = laserChannel }}

        let withMicrowaveXChannel microwaveXChannel parameters = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with MicrowaveXChannel = microwaveXChannel }}

        let withMicrowaveYChannel microwaveYChannel parameters = { parameters with PulseStreamerParameters = { (pulseStreamerParameters parameters) with MicrowaveYChannel = microwaveYChannel }}

        // creation functions
        let create frequency amplitude initialTau tauInterval numberOfPoints shotsPerPoint laserSwitchDelay microwaveSwitchDelay twoPhaseAcquisition performAutofocus pulseStreamerParameters detectionParameters additionalParameters = 
            { Frequency               = frequency
              Amplitude               = amplitude
              InitialTau              = initialTau
              TauInterval             = tauInterval
              NumberOfPoints          = numberOfPoints
              ShotsPerPoint           = shotsPerPoint
              LaserSwitchDelay        = laserSwitchDelay
              MicrowaveSwitchDelay    = microwaveSwitchDelay
              TwoPhaseAcquisition     = twoPhaseAcquisition
              PerformAutofocus        = performAutofocus
              PulseStreamerParameters = pulseStreamerParameters
              DetectionParameters     = detectionParameters  
              Date                    = DateTime.Now
              Notes                   = { SampleNotes = ""; ExperimentNotes = "" }
              AdditionalParameters    = additionalParameters }

        let createDetectionParameters histogramLength laserDuration = 
            { HistogramLength         = histogramLength
              LaserDuration           = laserDuration }

        let createPulseStreamerParameters acquisitionChannel laserChannel microwaveXChannel microwaveYChannel = 
            { AcquisitionChannel      = acquisitionChannel
              LaserChannel            = laserChannel
              MicrowaveXChannel       = microwaveXChannel
              MicrowaveYChannel       = microwaveYChannel }

        let createProjectionParameters integrationWindowOffset integrationWindowWidth = 
            { IntegrationWindowOffset = integrationWindowOffset; IntegrationWindowWidth = integrationWindowWidth }

    module Notes = 
        let sampleNotes notes = notes.SampleNotes

        let experimentNotes notes = notes.ExperimentNotes

        let withSampleNotes sampleNotes notes = { notes with SampleNotes = sampleNotes }

        let withExperimentNotes experimentNotes notes = { notes with ExperimentNotes = experimentNotes }

    [<AutoOpen>]
    module SignalProcessor =
        type Signal =
            { AccumulatedScans  : int[,]
              CurrentSnapshot   : int[,] * int64 }
    
        type SignalProcessorMessage =
            | SignalUpdate    of scanIndex : ScanIndex * histogram : Streaming.Histogram 
            | SnapshotRequest of replyChannel : AsyncReplyChannel<int[,] * int64>
    
        type SignalProcessor = SignalProcessor of agent : Agent<SignalProcessorMessage>
    
        let performAutofocus experiment pulseSequence (autofocusAutomator: MailboxProcessor<Autofocus.Automation.AutofocusMessage>) autofocusInterval = async {
            printfn "performing autofocus at %s" (DateTime.Now.ToLongTimeString())
            autofocusAutomator.Post (Autofocus.Automation.AutofocusMessage.PerformAutofocus)
            printfn "sleeping for 30 seconds"
            do! Async.Sleep(30000)

            printfn "restarting acquisition"
            use! pulseGeneratorAgent  = QueuedAccessAgent.requestControl experiment.PulseGeneratorAgent

            // switch off pulse streamer before writing new sequence
            do! PulseStreamer8.PulseStreamer.PulseSequence.setState [] pulseGeneratorAgent.Contents

            let finalState = PulseStreamer8.Pulse.empty 0u
            let iterations = uint32 autofocusInterval

            do! PulseStreamer8.PulseStreamer.PulseSequence.writeSequence pulseSequence iterations finalState finalState PulseStreamer8.Model.Immediate pulseGeneratorAgent.Contents } |> Async.StartImmediate

        let internal accumulateHistogram (histogram : Streaming.Histogram) (scanIndex : ScanIndex) (createSnapshot : bool) (shouldPerformAutofocus : bool) experiment pulseSequence autofocusInterval autofocusMailbox (signal : Signal) : Signal =
            let accumulatedScans = signal.AccumulatedScans
    
            histogram.Histogram
            |> List.iter (fun (bin, counts) -> accumulatedScans.[bin, scanIndex.PointIndex] <- accumulatedScans.[bin, scanIndex.PointIndex] + counts) 

            if shouldPerformAutofocus then
                do performAutofocus experiment pulseSequence autofocusMailbox autofocusInterval

            if (scanIndex.PointIndex = scanIndex.NumberOfPoints - 1) && (scanIndex.ShotsRemaining = 0) && createSnapshot then
                { AccumulatedScans = accumulatedScans; CurrentSnapshot = (Array2D.copy accumulatedScans, scanIndex.ScanNumber) }
            else 
                { AccumulatedScans = accumulatedScans; CurrentSnapshot = signal.CurrentSnapshot }

        let takeSnapshot signal = signal.CurrentSnapshot
     
        /// agent which accumulates signals internally and provides copies of data for projection processing on request
        let createAgent experiment snapshotUpdateInterval autofocusInterval autofocusMailbox pulseSequence = 
            let parameters = experiment.Parameters

            let accumulatedScans = 
                if (Parameters.twoPhaseAcquisition parameters) then 
                    Array2D.zeroCreate<int> (int parameters.DetectionParameters.HistogramLength) (2 * parameters.NumberOfPoints)
                else
                    Array2D.zeroCreate<int> (int parameters.DetectionParameters.HistogramLength) parameters.NumberOfPoints
            let currentSnapshot = (Array2D.zeroCreate<int> (Array2D.length1 accumulatedScans) (Array2D.length2 accumulatedScans), 0L)
    
            Agent.Start(fun mailbox -> 
                let rec loop signal = async { 
                    let! message = mailbox.Receive ()
                    match message with 
                    | SignalUpdate (scanIndex, histogram) -> return! loop (accumulateHistogram histogram scanIndex (scanIndex.ScanNumber % (int64 snapshotUpdateInterval) = 0L) (((scanIndex.ScanNumber + 1L) % (int64 autofocusInterval) = 0L) && scanIndex.PointIndex = (parameters.NumberOfPoints - 1) && parameters.PerformAutofocus) experiment pulseSequence autofocusInterval autofocusMailbox signal)
                    | SnapshotRequest (replyChannel)      -> replyChannel.Reply (takeSnapshot signal); return! loop signal }

                loop { AccumulatedScans = accumulatedScans; CurrentSnapshot = currentSnapshot } )

    module Signal = 
        let pointIndexAndShotsRemaining parameters (i : int64) = 
            let shotsPerPoint = int64 parameters.ShotsPerPoint
            let numberOfPoints = 
                if (Parameters.twoPhaseAcquisition parameters) then
                    int64 ((parameters.NumberOfPoints) * 2)
                else
                    int64 parameters.NumberOfPoints

            let pointIndex = (i / shotsPerPoint) % numberOfPoints
            let shotsRemaining = shotsPerPoint - ((i % (shotsPerPoint * numberOfPoints)) - (pointIndex * shotsPerPoint)) - 1L
            let scanNumber = (i / (shotsPerPoint * numberOfPoints))
            { NumberOfPoints = parameters.NumberOfPoints; PointIndex = int pointIndex; ShotsRemaining = int shotsRemaining; ScanNumber = scanNumber }
                        
        let createProcessingAgent parameters = 
            SignalProcessor.createAgent parameters

        let private incomingHistogramsWithIndex experiment acquisition = 
            let emptyHistogram : Streaming.Histogram = { Histogram = List.empty }

            Streaming.Acquisition.Histogram.HistogramsAvailable acquisition
            |> Observable.scanInit (-1L, emptyHistogram) (fun (i, _) histogram -> (i+1L, histogram))
            |> Observable.map (fun (i, histogram) -> (pointIndexAndShotsRemaining experiment.Parameters i, histogram) )
    
        let initialiseProcessingAgent experiment acquisition (SignalProcessor agent) = 
            incomingHistogramsWithIndex experiment acquisition
            |> Observable.subscribe (SignalUpdate >> agent.Post)
            
        let curveProjection experiment (projectionCalculator : TauExperiment -> Histograms * TauCurveProjectionParameters -> Curve list) (SignalProcessor agent) =
                let histogram =
                    Observable.interval (TimeSpan.FromMilliseconds 300.)
                    |> Observable.flatmapAsync (fun _ -> async { return! agent.PostAndAsyncReply(SnapshotRequest)})

                let projectionParameterObservable = experiment.ProjectionParameters.Publish
                               
                Observable.combineLatest histogram projectionParameterObservable
                |> Observable.sample (TimeSpan.FromMilliseconds 300.)
                |> Observable.iter (fun x -> experiment.DataAvailable.Trigger (HistogramsAvailable (fst x))) // trigger new histograms available
                |> Observable.map (fun x -> projectionCalculator experiment (fst (fst x), snd x)) // transform to curve projection
                |> Observable.subscribe (fun x -> experiment.DataAvailable.Trigger (CurveAvailable x)) // trigger new curve available

    let create parameters picoHarpStreamingAcquisition pulseGeneratorAgent microwaveSourceAgent = 
        { Parameters                    = parameters
          PicoHarpStreamingAcquisition  = picoHarpStreamingAcquisition
          PulseGeneratorAgent           = pulseGeneratorAgent
          MicrowaveSourceAgent          = microwaveSourceAgent
          StopHandle                    = new ManualResetEvent(false)
          AcquisitionTokenSource        = new CancellationTokenSource()
          StatusChanged                 = NotificationEvent<_>()
          DataAvailable                 = Event<_>()
          ProjectionParameters          = Event<_>() }

    let status experiment = 
        experiment.StatusChanged.Publish
        |> Observable.fromNotificationEvent

    let statusMessage = function
        | StartingExperiment    -> "Starting experiment"
        | PerformingExperiment  -> "Performing experiment"
        | FinishedExperiment    -> "Finished experiment"

    let setupDetectionHardware experiment = async {
        use! pulseGeneratorAgent  = QueuedAccessAgent.requestControl experiment.PulseGeneratorAgent

        // switch off pulse streamer before writing new sequence
        do! PulseStreamer8.PulseStreamer.PulseSequence.setState [] pulseGeneratorAgent.Contents

        // setup time tagger acquisition and microwave source frequency / power; initialise processing pipeline
        let histogramParameters = Streaming.Acquisition.Histogram.Parameters.create (Model.Duration_ns 1.<ns>) (Model.Duration_ns (1.<ns> * float experiment.Parameters.DetectionParameters.HistogramLength)) Model.TTTRConfiguration.Marker0
        return Streaming.Acquisition.Histogram.create (experiment.PicoHarpStreamingAcquisition) histogramParameters
    }

    let setupHardware experiment pulseSequence autofocusInterval = async {
        use! microwaveSourceAgent = QueuedAccessAgent.requestControl experiment.MicrowaveSourceAgent
        use! pulseGeneratorAgent  = QueuedAccessAgent.requestControl experiment.PulseGeneratorAgent

        do! setCarrierFrequency (Frequency_Hz experiment.Parameters.Frequency) microwaveSourceAgent.Contents 
        do! setCarrierAmplitude (Power_dBm experiment.Parameters.Amplitude)    microwaveSourceAgent.Contents 
        do! Basic.setOutput     On                                             microwaveSourceAgent.Contents 
        
        let finalState = PulseStreamer8.Pulse.empty 0u
        let iterations = 
            if experiment.Parameters.PerformAutofocus then 
                printfn "Autofocus interval: %d" (uint32 (autofocusInterval))
                uint32 (autofocusInterval)
            else
                0u
        do! PulseStreamer8.PulseStreamer.PulseSequence.writeSequence pulseSequence iterations finalState finalState PulseStreamer8.Model.Immediate pulseGeneratorAgent.Contents   
    }

    let shutdownHardware experiment = async {
        use! microwaveSourceAgent = QueuedAccessAgent.requestControl experiment.MicrowaveSourceAgent
        use! pulseGeneratorAgent  = QueuedAccessAgent.requestControl experiment.PulseGeneratorAgent

        do! PulseStreamer8.PulseStreamer.PulseSequence.setState [experiment.Parameters.PulseStreamerParameters.LaserChannel] pulseGeneratorAgent.Contents 
        do! Basic.setOutput Off microwaveSourceAgent.Contents
    }

    let stop experiment =
        experiment.StopHandle.Set() |> ignore
        experiment.AcquisitionTokenSource.Cancel()

    let run experiment autofocusMailbox initialProjectionParameters pulseSequenceBuilder curveProjectionCalculator startupFunction cleanupFunction =
        let resultChannel = new ResultChannel<_>()

        let experimentWorkflow = async {
            experiment.StatusChanged.Trigger <| Next StartingExperiment

            let parameters = experiment.Parameters

            let pulseSequence = (pulseSequenceBuilder experiment.Parameters)
            let pulseSequenceLength = (PulseStreamer8.Pulse.sequenceLength pulseSequence) // in nanoseconds

            let roundToNearestEvenInteger number = 
                if (number % 2) = 0 then
                    number
                else
                    number + 1

            let autofocusInterval = roundToNearestEvenInteger (int ((300. * 1E9) / (float pulseSequenceLength)))

            let snapshotUpdateInterval = 
                if pulseSequenceLength > 245000000UL then
                    1
                else
                    if (Parameters.performAutofocus parameters) then
                        int (250000000.<ns> / (2. * (float pulseSequenceLength)))
                    else
                        int (250000000.<ns> / (float pulseSequenceLength))

            let! histogramAcquisition = setupDetectionHardware experiment

            use agent = Signal.createProcessingAgent experiment snapshotUpdateInterval autofocusInterval autofocusMailbox pulseSequence
            use __ = Signal.initialiseProcessingAgent experiment histogramAcquisition (SignalProcessor agent)
            use __ = Signal.curveProjection experiment curveProjectionCalculator (SignalProcessor agent)

            // trigger initial projection parameters to initialise curve 
            experiment.ProjectionParameters.Trigger initialProjectionParameters

            do! startupFunction experiment
            do! setupHardware experiment pulseSequence autofocusInterval
            experiment.StatusChanged.Trigger <| Next PerformingExperiment

            do! Async.AwaitWaitHandle (experiment.StopHandle) |> Async.Ignore

            do! shutdownHardware experiment
            do! cleanupFunction experiment }

        Async.StartWithContinuations (experimentWorkflow,
            (fun () -> 
                experiment.StatusChanged.Trigger (Next FinishedExperiment)
                experiment.StatusChanged.Trigger Completed
                resultChannel.RegisterResult ExperimentCompleted),
            (fun exn ->
                experiment.StatusChanged.Trigger (Error exn)
                resultChannel.RegisterResult (ExperimentError exn)),
            ignore)

        resultChannel.AwaitResult()