// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr.Tau

open System
open System.IO
open System.Threading
open System.Windows
open System.Windows.Data
open System.Windows.Input
open System.Windows.Forms
open Microsoft.Win32
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols

open FsXaml
open OxyPlot
open Nessos.FsPickler
open Nessos.FsPickler.Json
open Endorphin.Experiment.Odmr
open Endorphin.Experiment.Odmr.OdmrExperimentBuilder
open TauExperiment
open Endorphin.Core
open Endorphin.Instrument.PicoQuant.PicoHarp300
open Endorphin.Instrument.Keysight.N5172B
open Endorphin.Instrument.SwabianInstruments

open System
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open FSharp.Control.Reactive
open FSharp.ViewModule
open FSharp.ViewModule.Validation
open FSharp.Control.Reactive

type TauExperimentState = 
    | Waiting
    | Running of experiment : TauExperiment
    | Finished of parameters : TauExperimentParameters * scansCompleted : int64

type TauViewModel(picoHarpStreamingAcquisition, microwaveSourceAgent, pulseGeneratorAgent, autofocusMailbox, experimentBuilderParameters, pulseSequenceImagePath : string) as self = 
    inherit ViewModelBase()

    let defaultPulseStreamerParameters = Parameters.createPulseStreamerParameters
                                         <| PulseStreamer8.Model.Channel0
                                         <| PulseStreamer8.Model.Channel1
                                         <| PulseStreamer8.Model.Channel2
                                         <| PulseStreamer8.Model.Channel3

    let defaultDetectionParameters = Parameters.createDetectionParameters 3000<ns> 4000<ns>

    let (defaultFrequency, defaultPower) = 
        async {
            use! microwaveSourceAgent = QueuedAccessAgent.requestControl microwaveSourceAgent
            let! frequency = Basic.queryCarrierFrequency microwaveSourceAgent.Contents 
            let! power = Basic.queryCarrierAmplitude microwaveSourceAgent.Contents 
            return (frequency, power) }
        |> Async.RunSynchronously

    let tauVariableParameters = experimentBuilderParameters.TauParameters
    
    let generateDefaultParameters (Frequency_Hz defaultFrequency) (Power_dBm defaultPower) = 
                            Parameters.create 
                            <| defaultFrequency
                            <| defaultPower
                            <| tauVariableParameters.DefaultInitialValue
                            <| tauVariableParameters.DefaultInterval
                            <| 100
                            <| 1
                            <| 1300<ns>
                            <| 410<ns>
                            <| false
                            <| true
                            <| defaultPulseStreamerParameters
                            <| defaultDetectionParameters
                            <| Map.empty<string, ValueType>

    let defaultParameters = generateDefaultParameters defaultFrequency defaultPower
     
    let defaultProjectionParameters = Parameters.createProjectionParameters 1280<ns> 400<ns>

    // generate additional UI controls
    let additionalParameterControls = Map.toList experimentBuilderParameters.AdditionalParameterFields
                                      |> List.sortBy (fun (_, (id, _, _, _)) -> id)
                                      |> List.map (fun (key,(_, name, value, unit)) -> ParameterControlFactory key (name, value, unit))

    let histogramSeries = new Series.HeatMapSeries()
    do histogramSeries.Interpolate <- true
    do histogramSeries.Data <- Array2D.zeroCreate 1 1

    let histogramSeriesDefaultPlotModel = 
        let model = new PlotModel()
        model.Series.Add histogramSeries
        
        let axes = new OxyPlot.Axes.LinearColorAxis()
        axes.IsAxisVisible <- false
        model.Axes.Add axes

        let bottomAxis = new OxyPlot.Axes.LinearAxis()
        bottomAxis.IsAxisVisible <- true
        bottomAxis.Position <- OxyPlot.Axes.AxisPosition.Bottom
        bottomAxis.Title <- "Time (ns)"
        model.Axes.Add bottomAxis

        let leftAxis = new OxyPlot.Axes.LinearAxis()
        leftAxis.IsAxisVisible <- true
        leftAxis.Position <- OxyPlot.Axes.AxisPosition.Left
        leftAxis.Title <- experimentBuilderParameters.AbscissaTitle
        model.Axes.Add leftAxis

        model

    let projectedPrimaryPhaseCurveSeries = new Series.LineSeries()
    let projectedSecondaryPhaseCurveSeries = new Series.LineSeries()
    let projectedCurveSeries = new Series.LineSeries()

    let projectedCurveDefaultPlotModel = 
        let model = new PlotModel()
        model.Series.Add projectedCurveSeries

        let bottomAxis = new OxyPlot.Axes.LinearAxis()
        bottomAxis.IsAxisVisible <- true
        bottomAxis.Position <- OxyPlot.Axes.AxisPosition.Bottom
        bottomAxis.Title <- experimentBuilderParameters.AbscissaTitle
        model.Axes.Add bottomAxis

        let leftAxis = new OxyPlot.Axes.LinearAxis()
        leftAxis.IsAxisVisible <- true
        leftAxis.Position <- OxyPlot.Axes.AxisPosition.Left
        leftAxis.Title <- "Intensity (counts)"
        model.Axes.Add leftAxis
        
        model

    let experimentParameters = self.Factory.Backing(<@ self.ExperimentParameters @>, defaultParameters)
    let projectionParameters = self.Factory.Backing(<@ self.ProjectionParameters @>, defaultProjectionParameters)
    let notes                = self.Factory.Backing(<@ self.Notes @>, { ExperimentNotes = "" ; SampleNotes = "" }) 
    let experimentState = self.Factory.Backing(<@ self.ExperimentState @>, Waiting)
    let histogramPlotModel = self.Factory.Backing(<@ self.HistogramPlotModel @>, histogramSeriesDefaultPlotModel)
    let projectedCurvePlotModel = self.Factory.Backing(<@ self.ProjectedCurvePlotModel @>, projectedCurveDefaultPlotModel)
    let statusMessage = self.Factory.Backing(<@ self.StatusMessage @>, "")
    let experimentTimeMessage = self.Factory.Backing(<@ self.ExperimentTimeMessage @>, "")
    let scansCompleted = self.Factory.Backing(<@ self.ScansCompleted @>, 0L)
    let drawBothPhases = self.Factory.Backing(<@ self.DrawBothPhases @>, false)

    let serialiser = FsPickler.CreateJsonSerializer (indent = true)

    /// save the recorded data
    let save () =
        match self.ExperimentState with
        | Finished (parameters, scansCompleted) ->
            let saveFile = new SaveFileDialog (Filter="Tau ODMR experiment|*.tau")
            let result = saveFile.ShowDialog()
            if result.HasValue && result.Value then
                use paramsFile = new StreamWriter (saveFile.FileName)
                parameters |> Parameters.withNotes self.Notes |> serialiser.PickleToString |> paramsFile.Write
                paramsFile.WriteLine (sprintf "Scans completed: %d" scansCompleted)
        
                use histogramDataFile = new StreamWriter(Path.Combine ((Path.GetDirectoryName saveFile.FileName), ((Path.GetFileNameWithoutExtension saveFile.FileName) + "_histograms.csv")))
                for i in 0..(histogramSeries.Data.GetLength(0) - 1) do
                    for j in 0..(histogramSeries.Data.GetLength(1) - 1) do
                        histogramSeries.Data.[i,j] |> histogramDataFile.Write
                        "," |> histogramDataFile.Write
                    histogramDataFile.WriteLine ()
        
                if (Parameters.twoPhaseAcquisition parameters) then
                    use projectedCurveDataFile = new StreamWriter(Path.Combine ((Path.GetDirectoryName saveFile.FileName), ((Path.GetFileNameWithoutExtension saveFile.FileName) + "_projectedCurve.csv")))
                    Seq.iter2 (fun (phase1 : DataPoint) (phase2 : DataPoint) ->
                        phase1.X |> projectedCurveDataFile.Write
                        "," |> projectedCurveDataFile.Write
                        phase1.Y |> projectedCurveDataFile.Write
                        "," |> projectedCurveDataFile.Write
                        phase2.Y |> projectedCurveDataFile.WriteLine) projectedPrimaryPhaseCurveSeries.Points projectedSecondaryPhaseCurveSeries.Points
                else 
                    use projectedCurveDataFile = new StreamWriter(Path.Combine ((Path.GetDirectoryName saveFile.FileName), ((Path.GetFileNameWithoutExtension saveFile.FileName) + "_projectedCurve.csv")))
                    Seq.iter (fun (point : DataPoint) ->
                        point.X |> projectedCurveDataFile.Write
                        "," |> projectedCurveDataFile.Write
                        point.Y |> projectedCurveDataFile.WriteLine) projectedCurveSeries.Points
        | _ -> "No data to save!" |> MessageBox.Show |> ignore


    /// loads parameters from a user-specified Pulsed ODMR experiment parameters file.
    let loadParameters () =
        if self.IsReadyToStart then
            let openFile = new OpenFileDialog (Filter="Tau ODMR experiment|*.tau")
            let result = openFile.ShowDialog()
            if result.HasValue && result.Value then
                use paramsFile = new StreamReader (openFile.FileName)
                let parameters = paramsFile.ReadToEnd() |> serialiser.UnPickleOfString
                self.ExperimentParameters <- parameters 
                self.Notes                <- parameters |> Parameters.notes 

    let performExperiment ui = async {
        do! Async.SwitchToContext ui

        let cts = new CancellationTokenSource()
        self.ProjectedCurveData.Clear()

        // fetch current value of all additional parameter controls
        let additionalExperimentParameters = List.map (fun (control : IParameterFieldControl)-> (control.Name, control.Value)) additionalParameterControls
                                             |> Map.ofList

        let parameters = self.ExperimentParameters |> Parameters.withDate DateTime.Now |> Parameters.withAdditionalParameters additionalExperimentParameters
        let experiment = TauExperiment.create parameters picoHarpStreamingAcquisition pulseGeneratorAgent microwaveSourceAgent 
        
        use __ = 
            TauExperiment.status experiment
            |> Observable.observeOnContext ui
            |> Observable.subscribeWithError
                (fun status -> self.StatusMessage <- TauExperiment.statusMessage status)
                (fun exn    -> self.StatusMessage <- sprintf "Experiment failed: %s" exn.Message)

        use __ = 
            experiment.DataAvailable.Publish
            |> Observable.observeOnContext ui
            |> Observable.mapi (fun i x -> (i, x))
            |> Observable.subscribe (fun (i, x) ->
                match x with
                | CurveAvailable (curve) -> 
                    self.ProjectedCurveData.Clear()

                    if (Parameters.twoPhaseAcquisition parameters) then
                        self.ProjectedCurvePrimaryPhaseData.Clear()
                        curve.[0] |> Array.map (fun (x, y) -> DataPoint(float x, float y)) |> self.ProjectedCurvePrimaryPhaseData.AddRange
                        self.ProjectedCurveSecondaryPhaseData.Clear()
                        curve.[1] |> Array.map (fun (x, y) -> DataPoint(float x, float y)) |> self.ProjectedCurveSecondaryPhaseData.AddRange
                        Array.map2 (fun (x,y1) (_,y2) -> DataPoint (float x, float (y1 - y2))) curve.[0] curve.[1] |> self.ProjectedCurveData.AddRange
                    else 
                        curve.[0] |> Array.map (fun (x, y) -> DataPoint(float x, float y)) |> self.ProjectedCurveData.AddRange 

                    self.ProjectedCurvePlotModel.InvalidatePlot true
                | HistogramsAvailable (histograms, scanNumber) ->
                    // user interface struggles to keep up with v. large heatmaps
                    self.ScansCompleted <- scanNumber
                    if (Parameters.numberOfPoints parameters > 100) then
                        histogramSeries.Data <- Array2D.map float histograms
                        if i % 80 = 0 then
                            self.HistogramPlotModel.InvalidatePlot true
                    else
                        histogramSeries.Data <- Array2D.map float histograms
                        self.HistogramPlotModel.InvalidatePlot true)

        histogramSeries.Data <- Array2D.zeroCreate 1 1
        histogramSeries.X0 <- float 0
        histogramSeries.X1 <- float <| Parameters.histogramLength parameters
        histogramSeries.Y0 <- float <| Parameters.initialTau parameters
        histogramSeries.Y1 <- float <| ((Parameters.initialTau parameters) + (decimal (Parameters.numberOfPoints parameters)) * (Parameters.tauInterval parameters))

        self.ExperimentState <- Running(experiment)
 
        use __ = 
            Observable.interval (TimeSpan.FromSeconds 1.)
            |> Observable.subscribe (fun i -> 
                let timeElapsed = TimeSpan.FromSeconds (float i)
                self.ExperimentTimeMessage <- (sprintf "Time elapsed: %02d:%02d:%02d; Scans completed: " (timeElapsed.Days * 24 + timeElapsed.Hours) (timeElapsed.Minutes) (timeElapsed.Seconds)))

        do! Async.SwitchToThreadPool()
 
        let! experimentResult = TauExperiment.run 
                                <| experiment 
                                <| autofocusMailbox
                                <| self.ProjectionParameters
                                <| experimentBuilderParameters.PulseSequenceBuilder 
                                <| experimentBuilderParameters.CurveProjectionCalculator 
                                <| experimentBuilderParameters.StartupFunction
                                <| experimentBuilderParameters.CleanupFunction

        do! Async.SwitchToContext ui
        self.ExperimentState <- Finished(parameters, scansCompleted.Value)
    }

    // commands
    let startExperimentCommand = self.Factory.CommandAsync performExperiment

    let stopExperimentCommand = 
        self.Factory.CommandSyncParamChecked(
            (function 
                | Running (experiment) -> TauExperiment.stop experiment
                | _ -> ()), 
            (fun _ -> self.IsPerformingExperiment), 
            [ <@ self.IsPerformingExperiment @> ])

    let saveCommand =   
        self.Factory.CommandSyncChecked(
            save,
            (fun () -> match self.ExperimentState with Finished _ -> true | _ -> false),
            [ <@ self.ExperimentState @> ])

    let loadParametersCommand = 
          self.Factory.CommandSyncChecked(
            loadParameters,
            (fun () -> self.IsReadyToStart),
            [ <@ self.IsReadyToStart @> ])

    do
        self.DependencyTracker.AddPropertyDependency(<@ self.Notes @>,                  <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.AcquisitionChannel @>,     <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.FinalTauLength @>,         <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.InitialTauDuration @>,     <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IntegrationWindowOffset @>,<@ self.ProjectionParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IntegrationWindowWidth @>, <@ self.ProjectionParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.LaserChannel @>,           <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.LaserSwitchDelay @>,       <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.MicrowaveAmplitude @>,     <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.MicrowaveXChannel @>,      <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.MicrowaveYChannel @>,      <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.MicrowaveFrequency @>,     <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.TauDurationChange @>,      <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.ShotsPerPoint @>,          <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.LaserDuration @>,          <@ self.ExperimentParameters @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.TwoPhaseAcquisition@>,     <@ self.ExperimentParameters @>)

        self.DependencyTracker.AddPropertyDependency(<@ self.SampleNotes @>,            <@ self.Notes @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.ExperimentNotes @>,        <@ self.Notes @>)
        
        self.DependencyTracker.AddPropertyDependency(<@ self.IsPerformingExperiment @>, <@ self.ExperimentState @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.IsReadyToStart @>,         <@ self.IsPerformingExperiment @>)
        self.DependencyTracker.AddPropertyDependency(<@ self.TwoPhaseReadyToStart @>,   <@ self.IsReadyToStart @>)

    member x.HistogramPlotModel : PlotModel = histogramPlotModel.Value
    member x.HistogramData : Double[,] = histogramSeries.Data
    member x.ProjectedCurvePlotModel : PlotModel = projectedCurvePlotModel.Value
    member x.ProjectedCurveData : ResizeArray<DataPoint> = projectedCurveSeries.Points
    member x.ProjectedCurvePrimaryPhaseData : ResizeArray<DataPoint> = projectedPrimaryPhaseCurveSeries.Points
    member x.ProjectedCurveSecondaryPhaseData : ResizeArray<DataPoint> = projectedSecondaryPhaseCurveSeries.Points
 
    member x.IsPerformingExperiment = x.ExperimentState |> (function Running _ -> true | _ -> false)
    member x.IsReadyToStart = not x.IsPerformingExperiment

    member x.ExperimentParameters
        with get() : TauExperimentParameters = experimentParameters.Value 
        and  set value = experimentParameters.Value <- value

    member x.ProjectionParameters
        with get()     = projectionParameters.Value
        and  set value = projectionParameters.Value <- value
    
    member x.ExperimentState
        with get()     = experimentState.Value
        and  set value = experimentState.Value <- value

    member x.StatusMessage
        with get()     = statusMessage.Value
        and  set value = statusMessage.Value <- value

    member x.ExperimentTimeMessage
        with get()     = experimentTimeMessage.Value
        and  set value = experimentTimeMessage.Value <- value

    member x.ScansCompleted
        with get()     = scansCompleted.Value
        and  set (value : int64) = scansCompleted.Value <- value

    member x.MicrowaveFrequency
        with get()                  = (decimal (Parameters.frequency x.ExperimentParameters) / 1E9m)
        and  set (value : decimal)  = x.ExperimentParameters <- Parameters.withFrequency ((float (value * 1E9m)) * 1.<Hz>) x.ExperimentParameters

    member x.MicrowaveAmplitude
        with get()                  = decimal (Parameters.amplitude x.ExperimentParameters)
        and  set (value : decimal)  = x.ExperimentParameters <- Parameters.withAmplitude ((float value) * 1.<dBm>) x.ExperimentParameters

    member x.InitialTauDuration
        with get()     = Parameters.initialTau x.ExperimentParameters
        and  set value = x.ExperimentParameters <- Parameters.withInitialTau value x.ExperimentParameters

    member x.TauDurationChange 
        with get()     = Parameters.tauInterval x.ExperimentParameters
        and  set value = x.ExperimentParameters <- Parameters.withTauInterval value x.ExperimentParameters

    member x.FinalTauLength
        with get()     = (decimal (Parameters.numberOfPoints x.ExperimentParameters)) * (Parameters.tauInterval x.ExperimentParameters) + (Parameters.initialTau x.ExperimentParameters)
        and  set value = 
            let numberOfPoints = int ((value - (Parameters.initialTau x.ExperimentParameters)) / (Parameters.tauInterval x.ExperimentParameters))
            (x.ExperimentParameters <- Parameters.withNumberOfPoints numberOfPoints x.ExperimentParameters)

    member x.LaserDuration
        with get()     = Parameters.laserDuration x.ExperimentParameters
        and  set value = x.ExperimentParameters <- Parameters.withLaserDuration value x.ExperimentParameters

    member x.PerformAutofocus
        with get()     = Parameters.performAutofocus x.ExperimentParameters
        and  set value = x.ExperimentParameters <- Parameters.withPerformAutofocus value x.ExperimentParameters

    member x.ShotsPerPoint
        with get()     = Parameters.shotsPerPoint x.ExperimentParameters
        and  set value = x.ExperimentParameters <- Parameters.withShotsPerPoints value x.ExperimentParameters

    member x.IntegrationWindowOffset
        with get()     = int (Parameters.integrationWindowOffset x.ProjectionParameters)
        and  set value = 
            x.ProjectionParameters <- Parameters.withIntegrationWindowOffset (value * 1<ns>) x.ProjectionParameters
            match x.ExperimentState with 
            | Running (experiment) -> experiment.ProjectionParameters.Trigger x.ProjectionParameters
            | _                       -> () 

    member x.IntegrationWindowWidth
        with get()     = int (Parameters.integrationWindowWidth x.ProjectionParameters)
        and  set value =
            x.ProjectionParameters <- Parameters.withIntegrationWidth (value * 1<ns>) x.ProjectionParameters
            match x.ExperimentState with
            | Running (experiment) -> experiment.ProjectionParameters.Trigger x.ProjectionParameters
            | _                       -> ()

    member x.LaserSwitchDelay 
        with get()     = int (Parameters.laserSwitchDelay x.ExperimentParameters)
        and  set value = x.ExperimentParameters <- Parameters.withLaserSwitchDelay (value * 1<ns>) x.ExperimentParameters

    member x.MicrowaveSwitchDelay
        with get()     = int (Parameters.microwaveSwitchDelay x.ExperimentParameters)
        and  set value = x.ExperimentParameters <- Parameters.withMicrowaveSwitchDelay (value * 1<ns>) x.ExperimentParameters

    member x.AcquisitionChannel
        with get()     = DuHelper.findDuStringIndexFromValue (Parameters.acquisitionChannel x.ExperimentParameters)
        and  set value = 
            let duValue = DuHelper.findDuValueFromStringIndex<PulseStreamer8.Model.Channel> value
            x.ExperimentParameters <- Parameters.withAcquisitionChannel duValue x.ExperimentParameters
        
    member x.LaserChannel
        with get()     =  DuHelper.findDuStringIndexFromValue (Parameters.laserChannel x.ExperimentParameters)
        and  set value = 
            let duValue = DuHelper.findDuValueFromStringIndex<PulseStreamer8.Model.Channel> value
            x.ExperimentParameters <- Parameters.withLaserChannel duValue x.ExperimentParameters

    member x.MicrowaveXChannel
        with get()     = DuHelper.findDuStringIndexFromValue (Parameters.microwaveXChannel x.ExperimentParameters)
        and  set value = 
            let duValue = DuHelper.findDuValueFromStringIndex<PulseStreamer8.Model.Channel> value
            x.ExperimentParameters <- Parameters.withMicrowaveXChannel duValue x.ExperimentParameters

    member x.MicrowaveYChannel
        with get()     = DuHelper.findDuStringIndexFromValue (Parameters.microwaveYChannel x.ExperimentParameters)
        and  set value = 
            let duValue = DuHelper.findDuValueFromStringIndex<PulseStreamer8.Model.Channel> value
            x.ExperimentParameters <- Parameters.withMicrowaveYChannel duValue x.ExperimentParameters

    member x.PulseStreamerChannels = 
        Microsoft.FSharp.Reflection.FSharpType.GetUnionCases typeof<PulseStreamer8.Model.Channel>
        |> Array.map(fun e -> e.Name)           

    member x.Notes
        with get()     = notes.Value
        and  set value = notes.Value <- value

    member x.SampleNotes
        with get()     = Notes.sampleNotes x.Notes
        and  set value = x.Notes <- x.Notes |> Notes.withSampleNotes value

    member x.ExperimentNotes
        with get()     = Notes.experimentNotes x.Notes
        and  set value = x.Notes <- x.Notes |> Notes.withExperimentNotes value

    member x.AdditionalParameterControls
        with get()     = additionalParameterControls

    member x.TwoPhaseAcquisition
        with get()     = Parameters.twoPhaseAcquisition x.ExperimentParameters
        and  set value = x.ExperimentParameters <- (Parameters.withTwoPhaseAcquisition value x.ExperimentParameters)
                         if (not (value)) then
                            self.DrawBothPhases <- false

    member x.DrawBothPhases
        with get()     = drawBothPhases.Value
        and  set value = drawBothPhases.Value <- value
                         if value then
                             x.ProjectedCurvePlotModel.Series.Add(projectedPrimaryPhaseCurveSeries)
                             x.ProjectedCurvePlotModel.Series.Add(projectedSecondaryPhaseCurveSeries)
                             x.ProjectedCurvePlotModel.Series.Remove(projectedCurveSeries) |> ignore
                         else
                             x.ProjectedCurvePlotModel.Series.Remove(projectedPrimaryPhaseCurveSeries) |> ignore 
                             x.ProjectedCurvePlotModel.Series.Remove(projectedSecondaryPhaseCurveSeries) |> ignore
                             x.ProjectedCurvePlotModel.Series.Add(projectedCurveSeries)
                         self.ProjectedCurvePlotModel.InvalidatePlot true

    member x.TwoPhaseReadyToStart
        with get()     = x.IsReadyToStart && experimentBuilderParameters.TwoPhaseAvailable

    member x.TauUnit
        with get()     = tauVariableParameters.Unit

    member x.TauStepSize
        with get()     = tauVariableParameters.Precision

    member x.PulseSequenceImagePath
        with get()     = pulseSequenceImagePath

    member x.StartExperiment = startExperimentCommand
    member x.StopExperiment = stopExperimentCommand
    member x.Save = saveCommand
    member x.LoadParameters = loadParametersCommand