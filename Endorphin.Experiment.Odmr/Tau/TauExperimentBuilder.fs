// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace Endorphin.Experiment.Odmr.Tau

open System
open System.Windows.Controls
open System.Windows.Forms.Integration
open Endorphin.Core
open Endorphin.Experiment.Odmr.OdmrExperimentBuilder
open Endorphin.Experiment.Odmr.Tau.TauExperiment

module TauExperimentBuilder = 
    module Parameters =
        let create name abscissaTitle tauParameters twoPhaseAvailable additionalParameterFields pulseSequenceBuilder curveProjectionCalculator startupFunction cleanupFunction = 
            { Name                              = name
              AbscissaTitle                     = abscissaTitle
              TauParameters                     = tauParameters
              TwoPhaseAvailable                 = twoPhaseAvailable
              AdditionalParameterFields         = additionalParameterFields
              PulseSequenceBuilder              = pulseSequenceBuilder
              CurveProjectionCalculator         = curveProjectionCalculator
              StartupFunction                   = startupFunction
              CleanupFunction                   = cleanupFunction }

    module CommonTauExperimentFunctions = 
        let singlePhaseCurveProjectionCalculator experiment (histogramsAndProjectionParameters : Histograms * TauCurveProjectionParameters) = 
            let (histograms, projectionParameters) = histogramsAndProjectionParameters
            
            let numberOfPoints = Array2D.length2 histograms
            let curveProjection = Array.zeroCreate numberOfPoints

            for point in 0..(numberOfPoints - 1) do
                let intensity = histograms.[int projectionParameters.IntegrationWindowOffset..(int projectionParameters.IntegrationWindowOffset + int projectionParameters.IntegrationWindowWidth), point]
                                |> Seq.sum
                curveProjection.[point] <- intensity

            let projection = 
                curveProjection
                |> Array.mapi (fun point intensity -> ((int experiment.Parameters.InitialTau) + (point * (int experiment.Parameters.TauInterval)), intensity))

            [projection]

        let twoPhaseCurveProjectionCalculator experiment (histogramsAndProjectionParameters : Histograms * TauCurveProjectionParameters) : (int * int)[] list = 
            let (histograms, projectionParameters) = histogramsAndProjectionParameters
            
            if (Parameters.twoPhaseAcquisition (experiment.Parameters)) then
                let numberOfPoints = (Array2D.length2 histograms) / 2
                let phase1Projection = Array.zeroCreate numberOfPoints
                let phase2Projection = Array.zeroCreate numberOfPoints

                for point in 0..(numberOfPoints - 1) do
                    let phase1Intensity = histograms.[int projectionParameters.IntegrationWindowOffset..(int projectionParameters.IntegrationWindowOffset + int projectionParameters.IntegrationWindowWidth), point]
                                          |> Seq.sum
                    phase1Projection.[point] <- phase1Intensity
                    let phase2Intensity = histograms.[int projectionParameters.IntegrationWindowOffset..(int projectionParameters.IntegrationWindowOffset + int projectionParameters.IntegrationWindowWidth), numberOfPoints + point]
                                          |> Seq.sum
                    phase2Projection.[point] <- phase2Intensity

                let projection1 = 
                    phase1Projection
                    |> Array.mapi (fun point intensity -> ((int experiment.Parameters.InitialTau) + (point * (int experiment.Parameters.TauInterval)), intensity))

                let projection2 = 
                    phase2Projection
                    |> Array.mapi (fun point intensity -> ((int experiment.Parameters.InitialTau) + (point * (int experiment.Parameters.TauInterval)), intensity))

                [projection1; projection2]
            else 
                singlePhaseCurveProjectionCalculator experiment histogramsAndProjectionParameters

        let interleavedTwoPhaseCurveProjectionCalculator experiment (histogramsAndProjectionParameters : Histograms * TauCurveProjectionParameters) : (int * int)[] list = 
            let (histograms, projectionParameters) = histogramsAndProjectionParameters
            
            if (Parameters.twoPhaseAcquisition (experiment.Parameters)) then
                let numberOfPoints = (Array2D.length2 histograms) / 2
                let phase1Projection = Array.zeroCreate numberOfPoints
                let phase2Projection = Array.zeroCreate numberOfPoints

                for point in 0..(numberOfPoints - 1) do
                    let phase1Intensity = histograms.[int projectionParameters.IntegrationWindowOffset..(int projectionParameters.IntegrationWindowOffset + int projectionParameters.IntegrationWindowWidth), 2 * point]
                                          |> Seq.sum
                    phase1Projection.[point] <- phase1Intensity
                    let phase2Intensity = histograms.[int projectionParameters.IntegrationWindowOffset..(int projectionParameters.IntegrationWindowOffset + int projectionParameters.IntegrationWindowWidth), 2 * point + 1]
                                          |> Seq.sum
                    phase2Projection.[point] <- phase2Intensity

                let projection1 = 
                    phase1Projection
                    |> Array.mapi (fun point intensity -> ((int experiment.Parameters.InitialTau) + (point * (int experiment.Parameters.TauInterval)), intensity))

                let projection2 = 
                    phase2Projection
                    |> Array.mapi (fun point intensity -> ((int experiment.Parameters.InitialTau) + (point * (int experiment.Parameters.TauInterval)), intensity))

                [projection1; projection2]
            else 
                singlePhaseCurveProjectionCalculator experiment histogramsAndProjectionParameters

        let addOrderId (additionalParameterList : List<string * (string * ParameterField * string)>) = 
            List.mapi (fun i (a, (b, c, d)) -> (a, (i, b, c, d))) additionalParameterList

    module Experiment = 
        let createExperimentWindow parameters width height pulseSequenceImagePath timeTaggerStreamingAcquisition microwaveSourceAgent pulseGeneratorAgent autofocusMailbox =   
            let tauWindow = System.Windows.Window()
            tauWindow.Title    <- parameters.Name
            tauWindow.Width    <- width
            tauWindow.Height   <- height

            let tauView = Endorphin.Experiment.Odmr.Tau.View.TauMainView()
            tauView.DataContext <- Endorphin.Experiment.Odmr.Tau.TauViewModel(timeTaggerStreamingAcquisition, microwaveSourceAgent, pulseGeneratorAgent, autofocusMailbox, parameters, pulseSequenceImagePath)
            tauWindow.Content <- tauView
            ElementHost.EnableModelessKeyboardInterop(tauWindow)
            tauWindow