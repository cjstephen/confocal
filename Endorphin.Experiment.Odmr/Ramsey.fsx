// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

#r "../packages/Endorphin.Core/lib/net452/Endorphin.Core.dll"
#r "../packages/FSharp.Control.Reactive/lib/net40/FSharp.Control.Reactive.dll"
#r "../packages/FSharp.ViewModule.Core/lib/portable-net45+netcore45+wpa81+wp8+MonoAndroid1+MonoTouch1/FSharp.ViewModule.dll"
#r "../packages/Extended.Wpf.Toolkit/lib/net40/Xceed.Wpf.Toolkit.dll"
#r "../packages/FsXaml.Wpf/lib/net45/FsXaml.Wpf.dll"
#r "../packages/FsXaml.Wpf/lib/net45/FsXaml.Wpf.TypeProvider.dll"
#r "../packages/OxyPlot.Core/lib/net45/OxyPlot.dll"
#r "../packages/OxyPlot.Wpf/lib/net45/OxyPlot.Wpf.dll"
#r "../packages/OxyPlot.Wpf/lib/net45/OxyPlot.Xps.dll"
#r "PresentationCore.dll"
#r "PresentationFramework.dll"
#r "System.Core.dll"
#r "System.dll"
#r "System.Numerics.dll"
#r "../packages/Rx-Core/lib/net45/System.Reactive.Core.dll"
#r "../packages/Rx-Interfaces/lib/net45/System.Reactive.Interfaces.dll"
#r "../packages/Rx-Linq/lib/net45/System.Reactive.Linq.dll"
#r "../packages/Expression.Blend.Sdk/lib/net45/System.Windows.Interactivity.dll"
#r "System.Xaml.dll"
#r "System.Xml.dll"
#r "WindowsBase.dll"
#r "../packages/Endorphin.Instrument.SwabianInstruments.PulseStreamer8/lib/net452/Endorphin.Instrument.SwabianInstruments.PulseStreamer8.dll"
#r "../packages/Endorphin.Instrument.PicoQuant.PicoHarp300/lib/net452/Endorphin.Instrument.PicoQuant.PicoHarp300.dll"
#r "../packages/Endorphin.Instrument.Keysight.N5172B/lib/net452/Endorphin.Instrument.Keysight.N5172B.dll"
#r "bin/Debug/Endorphin.Experiment.Odmr.dll"

open System
open System.Threading
open FsXaml
open Endorphin.Experiment.Odmr
open Endorphin.Experiment.Odmr.Tau
open Endorphin.Experiment.Odmr.Tau.TauExperiment
open Endorphin.Instrument
open Endorphin.Instrument.Keysight.N5172B
open Endorphin.Instrument.SwabianInstruments
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Core

let ramseySequence (parameters : TauExperimentParameters) = 
   seq { for i in 0..(parameters.NumberOfPoints - 1) do     
            for j in 0..(parameters.ShotsPerPoint - 1) do   
                yield PulseStreamer8.Pulse.empty 200u
                yield PulseStreamer8.Pulse.create 
                    <| [parameters.PulseStreamerParameters.MicrowaveXChannel] 
                    <| uint32 (parameters.AdditionalParameters.["pi2length"] :?> int)
                yield PulseStreamer8.Pulse.empty
                    <| (uint32 (parameters.InitialTau + (decimal i) * parameters.TauInterval))
                yield PulseStreamer8.Pulse.create
                    <| [parameters.PulseStreamerParameters.MicrowaveXChannel]
                    <| uint32 (parameters.AdditionalParameters.["pi2length"] :?> int)
                yield PulseStreamer8.Pulse.empty
                    <| 50u
                yield PulseStreamer8.Pulse.create
                    <| [parameters.PulseStreamerParameters.LaserChannel; parameters.PulseStreamerParameters.AcquisitionChannel]
                    <| 30u
                yield PulseStreamer8.Pulse.create 
                    <| [parameters.PulseStreamerParameters.LaserChannel]
                    <| (uint32 (parameters.DetectionParameters.LaserDuration)) 
         if (Parameters.twoPhaseAcquisition parameters) then
             for i in 0..(parameters.NumberOfPoints - 1) do     
                for j in 0..(parameters.ShotsPerPoint - 1) do   
                    yield PulseStreamer8.Pulse.empty 200u
                    yield PulseStreamer8.Pulse.create 
                        <| [parameters.PulseStreamerParameters.MicrowaveXChannel] 
                        <| uint32 (parameters.AdditionalParameters.["pi2length"] :?> int)
                    yield PulseStreamer8.Pulse.empty
                        <| (uint32 (parameters.InitialTau + (decimal i) * parameters.TauInterval))
                    yield PulseStreamer8.Pulse.create
                        <| [parameters.PulseStreamerParameters.MicrowaveXChannel]
                        <| uint32 (parameters.AdditionalParameters.["pi3by2length"] :?> int)
                    yield PulseStreamer8.Pulse.empty
                        <| 50u
                    yield PulseStreamer8.Pulse.create
                        <| [parameters.PulseStreamerParameters.LaserChannel; parameters.PulseStreamerParameters.AcquisitionChannel]
                        <| 30u
                    yield PulseStreamer8.Pulse.create 
                        <| [parameters.PulseStreamerParameters.LaserChannel]
                        <| (uint32 (parameters.DetectionParameters.LaserDuration * 4)) }
    |> List.ofSeq
    |> PulseStreamer8.Pulse.Transform.compensateHardwareDelays [parameters.PulseStreamerParameters.LaserChannel; parameters.PulseStreamerParameters.AcquisitionChannel] (uint32 parameters.LaserSwitchDelay)
    |> PulseStreamer8.Pulse.Transform.compensateHardwareDelays [parameters.PulseStreamerParameters.MicrowaveXChannel; parameters.PulseStreamerParameters.MicrowaveYChannel] (uint32 parameters.MicrowaveSwitchDelay)

let tauVariableParameters = 
    { Unit    = "ns"
      DefaultInitialValue = 0m
      DefaultInterval     = 5m
      Precision           = 1m }

let experimentBuilderParameters = TauExperimentBuilder.Parameters.create 
                                  <| "Ramsey"
                                  <| "Evolution time (ns)"
                                  <| tauVariableParameters
                                  <| true
                                  <| ([("pi2length", ("π/2 length", OdmrExperimentBuilder.IntParameterInRange (0, 10000, 1, 30), "ns")); 
                                       ("pi3by2length", ("3π/2 length", OdmrExperimentBuilder.IntParameterInRange (0, 10000, 1, 90), "ns"))] 
                                       |> TauExperimentBuilder.CommonTauExperimentFunctions.addOrderId 
                                       |> Map.ofList)
                                  <| ramseySequence
                                  <| TauExperimentBuilder.CommonTauExperimentFunctions.twoPhaseCurveProjectionCalculator
                                  <| (fun _ -> async { () })
                                  <| (fun _ -> async { () })

let imagePath = String.concat "/" [__SOURCE_DIRECTORY__; "images/ramsey.png"]
let ramseyWindow = TauExperimentBuilder.Experiment.createExperimentWindow experimentBuilderParameters 1200. 800. imagePath picoHarpStreamingAcquisition keysightAgent pulseGeneratorAgent autofocusAutomator
ramseyWindow.Show()