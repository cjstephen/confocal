// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

#r "../../packages/Rx-Core/lib/net45/System.Reactive.Core.dll"
#r "../../packages/Rx-Interfaces/lib/net45/System.Reactive.Interfaces.dll"
#r "../../packages/Rx-Linq/lib/net45/System.Reactive.Linq.dll"
#r "../../packages/Rx-PlatformServices/lib/net45/System.Reactive.PlatformServices.dll"
#r "../../packages/FSharp.Control.Reactive/lib/net40/FSharp.Control.Reactive.dll"
#r "../../packages/log4net/lib/net40-full/log4net.dll"
#r "../../packages/Endorphin.Core/lib/net452/Endorphin.Core.dll"
#r "../../Endorphin.UI/bin/Debug/Endorphin.UI.dll"
#r "../../packages/Endorphin.Instrument.PicoQuant.PicoHarp300/lib/net452/Endorphin.Instrument.PicoQuant.PicoHarp300.dll"
#r "../../packages/Endorphin.Abstract/lib/net452/Endorphin.Abstract.dll"
#r "../../Endorphin.Instrument.FastScanningController/bin/Debug/Endorphin.Instrument.FastScanningController.dll"
#r "../bin/Debug/Endorphin.Experiment.dll"

#r "PresentationCore.dll"
#r "PresentationFramework.dll"
#r "WindowsBase.dll"

open System
open System.Threading
open System.Windows
open System.Windows.Threading
open Endorphin.UI
open Endorphin.Core
open FSharp.Control.Reactive
open Endorphin.Instrument
open Endorphin.Instrument.PicoQuant
open Endorphin.Instrument.PicoQuant.PicoHarp300.Streaming
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols

let liveCountThread =  Thread (ThreadStart (fun () ->
    /// create window for experiment
    let liveCountWindow = new LiveCounts()
    let win = Window(Content=liveCountWindow, Width=400., Height=800., Title="Live counts")
    win.Topmost <- true

    // cancellationtokensource for the duration of the experiment
    let mutable cts : CancellationTokenSource option = None
    let syncRoot = new obj()

    let manualStop = new ManualResetEvent(false)

    let experiment =
        async {
            let liveCountAcquisition = PicoHarp300.Streaming.Acquisition.LiveCounts.create picoHarpStreamingAcquisition

            let normalizationFactor = (1./0.08)

            use __ = 
                PicoHarp300.Streaming.Acquisition.LiveCounts.LiveCountAvailable liveCountAcquisition
                |> Observable.subscribe (fun (ch0, ch1) -> liveCountWindow.addPoints (int ((float ch0) * normalizationFactor), int ((float ch1) * normalizationFactor)))
                
            do! Async.AwaitWaitHandle manualStop |> Async.Ignore }

    let experimentCts = new CancellationTokenSource()
    lock syncRoot (fun _ -> cts <- Some experimentCts)
    Async.StartWithContinuations (experiment, 
        (fun _ -> lock syncRoot (fun _ -> cts <- None)), 
        (fun exn -> 
            System.Windows.MessageBox.Show((sprintf "Problem with live charting: %s" exn.Message)) |> ignore
            lock syncRoot (fun () -> cts <- None) ),
        (fun _ -> lock syncRoot (fun _ -> cts <- None)),
        experimentCts.Token )

    win.Closing
    |> Event.add (fun _ ->
        match cts with
            | Some source -> source.Cancel()
            | _ -> ()
        manualStop.Set() |> ignore )

    win.Show() |> ignore  
    win.Closed.Add(fun _  -> win.Dispatcher.InvokeShutdown())
    System.Windows.Threading.Dispatcher.Run() ) )

liveCountThread.SetApartmentState(System.Threading.ApartmentState.STA)
liveCountThread.IsBackground <- true
liveCountThread.Start()