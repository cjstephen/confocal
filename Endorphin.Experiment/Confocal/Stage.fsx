// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

#r "../../packages/Rx-Core/lib/net45/System.Reactive.Core.dll"
#r "../../packages/Rx-Interfaces/lib/net45/System.Reactive.Interfaces.dll"
#r "../../packages/Rx-Linq/lib/net45/System.Reactive.Linq.dll"
#r "../../packages/Rx-PlatformServices/lib/net45/System.Reactive.PlatformServices.dll"
#r "../../packages/FSharp.Control.Reactive/lib/net40/FSharp.Control.Reactive.dll"
#r "../../packages/log4net/lib/net40-full/log4net.dll"
#r "../../packages/Endorphin.Core/lib/net452/Endorphin.Core.dll"
#r "../../Endorphin.UI/bin/Debug/Endorphin.UI.dll"
#r "../../packages/Endorphin.Instrument.PicoQuant.PicoHarp300/lib/net452/Endorphin.Instrument.PicoQuant.PicoHarp300.dll"
#r "../../packages/Endorphin.Abstract/lib/net452/Endorphin.Abstract.dll"
#r "../../Endorphin.Instrument.FastScanningController/bin/Debug/Endorphin.Instrument.FastScanningController.dll"
#r "../bin/Debug/Endorphin.Experiment.dll"

#r "PresentationCore.dll"
#r "PresentationFramework.dll"
#r "WindowsBase.dll"

open System
open System.Threading
open System.Windows
open System.Windows.Threading
open Endorphin.UI
open Endorphin.Core
open FSharp.Control.Reactive
open Endorphin.Instrument
open Endorphin.Instrument.FastScanningController.Model
open Endorphin.Instrument.FastScanningController.Instrument
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Abstract.Position

(* comment out next block if running from launcher *)
// init. fast scanning controller
(*let scanningControllerAgent = 
    let calibration = {X = 8e-6m<m/V>; Y = 8e-6m<m/V>; Z = 8e-6m<m/V>}
    let scanningController = ScanningController.openInstrument "COM7" 10000<ms> calibration |> Async.RunSynchronously
    Endorphin.Core.QueuedAccessAgent.create scanningController*)

//log4net.Config.BasicConfigurator.Configure()

let stageThread =  Thread (ThreadStart (fun () ->
    /// create window for experiment
    let stageWindow = new ManualStage()
    let win = Window(Content=stageWindow, Width=200., Height=200., Title="Manual stage control")

    win.Activated 
    |> Event.add (fun _ ->
        Async.RunSynchronously <| async {
            use! scanningControllerAccess = QueuedAccessAgent.requestControl scanningControllerAgent
            let! (x, y, z) = Position.getPosition scanningControllerAccess.Contents
            stageWindow.SetCurrentCoordinates(x/1m<um>, y/1m<um>, z/1m<um>)
            stageWindow.UIEnabled <- true
            } )

    win.Deactivated 
    |> Event.add (fun _ ->
        stageWindow.UIEnabled <- false)

    stageWindow.GoToLocation
    |> Event.add (fun (location) -> 
        Async.RunSynchronously <| async {
            use! scanningControllerAccess = QueuedAccessAgent.requestControl scanningControllerAgent
            do! Position.setPosition scanningControllerAccess.Contents (Point.tfst location.coordinate * 1m<um>, Point.tsnd location.coordinate * 1m<um>, Point.ttrd location.coordinate * 1m<um>)
        } )
        
    win.Show() |> ignore  
    win.Closed.Add(fun _  -> win.Dispatcher.InvokeShutdown())
    System.Windows.Threading.Dispatcher.Run() ) )

stageThread.SetApartmentState(System.Threading.ApartmentState.STA)
stageThread.IsBackground <- true
stageThread.Start()