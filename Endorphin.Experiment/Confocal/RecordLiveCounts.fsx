// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

#r "../../packages/Rx-Core.2.2.5/lib/net45/System.Reactive.Core.dll"
#r "../../packages/Rx-Interfaces.2.2.5/lib/net45/System.Reactive.Interfaces.dll"
#r "../../packages/Rx-Linq.2.2.5/lib/net45/System.Reactive.Linq.dll"
#r "../../packages/Rx-PlatformServices.2.2.5/lib/net45/System.Reactive.PlatformServices.dll"
#r "../../packages/FSharp.Control.Reactive.3.2.0/lib/net40/FSharp.Control.Reactive.dll"
#r "../../packages/log4net.2.0.3/lib/net40-full/log4net.dll"
#r "../../Endorphin.Core/bin/Debug/Endorphin.Core.dll"
#r "../../Endorphin.UI/bin/Debug/Endorphin.UI.dll"
#r "../../Endorphin.Instrument.PicoHarp300/bin/Debug/Endorphin.Instrument.PicoHarp300.dll"
#r "../../Endorphin.Utilities.Position/bin/Debug/Endorphin.Utilities.Position.dll"
#r "../../Endorphin.Instrument.FastScanningController/bin/Debug/Endorphin.Instrument.FastScanningController.dll"
#r "../bin/Debug/Endorphin.Experiment.dll"

#r "PresentationCore.dll"
#r "PresentationFramework.dll"
#r "WindowsBase.dll"

open System
open System.Threading
open System.Windows
open System.Windows.Threading
open Endorphin.UI
open Endorphin.Core
open FSharp.Control.Reactive
open Endorphin.Instrument
open Endorphin.Instrument.PicoHarp300.Streaming
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols

// cancellationtokensource for the duration of the experiment
let mutable cts : CancellationTokenSource option = None
let syncRoot = new obj()

let manualStop = new ManualResetEvent(false)
let filePath = @"C:\Users\NQIT\Desktop\RawData\LiveCounts.csv"
let recordingTimeInSeconds = 10.

let experiment =
    async {
        use writeFile = new System.IO.StreamWriter(filePath)

        let liveCountAcquisition = PicoHarp300.Streaming.Acquisition.LiveCounts.create picoHarpStreamingAcquisition

        let normalizationFactor = (1./0.08)

        use __ = 
            Observable.interval (TimeSpan.FromMilliseconds 1000.)
            |> Observable.subscribe (fun s -> printfn "%d/%d" s ((int recordingTimeInSeconds) - 1))

        use __ = 
            Observable.interval (TimeSpan.FromSeconds recordingTimeInSeconds)
            |> Observable.subscribe (fun _ -> do manualStop.Set() |> ignore)

        use __ = 
            PicoHarp300.Streaming.Acquisition.LiveCounts.LiveCountAvailable liveCountAcquisition
            |> Observable.subscribe (fun (ch0, ch1) -> 
                let writeString = [(string ((float ch0)*normalizationFactor)); ","; (string ((float ch1)*normalizationFactor)); "\n"] |> String.concat ""
                do writeFile.Write(writeString))
                
        do! Async.AwaitWaitHandle manualStop |> Async.Ignore 
        printfn "Finished recording live counts" }

let experimentCts = new CancellationTokenSource()
lock syncRoot (fun _ -> cts <- Some experimentCts)
Async.StartWithContinuations (experiment, 
    (fun _ -> lock syncRoot (fun _ -> cts <- None)), 
    (fun exn -> 
        System.Windows.MessageBox.Show((sprintf "Problem with recording live counts: %s" exn.Message)) |> ignore
        lock syncRoot (fun () -> cts <- None) ),
    (fun _ -> lock syncRoot (fun _ -> cts <- None)),
    experimentCts.Token )