// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

#r "../../packages/Rx-Core/lib/net45/System.Reactive.Core.dll"
#r "../../packages/Rx-Interfaces/lib/net45/System.Reactive.Interfaces.dll"
#r "../../packages/Rx-Linq/lib/net45/System.Reactive.Linq.dll"
#r "../../packages/Rx-PlatformServices/lib/net45/System.Reactive.PlatformServices.dll"
#r "../../packages/FSharp.Control.Reactive/lib/net40/FSharp.Control.Reactive.dll"
#r "../../packages/log4net/lib/net40-full/log4net.dll"
#r "../../packages/Endorphin.Core/lib/net452/Endorphin.Core.dll"
#r "../../Endorphin.UI/bin/Debug/Endorphin.UI.dll"
#r "../../packages/Endorphin.Instrument.PicoQuant.PicoHarp300/lib/net452/Endorphin.Instrument.PicoQuant.PicoHarp300.dll"
#r "../../packages/Endorphin.Abstract/lib/net452/Endorphin.Abstract.dll"
#r "../../Endorphin.Instrument.FastScanningController/bin/Debug/Endorphin.Instrument.FastScanningController.dll"
#r "../../packages/Endorphin.Instrument.SwabianInstruments.PulseStreamer8/lib/net452/Endorphin.Instrument.SwabianInstruments.PulseStreamer8.dll"
#r "../bin/Debug/Endorphin.Experiment.dll"

#r "PresentationCore.dll"
#r "PresentationFramework.dll"
#r "WindowsBase.dll"

open System
open System.Threading
open System.Windows
open System.Windows.Threading
open Endorphin.UI
open Endorphin.Core
open FSharp.Control.Reactive
open Endorphin.Instrument
open Endorphin.Instrument.PicoQuant
open Endorphin.Instrument.PicoQuant.PicoHarp300.Streaming
open Endorphin.Instrument.FastScanningController.Model
open Endorphin.Instrument.FastScanningController.Instrument
open Endorphin.Instrument.SwabianInstruments.PulseStreamer8
open Microsoft.FSharp.Data.UnitSystems.SI.UnitSymbols
open Endorphin.Abstract.Position

let confocalThread = Thread (ThreadStart (fun () -> 
    /// create window for experiment
    let confocalWindow = new Confocal()
    let win = Window(Content=confocalWindow, Width=800., Height=900., Title="Confocal mapping")

    // cancellationtokensource for the duration of the experiment
    let mutable cts : CancellationTokenSource option = None
    let syncRoot = new obj()

    // prevent the user from closing the window if the experiment is still running
    win.Closing
    |> Event.add (fun cancelArgs ->
        lock syncRoot (fun _ ->
        match cts with
        | None -> ()
        | Some token ->
            MessageBox.Show("Please finish the experiment before closing the window.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning) |> ignore
            cancelArgs.Cancel <- true))

    /// take a two-dimensional coordinate and provide a 3D coordinate
    let convertPointToXYZ point initialStagePosition axes =
        match axes, initialStagePosition with
        | Confocal.ScanAxes.XY, (_, _, z)   -> (fst point, snd point, z)
        | Confocal.ScanAxes.XZ, (_, y, _)   -> (fst point, y, snd point)
        | Confocal.ScanAxes.YZ, (x, _, _)   -> (x, fst point, snd point)
        | _, _                              -> failwithf "Unexpected scan axes value %A" axes

    /// go to the clicked point if there isn't an experiment running
    confocalWindow.GoToPoint
    |> Event.add (fun point ->
        match cts with
        | None ->
            Async.RunSynchronously <| async {
                let (x, y, z) = point.coordinate
                let targetPosition = (decimal x * 1m<um>, decimal y * 1m<um>, decimal z * 1m<um>)
                use! scanningControllerAccess = QueuedAccessAgent.requestControl scanningControllerAgent
                do! Position.setPosition scanningControllerAccess.Contents targetPosition
                confocalWindow.SetCurrentCoordinates (x,y,z) }
        | Some _ -> ()
        )

    // define experiment
    let experiment (parameters:Confocal.MapParameters) = async {

        // enable cancel button for duration of experiment
        use __ =
            confocalWindow.StopExperiment
            |> Observable.subscribe (fun _ ->
                match cts with
                | Some source -> source.Cancel()
                | _ -> ())


        confocalWindow.InformationText <- "Requesting access to scanning controller"
        use! scanningControllerAccess = QueuedAccessAgent.requestControl scanningControllerAgent
        //confocalWindow.InformationText <- "Requesting access to PicoHarp"
        //use! picoHarpAccess = QueuedAccessAgent.requestControl picoHarpAgent
        confocalWindow.InformationText <- "Requesting access to PulseStreamer"
        use! pulseStreamerAccess = QueuedAccessAgent.requestControl pulseGeneratorAgent

        do! PulseStreamer.PulseSequence.setState [Channel1] pulseStreamerAccess.Contents

        // store current stage position and setup the dwell time on each point
        let! initialStagePosition = Position.getPosition scanningControllerAccess.Contents //PiezosystemNV40.PiezojenaNV40.Motion.queryPosition stage
        do! Timing.setDwellTime scanningControllerAccess.Contents (parameters.integrationTime * 1<ms>)
        do! Timing.setTriggerDelay scanningControllerAccess.Contents 3.<ms>

        // setup confocal image
        let numberOfPoints = 1 + int (round((float parameters.gridSize) / parameters.stepSize))
        confocalWindow.Intensity <- Array2D.create numberOfPoints numberOfPoints (Double.NaN)

        let (windowOrigin, scanAxes) =
            match parameters.axes, parameters.origin with
            | Confocal.ScanAxes.XY, (x, y, _) -> (x, y), Path.Plane.XY
            | Confocal.ScanAxes.XZ, (x, _, z) -> (x, z), Path.Plane.XZ
            | Confocal.ScanAxes.YZ, (_, y, z) -> (y, z), Path.Plane.YZ
            | _, _                            -> failwithf "Unexpected scan axis value  %A" parameters.axes

        let origin = (decimal (Point.tfst parameters.origin) * 1m<um>, decimal (Point.tsnd parameters.origin) * 1m<um>, decimal (Point.ttrd parameters.origin) * 1m<um>)

        let windowLimits =  [| fst windowOrigin; fst windowOrigin + float parameters.gridSize; snd windowOrigin; snd windowOrigin + float parameters.gridSize |]
        (*match parameters.axes with
            | Confocal.ScanAxes.XY -> [| fst windowOrigin; fst windowOrigin + float parameters.gridSize; snd windowOrigin; snd windowOrigin + float parameters.gridSize |]
            | _                    -> [| fst windowOrigin; fst windowOrigin + float parameters.gridSize; snd windowOrigin + float parameters.gridSize; snd windowOrigin |]*)
        confocalWindow.SetAxisLimits windowLimits// [| fst windowOrigin; fst windowOrigin + float parameters.gridSize; snd windowOrigin; snd windowOrigin + float parameters.gridSize |]

        // create path
        let path = Path.createSquareSnake origin ((decimal parameters.gridSize) * 1m<um>) (decimal parameters.stepSize * 1m<um>) scanAxes

        confocalWindow.InformationText <- "Writing path to controller"
        do! Position.Path.writePathToController scanningControllerAccess.Contents path

        confocalWindow.InformationText <- "Moving to first point..."
        do! Position.setPosition scanningControllerAccess.Contents (Path.coordinatesAtIndex 0 path)
        do! Async.Sleep 500

        // set picoharp on a long measurement
        let histogramParameters = PicoHarp300.Streaming.Acquisition.Histogram.Parameters.create (PicoHarp300.Model.Duration_ms (1.0<ms> * float parameters.integrationTime)) (PicoHarp300.Model.Duration.Duration_ms (1.0<ms> * float parameters.integrationTime)) PicoHarp300.Model.TTTRConfiguration.Marker2
        //let acquisition = PicoHarp300.Streaming.Acquisition.create picoHarpAccess.Contents (PicoHarp300.Streaming.Parameters.create (PicoHarp300.Model.Duration_ms (1.0<ms> * float parameters.integrationTime)) (PicoHarp300.Model.Duration.Duration_ms (1.0<ms> * float parameters.integrationTime)))
        //let! acquisitionHandle = PicoHarp300.Streaming.Acquisition.startAsChild acquisition
        let histogramAcquisition = PicoHarp300.Streaming.Acquisition.Histogram.create picoHarpStreamingAcquisition histogramParameters

        confocalWindow.InformationText <- "Experiment running..."
        //try
        // when new histogram is available, update the map image
        let histogramObservable =
            (PicoHarp300.Streaming.Acquisition.Histogram.HistogramsAvailable histogramAcquisition)
            |> Observable.mapi (fun i histogram -> (i, histogram))

        use __ =
            histogramObservable
            |> Observable.subscribe (fun (i, histogram) ->
                let index = Path.pointAtIndex i path
                match histogram.Histogram with
                | [(binNumber, intensity)]     -> confocalWindow.Intensity.[fst index, snd index] <- (float intensity / ((float parameters.integrationTime) / 1000.))
                | []                           -> confocalWindow.Intensity.[fst index, snd index] <- 0.
                | _                            -> raise (new Exception(sprintf "PicoHarp streaming histogram in a format other than (bin, intensity) - %A" histogram.Histogram)) )

        use __ =
            Observable.interval (TimeSpan.FromMilliseconds 200.)
            |> Observable.subscribe (fun _ -> confocalWindow.UpdateImage())

        let! waitToFinish =
            histogramObservable
            |> Observable.filter (fun (i, _) -> i = numberOfPoints * numberOfPoints - 1)
            |> Async.AwaitObservable
            |> Async.Ignore
            |> Async.StartChild

        do! Position.Path.runPath scanningControllerAccess.Contents

        do! waitToFinish
        // once scan has finished, update the window for the last time
        do confocalWindow.UpdateImage()
    }

    // start experiment when ui button clicked
    confocalWindow.StartExperiment
    |> Event.add (fun parameters ->
        confocalWindow.UIEnabled <- false
        let experimentCts = new CancellationTokenSource()
        lock syncRoot (fun () -> cts <- Some experimentCts)
        Async.StartWithContinuations (experiment parameters,
            (fun _ ->
                Async.RunSynchronously <| async {
                    use! scanningControllerAccess = QueuedAccessAgent.requestControl scanningControllerAgent
                    do! Position.Path.stopPath scanningControllerAccess.Contents
                    confocalWindow.InformationText <- "Successfully completed experiment."
                    confocalWindow.UIEnabled <- true
                    lock syncRoot (fun () -> cts <- None) }),
            (fun exn ->
                confocalWindow.InformationText <- sprintf "Failed to complete experiment due to error: %s" exn.Message
                lock syncRoot (fun () -> cts <- None)),
            (fun _ ->
                Async.RunSynchronously <| async {
                    use! scanningControllerAccess = QueuedAccessAgent.requestControl scanningControllerAgent
                    do! Position.Path.stopPath scanningControllerAccess.Contents
                    confocalWindow.UIEnabled <- true
                    confocalWindow.InformationText <- "Experiment cancelled"
                    lock syncRoot (fun () -> cts <- None) }),
            experimentCts.Token))

    win.Show() |> ignore
    win.Closed.Add(fun _  -> win.Dispatcher.InvokeShutdown())
    System.Windows.Threading.Dispatcher.Run() ) )


confocalThread.SetApartmentState(System.Threading.ApartmentState.STA)
confocalThread.IsBackground <- true
confocalThread.Start()