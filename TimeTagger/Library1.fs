// Copyright (c) University of Warwick. All Rights Reserved. Licensed under the Apache License, Version 2.0. See LICENSE.txt in the project root for license information.

namespace TimeTagger

open SwabianInstruments.TimeTagger
open FSharp.Control.Reactive
open System

module Foo =

    type Channel =
        | Rising0
        | Rising1
        | Rising2
        | Rising3
        | Rising4
        | Rising5
        | Rising6
        | Rising7
        | Falling0
        | Falling1
        | Falling2
        | Falling3
        | Falling4
        | Falling5
        | Falling6
        | Falling7

    let tt = SwabianInstruments.TimeTagger.TT.createTimeTagger()
    let a = new SwabianInstruments.TimeTagger.Countrate(tt,[| 1u; 2u |])
    let b = a.getData()

    let o = Observable.interval(System.TimeSpan.FromMilliseconds(100.0))
            |> Observable.map (fun _ -> a.getData())

//    let c = new SwabianInstruments.TimeTagger.Counter(

    let d = new SwabianInstruments.TimeTagger.CountBetweenMarkers(tt,1u,1u,1u,1000)
    SwabianInstruments.TimeTagger.Counter(

    type Histogram2D = unit

    type ITimeTag =
        // binsize, bins, histograms
//        abstract Histogram2D : TimeSpan -> int64 -> int -> int -> IObservable<Histogram2D>
        // apply to synthetic combined channel
        // binwidth, number of bins in rolling structure (can be 1)
        abstract LiveCounts : int64 -> int -> TimeSpan -> IObservable<float[]>
        abstract CountSequence : int -> TimeSpan -> IObservable<float>

    type TimeTaggerSource(tt : SwabianInstruments.TimeTagger.TimeTagger, clickChannels : uint32[], triggerChannel : uint32) =

        let combiner = new SwabianInstruments.TimeTagger.Combiner(tt,clickChannels)
        let combinedChannel = combiner.getChannel()

        let triggerCounter = new SwabianInstruments.TimeTagger.Counter(tt,[| triggerChannel |])

        interface ITimeTag with
            member __.CountSequence n (interval:TimeSpan) =
                let countBetween = new SwabianInstruments.TimeTagger.CountBetweenMarkers(tt,combinedChannel,triggerChannel,triggerChannel,n)
                countBetween.start()
                triggerCounter.start()

                Observable.interval interval
                |> Observable.map (fun _ -> triggerCounter.getData().[0,0])
                |> Observable.startWith [0]
                |> Observable.pairwise
                |> Observable.flatmapSeq (fun (lastCount,nextCount) ->
                    let data = countBetween.getData()
                    let width = countBetween.getBinWidths()
                    let countRate c t = (float c) / (float t)
                    Array.map2 countRate data.[lastCount .. nextCount-1] width.[lastCount .. nextCount-1]
                    |> Array.toSeq)

            member __.LiveCounts w n interval =
                let liveCounter = new SwabianInstruments.TimeTagger.Counter(tt,[|combinedChannel|],w,n)
                liveCounter.start()

                Observable.interval interval
                |> Observable.map (fun _ -> liveCounter.getData().[0,0..n-1] |> Array.map (fun c -> (float c)/(float w)))
                

        interface IDisposable with
            member x.Dispose() = combiner.Dispose()





